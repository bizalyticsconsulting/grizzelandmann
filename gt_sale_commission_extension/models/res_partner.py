# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import except_orm, Warning
import logging
_logger = logging.getLogger(__name__)

class ResPartner(models.Model):
    _inherit = "res.partner"

    partner_type = fields.Selection(selection_add=[("Client", "Client"),("ot", "OT"),("Showroom", "Showroom"),("Office", "Office"),("Personal", "Personal"),("Vendor", "Vendor"),("Designer", "Designer"),("Project", "Project"),("p", "P"),("Ship To", "Ship To"),("po", "PO"),("Other", "Other"),("Purchasing", "Purchasing")], string="Type")
    showroom_type = fields.Selection([('showroom_bill','Showroom Bill'),('vendor_bill','Vendor Bill')], string="Billing Option")
    commissions_percentage = fields.Float(string="Commission Percentage")
    is_part_comm = fields.Boolean('Is Partner Commission ?')
    
    

