# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.osv import  osv
from odoo.exceptions import except_orm, Warning
import odoo.addons.decimal_precision as dp
from datetime import datetime
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT
import logging
_logger = logging.getLogger(__name__)

class ProductTemplate(models.Model):
	_inherit = "product.template"

	flag = fields.Boolean(string="Commissionable Product", default=True)
