# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
from odoo.exceptions import Warning,UserError

class WizardForPreNumberedChecks(models.TransientModel):
    _inherit = 'print.prenumbered.checks'

    @api.multi
    def print_checks(self):
        ctx = self.env.context.copy()
        check_number = self.next_check_number
        if self.env.context.get('check_commission_ids'):
            ctx.update({'commission_check_print': True, 'commission_id': self.env.context.get('check_commission_ids')})
            search_paid_checks = self.env['search.commission.paid.check'].browse(self.env.context.get('check_commission_ids'))
            for check in search_paid_checks:
                check.check_number = check_number
                check.check_printed = True
                check_number += 1
        else:
            payments = self.env['account.payment'].browse(self.env.context['payment_ids'])
            payments.filtered(lambda r: r.state == 'draft').post()
            payments.filtered(lambda r: r.state != 'sent').write({'state': 'sent'})
            for payment in payments:
                # if payment.filtered(lambda r: r.state == 'draft'):
                #     raise UserError(_("You cannot print a payment which is in draft state"))
                payment.check_number = check_number
                check_number += 1
        return {
                'name': _('Print Checks Format'),
                'type': 'ir.actions.act_window',
                'res_model': 'wizard.dynamic.cheque.print',
                'view_type': 'form',
                'view_mode': 'form',
                'target': 'new',
                'context': ctx,
            }
    
class wizard_dynamic_cheque_print(models.TransientModel):
    _inherit = 'wizard.dynamic.cheque.print'

    @api.multi
    def action_call_report(self):
        if self.env.context.get('commission_check_print'):
            data = self.read()[0]
            data.update({'commission_check_print': True})
            paid_commission_obj = self.env['search.commission.paid.check']
            # paid_comm_id = paid_commission_obj.search([('id','in',self.env.context.get('active_ids'))])
            paid_comm_id = paid_commission_obj.browse(self._context.get('commission_id'))
            if paid_comm_id:
                paid_comm_id.write({'check_printed': True})
            if self.cheque_format_id.paper_format_id and self.cheque_format_id.cheque_height <= 0 or self.cheque_format_id.cheque_width <= 0:
                raise Warning(_("Check height and width can not be less than Zero(0)."))
            result = self.cheque_format_id.paper_format_id.write({
                    'format': 'custom',
                    'page_width': self.cheque_format_id.cheque_width,
                    'page_height': self.cheque_format_id.cheque_height,
                    })
            datas = {
                'ids': self._context.get('commission_id') if self._context.get('commission_id',False) else self._context.get('active_ids'),
                'model': 'wizard.dynamic.cheque.print',
                'form': data
            }
            return  self.env['report'].get_action(self, 'dynamic_cheque_print.dynamic_cheque_print_template', data=datas)
            # return  self.env['report'].get_action(self, 'gt_sale_commission_extension.commission_cheque_print_report_template', data=datas)
        else:
            return super(wizard_dynamic_cheque_print, self).action_call_report()
        
class dynamic_cheque_print_template(models.AbstractModel):
    _inherit = 'report.dynamic_cheque_print.dynamic_cheque_print_template'

    @api.multi
    def render_html(self, docids, data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('dynamic_cheque_print.dynamic_cheque_print_template')
        if not data.get('form').get('label_preview'):
            records = self.env["account.payment"].browse(data["ids"])
        else:
            records = self.env['wizard.cheque.preview'].browse(data['ids'])
        if data.get('form').get('commission_check_print'):
            records = self.env['search.commission.paid.check'].browse(data["ids"])
        docargs = {
           'doc_ids': records,
           'doc_model': report.model,
           'docs': self,
           'draw_style': self._draw_style,
           'get_date': self._get_date,
           'get_company': self._get_company,
           'get_signatory_one': self._get_signatory_one,
           'get_word_line': self._get_word_line,
           'get_currency_position': self._get_currency_position,
           'num2words': self.num2words,
           'is_pay_acc': self._is_pay_acc,
           'get_amount': self._get_amount,
           'data': data
        }
        if data.get('form').get('commission_check_print'):
            return report_obj.render('gt_sale_commission_extension.commission_cheque_print_report_template', docargs)
        else:
            return report_obj.render('dynamic_cheque_print.dynamic_cheque_print_template', docargs,)

    def _get_word_line(self, data, record):
        num_words_list = []
        if data.get('commission_check_print'):
            num_words = self.num2words(data, record.pay_amount)
        else:
            num_words = self.num2words(data, record.amount)
        config_id = self.env['dynamic.cheque.format.configuration'].browse([data.get('cheque_format_id')[0]])
        if config_id:
            if config_id.currency_name:
                if config_id.currency_name_position == 'before':
                    num_words = record.currency_id.name + ' ' + num_words
                else:
                    num_words = num_words + ' ' + record.currency_id.name
            if config_id and config_id.amount_word_type == 'standard':
                num_words = num_words.title()
            if config_id and config_id.amount_word_type == 'capital':
                num_words = num_words.upper()
            if config_id and config_id.amount_word_type == 'small':
                num_words = num_words.lower()
            if config_id.first_line_words_count > 0:
                num_words_list.append(num_words[0:config_id.first_line_words_count])
            if config_id.second_line_words_count > 0:
                num_words_list.append(num_words[config_id.first_line_words_count:(config_id.first_line_words_count + config_id.second_line_words_count)])
        if num_words_list:
            return num_words_list
        
    def _draw_style(self, data, field):
        config_id = False
        style = super(dynamic_cheque_print_template, self)._draw_style(data, field)
        if data.get('cheque_format_id'):
            config_id = self.env['dynamic.cheque.format.configuration'].browse([data.get('cheque_format_id')[0]])
            
        if config_id:
            if field == 'bill_data':
                style = 'position:absolute;font-size:' + str(config_id.font_size_bill_data) + 'mm;margin-top:' + str(config_id.bill_data_top_margin) + 'mm;'
                # style="position: fixed;margin-top:100mm;"
            if field == 'comm_line_data':
                style = 'position:absolute;font-size:' + str(config_id.font_size_bill_data) + 'mm;margin-top:120 mm;'
        return style
    
    
    