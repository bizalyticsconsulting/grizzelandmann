# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError
from datetime import datetime

class SaleConfigSetting(models.TransientModel):
    _inherit = 'res.config.settings'
  
    when_to_pay = fields.Selection([
        ('0', 'Sale Validation'),
        ('1', 'Invoice Validation'),
        ('2', 'Payment Validation'),
        ('3', 'Full Payment & Shipped Validation')
        ], "When To Pay", default='0')
      
    commission_calculation = fields.Selection([
        ('0', 'Product'),
        ('1', 'Product Category'),
        ('2', 'Sales Team'),
        ('3', 'Showroom Partner'),
        ], "Calculation Based On", default='0')
#    
    @api.model
    def get_values(self):
        res = super(SaleConfigSetting, self).get_values()
        ICPSudo = self.env['ir.config_parameter'].sudo()
        when_to_pay = ICPSudo.get_param('gt_sale_commission_extension.sale.when_to_pay')
        print("ICPSudoICPSudo111111111",ICPSudo,when_to_pay)
        res.update(
            when_to_pay=ICPSudo.get_param('gt_sale_commission_extension.sale.when_to_pay'),
            commission_calculation=ICPSudo.get_param('gt_sale_commission_extension.sale.commission_calculation'),
        )
        print("resresresres111111111111",res)
        return res
  
        
    @api.multi
    def set_values(self):
        super(SaleConfigSetting, self).set_values()
        ICPSudo = self.env['ir.config_parameter'].sudo()
        when_to_pay = ICPSudo.set_param("gt_sale_commission_extension.sale.when_to_pay", self.when_to_pay)
        commission_calculation = ICPSudo.set_param("gt_sale_commission_extension.sale.commission_calculation", self.commission_calculation)
        print("ICPSudo1111111111",ICPSudo)
        print("when_to_pay11111111111",when_to_pay)
        print("commission_calculation111111111111",commission_calculation)
        product_obj = self.env['product.template']
        partner_obj = self.env['res.partner']
        product_categ_obj = self.env['product.category']
        crm_team_obj = self.env['crm.team']
        # Setting the values in the vies of commission data.
        print("self.commission_calculation1111111111",self.commission_calculation)
        print("selfwhen_to_pay1111111111111",self.when_to_pay)
        
        if self.commission_calculation == '0':
            # print "Product : "
            product_search = product_obj.search([]).ids
            print("product_search",product_search)
            product_browse = product_obj.browse(product_search)
            print("product_browse",product_browse)
            for product_id in product_browse:
                product_id.write({'is_prod_comm': True})
            # print "Product Category : "
            product_categ_search = product_categ_obj.search([]).ids
            product_categ_browse = product_categ_obj.browse(product_categ_search)
            for product_categ_id in product_categ_browse:
                product_categ_id.write({'is_prod_categ_comm': False})
            # print "CRM Team : "
            crm_team_search = crm_team_obj.search([]).ids
            crm_team_browse = crm_team_obj.browse(crm_team_search)
            for crm_team_id in crm_team_browse:
                crm_team_id.write({'is_sale_team_comm': False})
  
        if self.commission_calculation == '1':
            # print "Product : "
            product_search = product_obj.search([]).ids
            product_browse = product_obj.browse(product_search)
            for product_id in product_browse:
                product_id.write({'is_prod_comm': False})
            # print "Product Category : "
            product_categ_search = product_categ_obj.search([]).ids
            product_categ_browse = product_categ_obj.browse(product_categ_search)
            for product_categ_id in product_categ_browse:
                product_categ_id.write({'is_prod_categ_comm': True})
            # print "CRM Team : "
            crm_team_search = crm_team_obj.search([]).ids
            crm_team_browse = crm_team_obj.browse(crm_team_search)
            for crm_team_id in crm_team_browse:
                crm_team_id.write({'is_sale_team_comm': False})
  
        if self.commission_calculation == '2':
            # print "Product : "
            product_search = product_obj.search([]).ids
            product_browse = product_obj.browse(product_search)
            for product_id in product_browse:
                product_id.write({'is_prod_comm': False})
            # print "Product Category : "
            product_categ_search = product_categ_obj.search([]).ids
            product_categ_browse = product_categ_obj.browse(product_categ_search)
            for product_categ_id in product_categ_browse:
                product_categ_id.write({'is_prod_categ_comm': False})
            # print "CRM Team : "
            crm_team_search = crm_team_obj.search([]).ids
            crm_team_browse = crm_team_obj.browse(crm_team_search)
            for crm_team_id in crm_team_browse:
                crm_team_id.write({'is_sale_team_comm': True})
                  
        if self.commission_calculation == '3':
            # print "Product : "
            product_search = product_obj.search([]).ids
            product_browse = product_obj.browse(product_search)
            for product_id in product_browse:
                product_id.write({'is_prod_comm': False})
            # print "Product Category : "
            product_categ_search = product_categ_obj.search([]).ids
            product_categ_browse = product_categ_obj.browse(product_categ_search)
            for product_categ_id in product_categ_browse:
                product_categ_id.write({'is_prod_categ_comm': False})
            # print "CRM Team : "
            crm_team_search = crm_team_obj.search([]).ids
            crm_team_browse = crm_team_obj.browse(crm_team_search)
            for crm_team_id in crm_team_browse:
                crm_team_id.write({'is_sale_team_comm': False})
            #Partner
            partner_srch = partner_obj.search([('customer','=',True), ('active','=',True)])
            for par_id in partner_srch:
                par_id.write({'is_part_comm': True})
            # ('partner_type','in',['showroom','client','ship to','vendor','office'])
          
#         return data

class SaleOrder(models.Model):
    _inherit = "sale.order"
    
    @api.multi
    def _get_commission_line_count(self):
        for rec in self:
            rec.commission_count = len(rec.account_move_line_ids)
    
    commission_count = fields.Integer(compute=_get_commission_line_count, string="#Commission")
    account_move_line_ids = fields.One2many('account.move.line','sale_order_id','Account Move Line', help="Journal Items for the Sales Commissions", domain=[('account_id.user_type_id.name', '=', 'Payable')], copy=False)
    # showroom_partner_id = fields.Many2one('res.partner', 'Showroom Partner')
    order_pay_state = fields.Selection([
        ('0', 'Sale Validation'),
        ('1', 'Invoice Validation'),
        ('2', 'Payment Validation'),
        ('3', 'Full Payment & Shipped Validation'),
    ], "When Paid", copy=False)
    commission_generated = fields.Boolean('Commission Generated')
    
    @api.multi
    def action_view_commission(self):
        imd = self.env['ir.model.data']
        action = imd.xmlid_to_object('sale_commission_gt.action_account_move_line')
        list_view_id = imd.xmlid_to_res_id('gt_sale_commission_extension.account_move_line_tree_view_inherit_com1')
        # form_view_id = imd.xmlid_to_res_id('account.view_move_line_form')
        result = {
            'name': action.name,
            'help': action.help,
            'type': action.type,
            'views': [[list_view_id, 'tree']],
            'target': 'new',
            'context': action.context,
            'res_model': action.res_model,
        }
        if len(self.account_move_line_ids) >= 1:
            result['domain'] = "[('id','in',%s)]" % self.account_move_line_ids.ids
            # result['target'] = 'new'
        # elif len(commission_ids) == 1:
        #     result['views'] = [(form_view_id, 'form')]
        #     result['res_id'] = self.account_move_line_ids and self.account_move_line_ids.id or False
        #     result['target'] =  'new'
        else:
            result = {'type': 'ir.actions.act_window_close'}
        return result
#         gt_sale_commission_extension
    @api.multi
    def commission_count_entries(self, commission_amt, sale_person, journal_ref):
#         commission_journal = self.env['ir.values'].get_default('account.config.settings', 'commission_journal')
        commission_journal = int(self.env['ir.config_parameter'].search([('key','=','account.commission_journal')]).value)
        print("commission_journalcommission_journal",commission_journal)
        account_obj = self.env['account.journal']
        account_id = account_obj.browse(commission_journal)
        prod_commissinable_amt = sum([i.price_subtotal for i in self.order_line if i.product_id.flag])
        if self.env.context.get('showroom_partner_contxt'):
            commission_pay = 'showroom_partner'
            comm_partner_id = sale_person.id
            sales_user = False
            commission_partner = comm_partner_id
        else:
            commission_pay = 'salesperson'
            comm_partner_id = sale_person.partner_id.id
            sales_user = sale_person.id
            commission_partner = False
    
        commission_vals = {
            'name': account_id.default_debit_account_id.name,
            'debit': commission_amt,
            'credit': 0.0,
            'account_id': account_id.default_debit_account_id.id,
            'partner_id': comm_partner_id,
            'commission_user': sales_user,
            'commission_on': self.order_pay_state,
            'commission_pay': commission_pay,
            'sale_order_id': self.id,
            'commission_partner': commission_partner,
            'commissionable_amount': prod_commissinable_amt,
            'commission_rate': self.x_showroom.commissions_percentage,
            'total_commission': commission_amt,
            'sales_commission_rate': self.x_showroom.commissions_percentage,
        }
        sale_person_vals = {
            'name': sale_person.name,
            'debit': 0.0,
            'credit': commission_amt,
            'account_id': sale_person.property_account_payable_id.id,
            'partner_id': comm_partner_id,
            'commission_user': sales_user,
            'commission_on': self.order_pay_state,
            'commission_pay': commission_pay,
            'sale_order_id': self.id,
            'commission_partner': commission_partner,
            'commissionable_amount': prod_commissinable_amt,
            'commission_rate': self.x_showroom.commissions_percentage,
            'total_commission': commission_amt,
            'sales_commission_rate': self.x_showroom.commissions_percentage,
        }
        now = datetime.now()
        vals = {
            'journal_id': account_id.id,
            'date': now.strftime('%Y-%m-%d'),
            'state': 'draft',
            'line_ids': [(0, 0, commission_vals), (0, 0, sale_person_vals)],
            'ref': journal_ref,
            'partner_id': comm_partner_id,
            'commission_user': sales_user,
            'commission_partner': commission_partner,
        }
        move = self.env['account.move'].create(vals)
        # move.post()
        return move.id
    
#     @api.multi
#     def action_confirm(self):
#         res = super(SaleOrder, self).action_confirm()
#         for order in self:
#             order.state = 'sale'
#             order.confirmation_date = fields.Datetime.now()
#             if self.env.context.get('send_email'):
#                 self.force_quotation_send()
#             order.order_line._action_procurement_create()
#         if self.env['ir.values'].get_default('res.config.settings', 'auto_done_setting'):
#             self.action_done()
            
    @api.multi
    def action_confirm(self):
        res = super(SaleOrder, self).action_confirm()
        ctx = self.env.context.copy()
        #Commission Calculation Code
        for order in self:
#             commission_type = self.env['ir.config_parameter'].get_default('sale.config.settings', 'commission_calculation')
            commission_type = self.env['ir.config_parameter'].search([('key', '=', 'gt_sale_commission_extension.sale.commission_calculation')]).value
            print("commission_typecommission_typecommission_type????",commission_type)
            if order.order_pay_state == '0':
                if commission_type == '0':
                    total_order_commission = 0
                    for line_id in order.order_line:
                        if line_id.product_id.prod_comm_per and line_id.product_id.prod_comm_per > 0:
                            product_commission = (
                                                 line_id.price_subtotal * line_id.product_id.prod_comm_per) / 100
                            total_order_commission += product_commission
                    order.commission_count_entries(total_order_commission, order.user_id, order.name)
                if commission_type == '1':
                    total_order_commission = 0
                    for line_id in order.order_line:
                        if line_id.product_id.categ_id.prod_categ_comm_per and line_id.product_id.categ_id.prod_categ_comm_per > 0:
                            product_categ_commission = (
                                                       line_id.price_subtotal * line_id.product_id.categ_id.prod_categ_comm_per) / 100
                            total_order_commission += product_categ_commission
                    order.commission_count_entries(total_order_commission, order.user_id, order.name)

                if commission_type == '2':
                    if order.team_id.sale_manage_comm and order.team_id.sale_manage_comm > 0:
                        manager_commission = (order.amount_untaxed * order.team_id.sale_manage_comm) / 100
                        order.commission_count_entries(manager_commission, order.team_id.user_id, order.name)
                    if order.team_id.sale_member_comm and order.team_id.sale_member_comm > 0:
                        member_commission = (order.amount_untaxed * order.team_id.sale_member_comm) / 100
                        order.commission_count_entries(member_commission, order.user_id, order.name)
                
                #Showroom Partner Commission
                if commission_type == '3':
                    ctx.update({'showroom_partner_contxt': True})
                    total_order_commission = 0
                    commissionable_so = False
                    for line_id in order.order_line:
                        if order.x_showroom and order.x_showroom.commissions_percentage > 0 and line_id.product_id.flag: #self.x_showroom
                            product_commission = (
                                                 line_id.price_subtotal * order.x_showroom.commissions_percentage) / 100
                            total_order_commission += product_commission
                            commissionable_so = True
                    if commissionable_so:
                        if order.x_showroom:
                            order.with_context(ctx).commission_count_entries(total_order_commission, order.x_showroom, order.name)
                        else:
                            raise UserError(_("Please select Showroom Partner in SaleOrder for the commission !"))
                    
            if order.order_pay_state == '2':
                if commission_type == '0':
                    total_order_commission = 0
                    for line_id in self.order_line:
                        if line_id.product_id.prod_comm_per and line_id.product_id.prod_comm_per > 0:
                            product_commission = (
                                                 line_id.price_subtotal * line_id.product_id.prod_comm_per) / 100
                            total_order_commission += product_commission
                    order.write({'commission_count_member': total_order_commission})

                if commission_type == '1':
                    total_order_commission = 0
                    for line_id in self.order_line:
                        if line_id.product_id.categ_id.prod_categ_comm_per and line_id.product_id.categ_id.prod_categ_comm_per > 0:
                            product_categ_commission = (
                                                       line_id.price_subtotal * line_id.product_id.categ_id.prod_categ_comm_per) / 100
                            total_order_commission += product_categ_commission
                    order.write({'commission_count_member': total_order_commission})

                if commission_type == '2':
                    if order.team_id.sale_manage_comm and order.team_id.sale_manage_comm > 0:
                        manager_commission = (order.amount_untaxed * order.team_id.sale_manage_comm) / 100
                        order.write({'commission_count_manage': manager_commission})

                    if order.team_id.sale_member_comm and order.team_id.sale_member_comm > 0:
                        member_commission = (order.amount_untaxed * order.team_id.sale_member_comm) / 100
                        order.write({'commission_count_member': member_commission})
        
        return res
