# -*- coding: utf-8 -*-

from openerp import fields,models ,api, _
import time

class PayableCommission(models.AbstractModel):
    _inherit = 'commission.report'

    def get_movelines(self, commission_report_id):
        wizard_obj = self.env['sale.commission.report']
        wizard_search = wizard_obj.search([('id', '=', commission_report_id)])
        salesman = wizard_search.sale_person
        move_line_obj = self.env['account.move.line']
        if wizard_search.commission_pay == 'salesperson':
            move_line_search = move_line_obj.search([('commission_user','=',salesman.id), ('paid','=',False),('account_id.user_type_id.name', '=', 'Payable'), ('date', '>=', wizard_search.start_date), ('date', '<=', wizard_search.end_date)])
        else:
            move_line_search = move_line_obj.search([('commission_partner', '=', wizard_search.partner_id.id), ('account_id.user_type_id.name', '=', 'Payable'), ('paid', '=', False), ('credit', '!=', 0)])
        return move_line_search

    @api.model
    def render_html(self, docids, data=None):
        wizard_obj = self.env['sale.commission.report']
        wizard_search = wizard_obj.search([('id','=',docids)])
        ctx = self._context.copy()
        ctx.update({'active_ids': docids})
        self.context = ctx
        docargs = {
            'doc_ids': docids,
            'doc_model': 'res.partner',
            'docs': self.context['active_ids'],
            'sale_person': wizard_search.sale_person,
            'commission_pay': wizard_search.commission_pay,
            'partner_id': wizard_search.partner_id,
            'data': {'docs': docids},
            'get_movelines': self.get_movelines,
            'currency_id': self.env.user.company_id.currency_id,
        }
        return self.env['report'].render('sale_commission_gt.payable_commission_report', docargs)


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: