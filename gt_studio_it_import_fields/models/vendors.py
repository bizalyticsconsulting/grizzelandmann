from odoo import models, fields, api

class Vendors(models.Model):

    _inherit= 'res.partner'

    fob = fields.Char("FOB")
    specifier = fields.Char("Specifier")
    federal_id =  fields.Char("Federal Id")
    social_security = fields.Char("Social Security ")
    entered_date = fields.Datetime("Entered Date")
    changed_by =fields.Boolean("Changed By")
    changed_date = fields.Datetime("Changed Date")
    sales_vendor_tax = fields.Boolean("Taxable Selling")
    freightable_tax = fields.Boolean("Taxable Freight")
    crating_tax = fields.Boolean("Taxable Crating")
    other_tax = fields.Boolean("Taxable Other")
    client_ship_via = fields.Many2one('delivery.carrier','Client Ship Via')
    client_terms = fields.Many2one('account.payment.term','Client terms')
    client_freight_terms = fields.Many2one('account.payment.term','Client Freight Terms')
    sidemark = fields.Char("Sidemark")
    partner_type = fields.Selection(
        [('showroom', 'Showroom'),('client','Client'), ('ship to','Ship To'),('vendor', 'Vendor'), ('specifier', 'Specifier'), ('designer', 'Designer'),
         ('other', 'Other'),('office','Office'),('purchasing','Purchasing'),('personal','personal'),('project', 'Project'),('prospect','Prospect')],
        string="Type")
