from odoo import models, fields, api
from odoo.exceptions import UserError

class AccountInvoiceAdditionalCharge(models.Model):
    _inherit = 'account.invoice'

    amount_charges = fields.Monetary(compute='_compute_amount', string='Total Charges')
    # amount_charges_tax = fields.Monetary(compute='_compute_amount', string='Charges Tax', readonly=True)
    freight_total = fields.Monetary(compute='_compute_amount', string='Freight', readonly=True)
    crating_total = fields.Monetary(compute='_compute_amount', string='Crating', readonly=True)
    install_total = fields.Monetary(compute='_compute_amount', string='Install', readonly=True)
    other_char_total = fields.Monetary(compute='_compute_amount', string='Other Charges', readonly=True)


    @api.one
    @api.depends('invoice_line_ids.price_subtotal', 'tax_line_ids.amount', 'currency_id', 'company_id', 'date_invoice',
                 'type')
    def _compute_amount(self):
        order_id = self.env['sale.order'].search([('name','=',self.origin)])
        round_curr = self.currency_id.round
        self.amount_untaxed = sum(line.price_subtotal for line in self.invoice_line_ids)
        self.amount_charges = sum(line.amount_charges for line in order_id if line.amount_charges)
        self.amount_tax = sum(round_curr(line.amount) for line in self.tax_line_ids)
        # self.amount_charges_tax = sum(round_curr(line.amount_charges_tax) for line in order_id)
        self.freight_total = sum(round_curr(line.freight_total) for line in order_id)
        self.crating_total = sum(round_curr(line.crating_total) for line in order_id)
        self.install_total = sum(round_curr(line.install_total) for line in order_id)
        self.other_char_total = sum(round_curr(line.other_char_total) for line in order_id)
        self.amount_total = self.amount_untaxed + self.amount_tax + self.amount_charges
        amount_total_company_signed = self.amount_total
        amount_untaxed_signed = self.amount_untaxed
        if self.currency_id and self.company_id and self.currency_id != self.company_id.currency_id:
            currency_id = self.currency_id.with_context(date=self.date_invoice)
            amount_total_company_signed = currency_id.compute(self.amount_total, self.company_id.currency_id)
            amount_untaxed_signed = currency_id.compute(self.amount_untaxed, self.company_id.currency_id)
        sign = self.type in ['in_refund', 'out_refund'] and -1 or 1
        self.amount_total_company_signed = amount_total_company_signed * sign
        self.amount_total_signed = self.amount_total * sign
        self.amount_untaxed_signed = amount_untaxed_signed * sign

    @api.model
    def invoice_line_move_line_get(self):
        res = []
        for line in self.invoice_line_ids:
            if line.quantity == 0:
                continue
            tax_ids = []
            for tax in line.invoice_line_tax_ids:
                tax_ids.append((4, tax.id, None))
                for child in tax.children_tax_ids:
                    if child.type_tax_use != 'none':
                        tax_ids.append((4, child.id, None))
            analytic_tag_ids = [(4, analytic_tag.id, None) for analytic_tag in line.analytic_tag_ids]

            move_line_dict = {
                'invl_id': line.id,
                'type': 'src',
                'name': line.name.split('\n')[0][:64],
                'price_unit': line.price_unit,
                'quantity': line.quantity,
                'price': line.price_subtotal + line.freight + line.crating + line.install + line.other_charges,
                'account_id': line.account_id.id,
                'product_id': line.product_id.id,
                'uom_id': line.uom_id.id,
                'account_analytic_id': line.account_analytic_id.id,
                'tax_ids': tax_ids,
                'invoice_id': self.id,
                'analytic_tag_ids': analytic_tag_ids
            }
            res.append(move_line_dict)
        return res

    @api.model
    def tax_line_move_line_get(self):
        res = []
        # keep track of taxes already processed
        done_taxes = []
        # loop the invoice.tax.line in reversal sequence
        for tax_line in sorted(self.tax_line_ids, key=lambda x: -x.sequence):
            if tax_line.amount:
                tax = tax_line.tax_id
                if tax.amount_type == "group":
                    for child_tax in tax.children_tax_ids:
                        done_taxes.append(child_tax.id)
                res.append({
                    'invoice_tax_line_id': tax_line.id,
                    'tax_line_id': tax_line.tax_id.id,
                    'type': 'tax',
                    'name': tax_line.name,
                    'price_unit': tax_line.amount,
                    'quantity': 1,
                    'price': tax_line.amount,
                    'account_id': tax_line.account_id.id,
                    'account_analytic_id': tax_line.account_analytic_id.id,
                    'invoice_id': self.id,
                    'tax_ids': [(6, 0, list(done_taxes))] if tax_line.tax_id.include_base_amount else []
                })
                done_taxes.append(tax.id)
        return res


class AccountInvoiceLineSubtotal(models.Model):

    _inherit = 'account.invoice.line'

    total_charges = fields.Monetary(compute='_compute_price', string='Total Charges')
    freight = fields.Integer(string="Freight Charges")
    crating = fields.Integer(string="Crating Charges",)
    install = fields.Integer(string="Installation Charges",)
    other_charges = fields.Integer(string="Other Charges",)

    @api.one
    @api.depends('price_unit', 'discount', 'invoice_line_tax_ids', 'quantity',
                 'product_id', 'invoice_id.partner_id', 'invoice_id.currency_id', 'invoice_id.company_id',
                 'invoice_id.date_invoice', 'invoice_id.date')
    def _compute_price(self):
        sale_ids = self.env['sale.order'].search([('name','=',self.invoice_id.origin)])
        if sale_ids:
            for line in sale_ids[0].order_line:
                self.total_charges =  line.total_charges if line.total_charges else 0.0
        currency = self.invoice_id and self.invoice_id.currency_id or None
        price = self.price_unit * (1 - (self.discount or 0.0) / 100.0)
        taxes = False
        if self.invoice_line_tax_ids:
            taxes = self.invoice_line_tax_ids.compute_all(price, currency, self.quantity, product=self.product_id,
                                                          partner=self.invoice_id.partner_id)
        self.price_subtotal = price_subtotal_signed = taxes['total_excluded']  if taxes else self.quantity * price
        if self.invoice_id.currency_id and self.invoice_id.company_id and self.invoice_id.currency_id != self.invoice_id.company_id.currency_id:
            #In odoo-10 _get_currency_rate_date() method is not found so that, changes this code as date_invoice or date 20.02.2019
            # price_subtotal_signed = self.invoice_id.currency_id.with_context(date=self.invoice_id._get_currency_rate_date()).compute(price_subtotal_signed,self.invoice_id.company_id.currency_id)
            price_subtotal_signed = self.invoice_id.currency_id.with_context(date=self.invoice_id.date_invoice or self.invoice_id.date).compute(price_subtotal_signed,self.invoice_id.company_id.currency_id)
        sign = self.invoice_id.type in ['in_refund', 'out_refund'] and -1 or 1
        self.price_subtotal_signed = price_subtotal_signed * sign



