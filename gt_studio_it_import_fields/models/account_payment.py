# -*- coding: utf-8 -*-

from odoo import models, fields, api

class CheckNumberForPayment(models.Model):

    _inherit= 'account.payment'

    new_check_number = fields.Char('Check Number')

    @api.model
    def default_get(self, fields):
        res = super(CheckNumberForPayment, self).default_get(fields)
        if self.env.context.get('check_number'):
            res.update({'new_check_number': self.env.context.get('check_number')})
        if 'deferred_bool' in self.env.context and self.env.context.get('deferred_bool') == False:
            res.update({'deferred_bool': self.env.context.get('deferred_bool')})
        return res