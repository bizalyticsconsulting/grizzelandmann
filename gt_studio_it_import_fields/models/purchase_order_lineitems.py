from odoo import models, fields, api

class PurchaseOrderLineItems(models.Model):

    _inherit = 'purchase.order.line'

    sequence = fields.Integer("Item Sequence")
    entered_by = fields.Many2one('res.users','Entered By')
    entered_date = fields.Date('Entered Date')
    # new_name = fields.Html('Description')