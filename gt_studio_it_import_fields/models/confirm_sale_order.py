from odoo import models, fields, api
from odoo.exceptions import UserError

class ConfirmSaleOrder(models.Model):

    _inherit= 'sale.order'


    client_ship_via = fields.Many2one('delivery.carrier','Client Ship Via')
    client_terms = fields.Many2one('account.payment.term','Client Terms')
    client_freight_terms = fields.Many2one('account.payment.term','Client Freight Terms')
    client = fields.Char("Client")
    specifier_client = fields.Many2one('res.partner','Specifier Client')
    specifier = fields.Char("Specifier")
    amount_charges = fields.Monetary(compute='_amount_all', string='Total Charges')
    freight_total = fields.Monetary(compute='_amount_all',string='Freight',readonly=True)
    crating_total = fields.Monetary(compute='_amount_all',string='Crating',readonly=True)
    install_total = fields.Monetary(compute='_amount_all',string='Install',readonly=True)
    other_char_total = fields.Monetary(compute='_amount_all',string='Other Charges',readonly=True)
    # x_order_shipdate = fields.Datetime(_compute='_compute_ship_date',string='Ship Date')
    is_picking_completed = fields.Boolean(string='Picking Done')
    x_order_shipdate = fields.Date(string="Ship Date", help="Sales Order Ship Date",readonly=True)

    @api.depends('order_line.price_total')
    def _amount_all(self):
        """
        Compute the total amounts of the SO.
        """
        for order in self:
            amount_untaxed = amount_tax = amount_charges = 0.0
            crating = freight = install = other_charges = 0.0
            for line in order.order_line:
                amount_untaxed += line.price_subtotal
                amount_charges += line.total_charges
                if line.crating or line.freight or line.install or line.other_charges:
                    crating += line.crating if line.crating else 0.0
                    freight += line.freight if line.freight else 0.0
                    install += line.install if line.install else 0.0
                    other_charges += line.other_charges if line.other_charges else 0.0
                # FORWARDPORT UP TO 10.0
                if order.company_id.tax_calculation_rounding_method == 'round_globally':
                    price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
                    taxes = line.tax_id.compute_all(price, line.order_id.currency_id, line.product_uom_qty,
                                                    product=line.product_id, partner=order.partner_shipping_id)
                    amount_tax += sum(t.get('amount', 0.0) for t in taxes.get('taxes', []))
                else:
                    amount_tax += line.price_tax
            order.update({
                'amount_untaxed': order.pricelist_id.currency_id.round(amount_untaxed),
                'amount_charges': amount_charges,
                'amount_tax': order.pricelist_id.currency_id.round(amount_tax),
                'amount_total': amount_untaxed + amount_tax + amount_charges,
                'freight_total': freight,
                'crating_total': crating,
                'install_total': install,
                'other_char_total': other_charges,
            })


    # @api.one
    # def _get_approval_for_admin(self):
    #     if self.env.ref('base.group_system'):
    #         self.is_picking_completed == True
    #         print "llllllllllllllllllll"
    #     else:
    #         self.is_picking_completed == False