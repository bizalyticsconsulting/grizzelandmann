from odoo import models, fields, api
from odoo.exceptions import UserError


class SalesPurchaseTaxLocation(models.Model):

    _inherit= 'sale.order.line'

    freight = fields.Integer("Freight Charges ")
    crating = fields.Integer("Crating Charges",)
    install = fields.Integer("Installation Charges",)
    other_charges = fields.Integer("Other Charges",)
    sales_tax = fields.Integer("Sales Tax")
    entered_by = fields.Many2one('res.users', 'Entered By')
    total_charges = fields.Monetary(compute='_compute_total_charges', string='Total Charges')
    entered_date = fields.Date('Entered Date')


    @api.depends('freight','crating','install','other_charges')
    def _compute_total_charges(self):
        for line in self:
            total = 0.0
            if line.crating or line.freight or line.install or line.other_charges:
                crating = line.crating if line.crating else 0.0
                freight = line.freight if line.freight else 0.0
                install = line.install if line.install else 0.0
                other_charges = line.other_charges if line.other_charges else 0.0
                total = crating + freight + install + other_charges
            line.update({
                'total_charges':total
            })

#    @api.depends('product_uom_qty', 'discount', 'price_unit', 'tax_id')
#    def _compute_amount(self):
#        """
#        Compute the amounts of the SO line.
        #"""
#        for line in self:
#            total = 0.0
#            price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
#            if line.crating or line.freight or line.install or line.other_charges:
#                crating = line.crating if line.crating else 0.0
#                freight = line.freight if line.freight else 0.0
#                install = line.install if line.install else 0.0
#                other_charges = line.other_charges if line.other_charges else 0.0
#                total = crating + freight + install + other_charges
#            taxes = line.tax_id.compute_all(price, line.order_id.currency_id, line.product_uom_qty,
#                                            product=line.product_id, partner=line.order_id.partner_shipping_id)
#            line.price_subtotal = taxes['total_excluded']
#            line.update({
#                'price_tax': taxes['total_included'] - taxes['total_excluded'],
#                'price_total': taxes['total_included'],
#            })

    @api.multi
    def _prepare_invoice_line(self, qty):
        res = super(SalesPurchaseTaxLocation, self)._prepare_invoice_line(qty)
        res.update({
            'freight':self.freight,
            'crating':self.crating,
            'install':self.install,
            'other_charges':self.other_charges
        })
        return res

