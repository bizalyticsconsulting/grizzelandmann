from odoo import api,models

class StockForShipDate(models.Model):
    _inherit = 'stock.picking'
#    Globalteckz

#    @api.multi
#    def do_new_transfer(self):
#        res = super(StockForShipDate, self).do_new_transfer()
#        sale_order = self.env['sale.order'].search([('name', '=', self.origin)])
#        print sale_order
#        for pick in self:
#            if pick.state == 'done':
#                sale_order.write({
#                    'x_order_shipdate': pick.min_date.date(),
#                    'is_picking_completed': True
#                })
#        return res

#    Globalteckz
    @api.multi
    def button_validate(self):
        res = super(StockForShipDate, self).button_validate()
        sale_order = self.env['sale.order'].search([('name', '=', self.origin)],limit=1)
        sale_order.write({
            'x_order_shipdate': self.scheduled_date.date(),
            'is_picking_completed': True
        })
        return res
