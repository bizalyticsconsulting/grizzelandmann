from odoo import models, fields, api

class PurchaseOrder(models.Model):

    _inherit= 'purchase.order'

    client_ship_via = fields.Many2one('delivery.carrier', 'Client Ship Via')
    client_terms = fields.Many2one('account.payment.term', 'Client Terms')
    client_freight_terms = fields.Many2one('account.payment.term', 'Client Freight Terms')
    client = fields.Char("Client")
    specifier = fields.Char("Specifier")
    ship_via = fields.Many2one('delivery.carrier', 'Ship Via')
    freight_terms = fields.Many2one('account.payment.term', 'Freight Terms')
    specifier_client = fields.Many2one('res.partner','Specifier Client')
    salesperson_1 = fields.Many2one('res.users',"Salesperson")
    requested_date = fields.Char("Requested Date")
    sidemark = fields.Char("Sidemark")
    client_account = fields.Char("Client Account")
    client_po = fields.Char("Client PO")
    purchaser_showroom = fields.Many2one('res.partner','Showroom')

