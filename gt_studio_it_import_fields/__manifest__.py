# -*- coding: utf-8 -*-
{
    'name': "gt_studio_it_import_fields",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Globalteckz",
    'website': "http://www.globalteckz.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Generic',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','sale','delivery','purchase','account'],
#    wk_sale_commission  in depends
    # always loaded
    'data': [
        'views/ship_via_delivery_view.xml',
        'views/tax_location_view.xml',
        'views/vendor_views.xml',
        'views/confirm_sale_order_views.xml',
        'views/sale_order_lineitem_views.xml',
        'views/purchase_order_views.xml',
        'views/purchase_order_line_views.xml',
        'views/invoice_lineitem_views.xml',
        'views/account_payment_check.xml',
        'views/product_temlpate_views.xml',
    ],
}