# -*- coding: utf-8 -*-
##############################################################################
#
#    Globalteckz Pvt Ltd
#    Copyright (C) 2013-Today(www.globalteckz.com).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import fields, models ,api
import base64
import xlrd
import urllib.request
from datetime import datetime

class ProductImportWizard(models.TransientModel):
    _name = 'product.import.wizard'
    _description = 'Product Import Wizard'

    data_file = fields.Binary(string="File")

    @api.multi
    def import_product_order(self):
#        Objects
        log_obj = self.env['log.management']
        Partner=self.env['res.partner']
        Template=self.env['product.template']
        Category=self.env['product.category']
        Uom=self.env['uom.uom']
        Uom_categ=self.env['uom.category']
        product_fields=Template.fields_get()
        
#       Fetch default values for the fields
        pro_def_val=Template.default_get(product_fields)
        new_pro_up = pro_def_val.copy()
        
        file_datas = base64.decodestring(self.data_file)
        workbook = xlrd.open_workbook(file_contents=file_datas)
        
#        Read worksheet
        sheet = workbook.sheet_by_index(0)
#        Prepare list of values

        data = [[sheet.cell_value(r, c) for c in range(sheet.ncols)] for r in range(sheet.nrows)]

#       Remove header
        data.pop(0)

        for row in data:
            categ_id=Category.search([('name','=',row[2])],limit=1)
            uom_id=Uom.search([('name','=ilike',row[5])],limit=1)
            uom_categ_id=Uom_categ.search([('name','=','Unit')],limit=1)
            partner_id=Partner.search([('name','=',row[10]),('supplier','=',True),('active','=',True)],limit=1)
            if partner_id:
                partner_code=partner_id.partner_code
                partner_id=partner_id.id
            else:
                partner_id=Partner.create({'name':row[10],'partner_code':row[10],'supplier':True,'customer':False,'company_type':'company'})
                partner_code=partner_id.partner_code
                partner_id=partner_id.id
            if categ_id:
                categ_id=categ_id.id
            else:
                categ_id=Category.create({'name':row[2]}).id
            if uom_id:
                uom_id=uom_id.id
            else:
                uom_id=Uom.create({'name':row[5],'category_id':uom_categ_id.id}).id

            image_path = row[14]
            image_base64 = ""
            if "http://" in image_path or "https://" in image_path:
                try:
                    link = urllib.request.urlopen(image_path).read()
                    image_base64 = base64.encodebytes(link)
                except:
                    print ("Please provide correct URL for product '%s' or check your image size.!", row[8])
            else:
                try:
                    with open(image_path, 'rb') as image:
                        image_base64 = image.read().encode("base64")
                except:
                    print ("Path not correct for product '%s'", row[8])

            new_pro_up.update({
                'name':row[0],
                'product_default_code':row[8],
                'default_code_option':int(row[9]) if isinstance(row[9],float) else row[9],
                'default_code': (str(partner_code.encode('ascii', 'ignore').decode('ascii')) if partner_code else '')+'-'+str(row[8])+'-'+str(int(row[9]) if isinstance(row[9],float) else row[9]),
                'type': row[3],
                'list_price': row[6],
                'standard_price': row[7],
                'barcode':(int(row[4]) if isinstance(row[4],(int,float)) else row[4]) if row[4] else False,
                'categ_id':categ_id,
                'uom_id' : uom_id,
                'image_medium': image_base64,
                'image_small': image_base64,
                'image': image_base64,
                'uom_po_id' : uom_id,
                'active':True if row[13] in (True,1,'TRUE') else False,
                'description_sale' : row[11] if row[11] else '',
                'description_purchase' : row[12] if row[12] else '',
                'seller_ids' : [(0,0,{'name':partner_id,'price':row[7]})],
            })
            p_id=Template.search(['|',('active','=',True),('active','=',False),('product_default_code','=',row[8]),('default_code_option','=',int(row[9]) if isinstance(row[9],float) else row[9])],limit=1)
            if p_id:
                if not p_id.active:
                    p_id.write({'active':True})
                    p_id.env.cr.commit() 
                if p_id.seller_ids:
                    p_id.seller_ids.unlink()
                p_id.write(new_pro_up)
                p_id.env.cr.commit() # one record commited
                log_vals = {'operation': 'p_purchased',
                        'message': 'Updated product with name %s '%(p_id.name), 'date': datetime.now()}
                log_obj.create(log_vals)
            else:
                p_id=Template.create(new_pro_up)
                log_vals = {'operation': 'p_purchased',
                        'message': 'Imported product with name %s '%(p_id.name), 'date': datetime.now()}
                log_obj.create(log_vals)
                p_id.env.cr.commit() # one record commited
                
           






































