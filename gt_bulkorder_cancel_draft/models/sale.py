# -*- encoding: utf-8 -*-
##############################################################################
#
# Globalteckz
# Copyright (C) 2012 (http://www.globalteckz.com)
#
##############################################################################

from odoo import fields,api,models,_

class SaleOrder(models.Model):
    _inherit = "sale.order"

    @api.multi
    def action_cancel(self):
        context = self.env.context.copy()
        for order in self:
            if order.state != 'cancel':
                #If any Invoice then check for payment. if payment registered then unconciled it.
                if order.invoice_ids:
                    for inv in order.invoice_ids:
                        context.update({'invoice_id': inv.id})
                        if inv.state != 'cancel':
                            for pay_move_line in inv.payment_move_line_ids:
                                pay_move_line.with_context(context).remove_move_reconcile()
                            inv.action_invoice_cancel() # Cancel Invoice(open State)
                super(SaleOrder, order).action_cancel()   # Cancel Order
                #unlink Commission and lines
                # if order.commission_ids:
                #     for comm in order.commission_ids:
                #         comm.commission_lines.unlink()
                #         comm.unlink()
                order.action_draft()    # Set Draft
                
        return True

# class commission_commission(models.Model):
#     _inherit= "commission.commission"
    
#     state = fields.Selection([('draft','Draft'),('confirm','Confirm'),('invoice','Invoiced'),('paid','Paid'),('done','Done'), ('cancel','Cancel')], string="State", default="draft",copy=False)
    
# class commission_lines(models.Model):
#     _inherit = "commission.lines"
    
#     state = fields.Selection([('draft','Draft'),('confirm','Confirm'),('invoice','Invoiced'),('paid','Paid'),('done','Done'), ('cancel','Cancel')], related='commission_id.state', string='Order Status', readonly=True, copy=False, store=True, default='draft')
#     