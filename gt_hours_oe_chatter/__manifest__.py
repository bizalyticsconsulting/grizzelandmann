# -*- coding: utf-8 -*-
##############################################################################
#
#    Globalteckz
#    Copyright (C) 2013-Today Globalteckz (http://www.globalteckz.com)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


{
    'name': 'oe_chatter',
    'version': '12.0.0.1',
    'category': 'View',
    'sequence': 1,
    'author': 'globalteckz',
    'website': 'http://www.globalteckz.com',
    'summary': '',
    'description': """

It will show actual timestamp and date instead of X hours ago in oe_chatter
=========================

    """,
    'depends': ['mail','web'],
    'data': [
            'static/src/xml/thread.xml',
            ],
    'application': True,
    'installable': True,
    'auto_install': False,
    'qweb': [
            ],
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
