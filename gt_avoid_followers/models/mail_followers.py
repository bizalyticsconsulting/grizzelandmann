# -*- encoding: utf-8 -*-
##############################################################################
#
# Globalteckz
# Copyright (C) 2012 (http://www.globalteckz.com)
#
##############################################################################

from odoo import models ,api

class Followers(models.Model):
    _inherit = 'mail.followers'
    
    @api.model_create_multi
    def create(self, vals):
        return True