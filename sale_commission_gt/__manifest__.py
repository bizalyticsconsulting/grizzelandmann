 # -*- coding: utf-8 -*-
##############################################################################
#
#    Globalteckz Pvt Ltd
#    Copyright (C) 2013-Today(www.globalteckz.com).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': 'Sales Commission by Sales/Invoice/Payments',
    'version': '1.0',
    'category': 'Sale',
    'sequence': 2,
    'summary': 'Helps you to manage your Sales Commission by Sales/Invoice/Payments',
    'description': """
Odoo Sales Commission/Incentive Calculations Based on Sales, Payments Received & Invoicing,Easily Manage Complex Commission or Incentive Plans to Enhance Corporate Goals 

""",
    'author': 'Globalteckz',
    'website': 'https://www.globalteckz.com',
    'depends': ['sale_management','account'],
    'data': [
        'report/payable_commission_report.xml',
        'report/paid_commission_report.xml',
        'report/all_commission_report.xml',
        'report/report_menu.xml',
        'view/account_view.xml',
        'view/sale_view.xml',
        'view/commission_view.xml',
        'view/sale_commission_view.xml',
        'wizard/commission_report_views.xml'
    ],
    'demo': [],
    'test': [],
    'installable': True,
    'application': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
