# -*- coding: utf-8 -*-
##############################################################################
#
#    Globalteckz Pvt Ltd
#    Copyright (C) 2013-Today(www.globalteckz.com).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#    #378401
##############################################################################

from odoo import fields, models, api, _
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError
from datetime import datetime


class SaleCommission(models.TransientModel):
    _name = 'sale.commission'
    _description = 'Sale Commission'

    @api.onchange('sale_person')
    def _get_move_lines(self):
        move_line_obj = self.env['account.move.line']
        move_line_search = move_line_obj.search([('commission_user','=',self.sale_person.id), ('account_id.user_type_id.name', '=', 'Payable'), ('paid', '=', False), ('credit', '!=', 0)])
        total_amount = 0
        for id in move_line_search:
            total_amount += id.credit
        if self.sale_person:
            self.move_lines = move_line_search.ids
            self.payment_amt = total_amount

    sale_person = fields.Many2one('res.users', 'Sale Person')
    # commission_journal = fields.Many2one('account.journal', 'Commission Journal', default=lambda self: self.env['ir.config_parameter'].get_default('res.config.settings', 'commission_journal'))
    commission_journal = fields.Many2one('account.journal', 'Commission Journal', default=lambda self: int(self.env['ir.config_parameter'].sudo().get_param('account.commission_journal')))
    payment_journal = fields.Many2one('account.journal', 'Payment Journal')
    payment_amt = fields.Float('Payment Amount')
    move_lines = fields.One2many('account.move.line', 'commission_user', string='Commission Lines')
    # payment_currency = fields.Many2one('res.currency', 'Payment Currency', default=lambda self: self.company_id.currency_id or self.env.user.company_id.currency_id)


    @api.multi
    def do_payment(self):
        move_line_obj = self.env['account.move.line']
        move_line_search = move_line_obj.search([('commission_user', '=', self.sale_person.id), ('account_id.user_type_id.name', '=', 'Payable'), ('paid', '=', False), ('credit', '!=', 0)])
        total_amount = 0
        for id in move_line_search:
            total_amount += id.credit
            id.write({'paid': True})

        commission_vals = {
            'name': self.payment_journal.default_credit_account_id.name,
            'credit': total_amount,
            'debit': 0.0,
            'account_id': self.payment_journal.default_credit_account_id.id,
            'partner_id': self.sale_person.partner_id.id,
            'commission_user': self.sale_person.id,
        }
        sale_person_vals = {
            'name': self.sale_person.name,
            'credit': 0.0,
            'debit': total_amount,
            'account_id': self.sale_person.property_account_payable_id.id,
            'partner_id': self.sale_person.partner_id.id,
            'commission_user': self.sale_person.id,
        }
        now = datetime.now()
        vals = {
            'journal_id': self.payment_journal.id,
            'date': now.strftime('%Y-%m-%d'),
            'state': 'draft',
            'line_ids': [(0, 0, commission_vals), (0, 0, sale_person_vals)],
            'ref': '',
            'partner_id': self.sale_person.partner_id.id,
            'commission_user': self.sale_person.id
        }
        move = self.env['account.move'].create(vals)
        move.post()
        return move.id


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: