# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT
import odoo.addons.decimal_precision as dp

class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    hide_invoice = fields.Boolean(string="Hide Invoice",default=False)
    
  
    @api.multi
    def copy(self, default=None):
        duplicate_po = super(PurchaseOrder, self).copy(default=default)
        duplicate_po.hide_invoice = False
     
        return duplicate_po    

    def _prepare_invoice_line_from_po_line(self, line):
        account_obj = self.env['account.invoice']
        taxes = line.taxes_id
        invoice_line_tax_ids = line.order_id.fiscal_position_id.map_tax(taxes)
        invoice_line = self.env['account.invoice.line']
        data = {
            'purchase_line_id': line.id,
            'name': line.order_id.name+': '+line.name,
            'origin': line.order_id.origin,
            'uom_id': line.product_uom.id,
            'product_id': line.product_id.id,
            'account_id': invoice_line.with_context({'journal_id': account_obj.default_get(['journal_id'])['journal_id'], 'type': 'in_invoice'})._default_account(),
            'price_unit': line.price_unit,
            'quantity': line.qty_to_invoice,
            'discount': 0.0,
            'account_analytic_id': line.account_analytic_id.id,
            'analytic_tag_ids': line.analytic_tag_ids.ids,
            'invoice_line_tax_ids': invoice_line_tax_ids.ids
        }
        account = invoice_line.get_invoice_line_account('in_invoice', line.product_id, line.order_id.fiscal_position_id, account_obj.env.user.company_id)
        if account:
            data['account_id'] = account.id
        return data

    @api.multi
    def action_invoice_create(self, grouped=False, final=False):
        """
        Create the invoice associated to the SO.
        :param grouped: if True, invoices are grouped by SO id. If False, invoices are grouped by
                        (partner_invoice_id, currency)
        :param final: if True, refunds will be generated if necessary
        :returns: list of created invoices
        """
        inv_obj = self.env['account.invoice']
        inv_line_obj =  self.env['account.invoice.line']
        precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')
        invoices = {}

        for order in self:
            # group_key = order.id if grouped else (order.currency_id.id)
            inv_data = order._prepare_invoice()
            invoice = inv_obj.create(inv_data)
            for line in order.order_line:
                # if line.qty_invoiced > 0:
                #     line.invoice_line_create(line.qty_invoiced)


                invoice.update({'invoice_line_ids':[(0,0,self._prepare_invoice_line_from_po_line(line))]})
        return [inv.id for inv in invoices.values()]

    @api.multi
    def _prepare_invoice(self):
        """
        Prepare the dict of values to create the new invoice for a sales order. This method may be
        overridden to implement custom invoice generation (making sure to call super() to establish
        a clean extension chain).
        """
        self.ensure_one()
        journal_id = self.env['account.invoice'].default_get(['journal_id'])['journal_id']
        if not journal_id:
            raise UserError(('Please define an accounting sale journal for this company.'))
        invoice_vals = {
            'name': self.partner_ref or '',
            'origin': self.name,
            'type': 'in_invoice',
            'account_id': self.partner_id.property_account_receivable_id.id,
            'partner_id': self.partner_id.id,
            'partner_shipping_id': self.partner_id.id,
            'journal_id': journal_id,
            'payment_term_id': self.payment_term_id.id,
            'company_id': self.company_id.id,
        }
        return invoice_vals


    @api.multi
    def button_confirm(self):
        if not self.order_line:
            raise UserError(_("Please select Product lines to confirm the order"))
        res=super(PurchaseOrder,self).button_confirm()
        return res


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

   
    prepayment_account_id = fields.Many2one('account.account',"Prepayment Account")

    @api.multi
    def set_values(self):
        super(ResConfigSettings, self).set_values()
        IrDefault = self.env['ir.default'].sudo()
        IrDefault.set('res.config.settings', 'prepayment_account_id', self.prepayment_account_id.id)
        

    @api.model
    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        IrDefault = self.env['ir.default'].sudo()
        res.update(prepayment_account_id=IrDefault.get('res.config.settings', 'prepayment_account_id'))
        return res




class PurchaseOrderLine(models.Model):
    _inherit='purchase.order.line'



    @api.depends('qty_invoiced', 'qty_received', 'product_qty', 'order_id.state')
    def _get_to_invoice_qty(self):
        """
        Compute the quantity to invoice. If the invoice policy is order, the quantity to invoice is
        calculated from the ordered quantity. Otherwise, the quantity delivered is used.
        """
        for line in self:
            if line.order_id.state in ['purchase', 'done']:
                if line.product_id.invoice_policy == 'order':
                    line.qty_to_invoice = line.product_qty - line.qty_invoiced
                else:
                    line.qty_to_invoice = line.qty_received - line.qty_invoiced
            else:
                line.qty_to_invoice = 0

    qty_to_invoice = fields.Float(
        compute='_get_to_invoice_qty', string='To Invoice', store=True, readonly=True,
        digits=dp.get_precision('Product Unit of Measure'))                    

