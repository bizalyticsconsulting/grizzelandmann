import time
from odoo import models, fields, api, _
from odoo.exceptions import UserError
import odoo.addons.decimal_precision as dp

class PurchaseMakeInvoiceAdvance(models.TransientModel):
	_name = 'purchase.advance.payment.inv'
	_description = 'purchase advance payment invoice '


	@api.model
	def _count(self):
	    return len(self._context.get('active_ids', []))

	@api.model
	def _get_advance_payment_method(self):
	    if self._count() == 1:
	        purchase_obj = self.env['purchase.order']
	        order = purchase_obj.browse(self._context.get('active_ids'))[0]
	        if all([line.product_id.invoice_policy == 'order' for line in order.order_line]) or order.invoice_count:
	            return 'all'
	    return 'delivered'

	@api.model
	def _default_product_id(self):
	    product_id = self.env['ir.default'].get('res.config.settings', 'deposit_product_id_setting')
	    return self.env['product.product'].browse(product_id)

	@api.model
	def _default_deposit_account_id(self):
	    return self._default_product_id().property_account_income_id

	@api.model
	def _default_deposit_taxes_id(self):
	    return self._default_product_id().taxes_id

	advance_payment_method = fields.Selection([
	    ('delivered', 'Invoiceable lines'),
	    ('all', 'Invoiceable lines (deduct down payments)'),
	    ('percentage', 'Down payment (percentage)'),
	    ('fixed', 'Down payment (fixed amount)')
	    ], string='What do you want to invoice?', default=_get_advance_payment_method, required=True)
	product_id = fields.Many2one('product.product', string='Down Payment Product', domain=[('type', '=', 'service')],
	    default=_default_product_id)
	count = fields.Integer(default=_count, string='# of Orders')
	amount = fields.Float('Down Payment Amount', digits=dp.get_precision('Account'), help="The amount to be invoiced in advance, taxes excluded.")
	deposit_account_id = fields.Many2one("account.account", string="Expense Account", domain=[('deprecated', '=', False)],
	    help="Account used for deposits", default=_default_deposit_account_id)
	deposit_taxes_id = fields.Many2many("account.tax", string="Customer Taxes", help="Taxes used for deposits", default=_default_deposit_taxes_id)

	@api.onchange('advance_payment_method')
	def onchange_advance_payment_method(self):
	    if self.advance_payment_method == 'percentage':
	        return {'value': {'amount': 0}}
	    return {}




	@api.model
	def default_get(self, vals):
		res = super(PurchaseMakeInvoiceAdvance,self).default_get(vals)
		product_ids = self.env['ir.default'].get('res.config.settings', 'prepayment_account_id')
		res.update({'deposit_account_id':product_ids})
		return res	    

	@api.multi
	def _create_invoice(self, order, po_line, amount):
		inv_obj = self.env['account.invoice']
		ir_property_obj = self.env['ir.property']

		account_id = False
		if self.product_id.id:
			account_id = self.product_id.property_account_income_id.id or self.product_id.categ_id.property_account_income_categ_id.id
		if not account_id:

		    inc_acc = ir_property_obj.get('property_account_income_categ_id', 'product.category')
		    account_id = order.fiscal_position_id.map_account(inc_acc).id if inc_acc else False
		if not account_id:
		    raise UserError(
		        _('There is no income account defined for this product: "%s". You may have to install a chart of account from Accounting app, settings menu.') %
		        (self.product_id.name,))

		if self.amount <= 0.00:
		    raise UserError(_('The value of the down payment amount must be positive.'))
		context = {'lang': order.partner_id.lang}
		if self.advance_payment_method == 'percentage':
		    amount = order.amount_untaxed * self.amount / 100
		    name = _("Down payment of %s%%") % (self.amount,)
		else:
		    amount = self.amount
		    name = _('Down Payment')
		del context
		taxes = self.product_id.taxes_id.filtered(lambda r: not order.company_id or r.company_id == order.company_id)
		if order.fiscal_position_id and taxes:
		    tax_ids = order.fiscal_position_id.map_tax(taxes).ids
		else:
		    tax_ids = taxes.ids
		values = {
			'name': order.partner_ref or order.name,
		    'origin': order.name,
		    'type': 'in_invoice',
		    'reference': False,
		    'account_id': order.partner_id.property_account_receivable_id.id,
		    'partner_id': order.partner_id.id,
		    'partner_shipping_id': order.partner_id.id,
		    'invoice_line_ids': [(0, 0, {
		        'name': name,
		        'origin': order.name,
		        'account_id': self.deposit_account_id.id,
		        'price_unit': amount,
		        'quantity': 1.0,
		        'discount': 0.0,
		        'uom_id': self.product_id.uom_id.id,
		        'product_id': self.product_id.id,
		        'purchase_line_id': po_line.id,
		        'invoice_line_tax_ids': [(6, 0, tax_ids)],
		        # 'account_analytic_id': order.product_id.id,
		    })],
		    'currency_id': order.currency_id.id,
		    'payment_term_id': order.payment_term_id.id,
		    'fiscal_position_id': order.fiscal_position_id.id or order.partner_id.property_account_position_id.id,
		    # 'team_id': order.team_id.id,
		    # 'user_id': order.user_id.id,
		    'comment': order.notes,
		}

		for invoice_li in values['invoice_line_ids']:
			if invoice_li[2].get('account_id') == False:
				raise UserError(_('Please Select The Income Account'))
		invoice = inv_obj.create(values)
		invoice.compute_taxes()
		invoice.message_post_with_view('mail.message_origin_link',
		            values={'self': invoice, 'origin': order},
		            subtype_id=self.env.ref('mail.mt_note').id)
		return invoice

	@api.multi
	def create_invoices(self):
		purchase_orders = self.env['purchase.order'].browse(self._context.get('active_ids', []))
		
		if self.advance_payment_method == 'delivered':
			purchase_orders.hide_invoice = True
			purchase_orders.action_invoice_create()
		elif self.advance_payment_method == 'all':
			purchase_orders.hide_invoice = True
			purchase_orders.action_invoice_create(final=True)
		else:
	        # Create deposit product if necessary
			if not self.product_id:
				vals = self._prepare_deposit_product()
				self.product_id = self.env['product.product'].create(vals)
				# self.env['ir.default'].sudo().set('res.config.settings', 'prepayment_account_id', self.product_id.id)

			purchase_line_obj = self.env['purchase.order.line']
			for order in purchase_orders:
				if self.advance_payment_method == 'percentage':
					amount = order.amount_untaxed * self.amount / 100
				else:
					amount = self.amount
				if self.product_id.invoice_policy != 'order':
				    raise UserError(_('The product used to invoice a down payment should have an invoice policy set to "Ordered quantities". Please update your deposit product to be able to create a deposit invoice.'))
				if self.product_id.type != 'service':
				    raise UserError(_("The product used to invoice a down payment should be of type 'Service'. Please use another product or update this product."))
				taxes = self.product_id.taxes_id.filtered(lambda r: not order.company_id or r.company_id == order.company_id)
				if order.fiscal_position_id and taxes:
				    tax_ids = order.fiscal_position_id.map_tax(taxes).ids
				else:
				    tax_ids = taxes.ids
				context = {'lang': order.partner_id.lang}
				po_line = purchase_line_obj.create({
				    'name': _('Advance: %s') % (time.strftime('%m %Y'),),
				    'price_unit': amount,
				    'product_uom_qty': 0.0,
				    'product_qty':purchase_line_obj.product_qty,
				    'date_planned':purchase_orders.date_order,
				    'order_id': order.id,
				    'discount': 0.0,
				    'product_uom': self.product_id.uom_id.id,
				    'product_id': self.product_id.id,
				    'tax_id': [(6, 0, tax_ids)],
				})
				del context
				self._create_invoice(order, po_line, amount)
				purchase_orders.hide_invoice = False


		if self._context.get('open_invoices', False):
			return purchase_orders.action_view_invoice()
		return {'type': 'ir.actions.act_window_close'}

	def _prepare_deposit_product(self):
	    return {
	        'name': 'Down payment',
	        'type': 'service',
	        'invoice_policy': 'order',
	        'property_account_income_id': self.deposit_account_id.id,
	        'taxes_id': [(6, 0, self.deposit_taxes_id.ids)],
	    }
