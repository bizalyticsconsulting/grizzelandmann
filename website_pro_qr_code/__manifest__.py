# -*- coding: utf-8 -*-

{
    'name': 'Website Product QR Code',
    'version': '12.0',
    'category': 'All',
    'sequence': 6,
    'author': 'Webveer',
    'summary': 'Website product QR Code allows you to add website product URL information in QR code and we can print on label.',
    'description': """

=======================
Website product QR Code allows you to add website product URL information in QR code and we can print on label.

""",
    'depends': ['website_sale'],
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        # 'views/templates.xml'
    ],
    'qweb': [
        # 'static/src/xml/pos.xml',
    ],
    'images': [
        'static/description/website.jpg',
    ],
    'installable': True,
    'website': '',
    'auto_install': False,
    'price': 25,
    'currency': 'EUR',
}
