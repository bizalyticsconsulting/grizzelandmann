# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools, _

class ResConfigSettings(models.TransientModel):

    _inherit = "res.config.settings"

    website_label_url = fields.Char(string='Product Label URL',config_parameter='website_pro_qr_code.website_label_url')

#    @api.model
#    def get_default_website_label_url(self, fields):
#        website_label_url = False
#        if 'website_label_url' in fields:
#            website_label_url = self.env['ir.config_parameter'].sudo().get_param('website_pro_qr_code.website_label_url')
#        return {
#            'website_label_url': website_label_url
#        }
#
#    @api.multi
#    def set_website_label_url(self):
#        for wizard in self:
#            self.env['ir.config_parameter'].sudo().set_param('website_pro_qr_code.website_label_url', wizard.website_label_url)


class ProductProduct(models.Model):
    _inherit = 'product.product'


    @api.one
    def ip_infromation(self):
    	product_ip_address = self.env['ir.config_parameter'].sudo().get_param('website_pro_qr_code.website_label_url')
    	return product_ip_address+""+self.website_url

class ProductTemplate(models.Model):
    _inherit = 'product.template'

    website_qr_code = fields.Char("Website QR code",compute='_computeVar', readonly=True)

    @api.multi
    def _computeVar(self):
        for record in self: 
            record.website_qr_code = record.website_url
