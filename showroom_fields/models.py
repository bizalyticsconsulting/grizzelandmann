from odoo import models, fields, api, _,tools 
from datetime import date
import logging

class showroom_fields(models.Model):
   _inherit = 'sale.order'

   x_showroom_terms      = fields.Text(string="Terms", help="PO Terms")
   x_freight_terms       = fields.Text(string="Freight Terms", help="Freight Terms")
   x_fob                 = fields.Char(string="FOB", size=100, help="Free On Board Terms", default="Atlanta, GA")
   x_attention_to        = fields.Many2one('res.partner', string="Attention", index=True, help="Customer Contact")
   x_attention_ship_to   = fields.Many2one('res.partner', string="Attention Ship To", index=True, help="Shipping Contact")
   x_client_po           = fields.Char(string="Customer PO", size=100, help="Customer Purchase Order")
   x_shipping_com        = fields.Text(string="Shipping Comment", size=100, help="shipping comment")
   x_showroom_po         = fields.Char(string="Showroom PO", size=100, help="Showroom Purchase Order")
   x_client_account      = fields.Char(string="Client Account", size=100, help="Client Account")
   x_sidemark            = fields.Char(string="Sidemark", size=100, help="Order Sidemark")
   x_mfg_order           = fields.Char(string="Mfg Order", size=100, help="Manufacturing Order Reference")
   x_mfg_order_date      = fields.Date(string="Mfg Order Date", help="Manufacturing Order Date")
   x_vend_inv_num        = fields.Char(string="Vendor Invoice Number", size=100, help="Vendor Invoice Number")
   x_vend_inv_date       = fields.Date(string="Vendor Invoice Date", help="Vendor Invoice Date")
   x_special_inst        = fields.Text(string="Special Instructions", help="Special Instructions for the Sales Order / Quote")
   x_order_cfa           = fields.Char(string="CFA", size=100, help="Cutting for Approval details")
   x_salesperson1        = fields.Many2one('res.partner', string="Salesperson 1", index=True, domain=[('is_company','=',False),('customer','=','True'),('type','=','contact')], help="Showroom Contact")
   x_salesperson2        = fields.Many2one('res.partner', string="Salesperson 2", index=True, domain=[('is_company','=',False),('customer','=','True'),('type','=','contact')], help="Showroom Alternate Contact")
   x_order_shipdate      = fields.Date(string="Ship Date", help="Sales Order Ship Date")
   x_tracking_num        = fields.Char(string="Tracking Number", size=100, help="Shipping Tracking Number")
   # x_supplier       = fields.Many2one(comodel_name='res.partner', string='Supplier', store=True, related='order_line.product_id.seller_id', help="Seller Information")
   x_showroom            = fields.Many2one('res.partner',ondelete='set null', string="Showroom", index=True, domain=[('partner_type','=','showroom')], help="Showroom")
   x_resale_num          = fields.Char(string="Resale Number", size=100, help="ReSale Number")
   x_dimensions          = fields.Char(string="Dimensions", size=200, help="Dimensions Product or Package")
   x_com_cot             = fields.Char(string="COM/COT", size=100, help="Client Owned Material / Trim")
   x_ship_via            = fields.Char(string="Ship Via", size=100, help="Ship Via")
   x_npd_order           = fields.Boolean(string="NPD Order", help="Select for NPD Order")
   x_order_changed  = fields.Boolean(string="Order Edited Message", help="Show customer order edited message in red bold", default=False)
   x_specifier      = fields.Many2one('res.partner', string="Specifier", index=True, domain=[('is_company','=','True'),('partner_type','=',"specifier")], help="Specifier for the Sale")
   x_override_ship  = fields.Boolean(string="Override Shipping Address", help="Override Shipping Address", default=False)
   #dropship_id = fields.Many2one('res.partner')
   x_est_comp_date  = fields.Date(string="Est. Completion Date", help="Est. Completion Date")
   x_com_ship_to    = fields.Many2one('res.partner', string="COM ShipTo Address", index=True, domain=[('supplier','=','True'),('type','=','delivery')], help="COM ShipTo Address")

  
   @api.onchange('partner_id', 'x_sidemark')
   def caps_sidemark(self):
      if self.x_sidemark:
         self.x_sidemark = str(self.x_sidemark).upper()
      else:
         self.x_sidemark = self.partner_id.name

   @api.onchange('x_showroom')
   def change_showroom(self):
      dom = {}
      if self.x_showroom:
         self.x_salesperson1 = False
         self.x_salesperson2 = False
         dom = {'x_salesperson1': [('is_company','=',False),('customer','=','True'),('type','=','contact'),('parent_id','=',self.x_showroom.id)],
                'x_salesperson2': [('is_company','=',False),('customer','=','True'),('type','=','contact'),('parent_id','=',self.x_showroom.id)]}
         return {'domain': dom}

   @api.one
   def get_soname(self):
      return self.name[2:]


   @api.one
   def get_payment_details(self):
      payments = {}
      inv_lines = self.invoice_ids.filtered(lambda d: d.state != 'draft')
      if (len(inv_lines) > 0):
        for inv in inv_lines:
           p_lines = inv.payment_ids.filtered(lambda p: (p.state == 'posted' or p.state == 'open'))
           if (len(p_lines) > 0):
              for p in p_lines:
                 payments.update(p)
      return payments

   @api.one
   def get_vendor_name(self):
      vendor_list = self.get_vendor()[0]
      vendor = vendor_list[0]
      return vendor.name

   @api.one
   def get_vendor(self):
      if (len(self.order_line) > 0):
          sale_line  = self.order_line[0]
          product_id = sale_line.product_id
          if (len(product_id.seller_ids) > 0):
              seller = product_id.seller_ids[0]
              return seller.name

   @api.one
   def get_ship_via(self):
      vendor_list = self.get_vendor()[0]
      vendor = vendor_list[0]

      dropship_lines = self.partner_id.dropship_ids
      if (len(dropship_lines) == 0):
          dropship_lines = self.partner_id.parent_id.dropship_ids

      d_lines = dropship_lines.filtered(lambda d: d.vendor.id == vendor.id)
      if (len(d_lines) > 0):
          return d_lines[0].ship_via
      else:
          return ""

   @api.one
   def get_terms(self):
      vendor_list = self.get_vendor()[0]
      vendor = vendor_list[0]

      dropship_lines = self.partner_id.dropship_ids
      if (len(dropship_lines) == 0):
          dropship_lines = self.partner_id.parent_id.dropship_ids

      d_lines = dropship_lines.filtered(lambda d: d.vendor.id == vendor.id)
      if (len(d_lines) > 0):
          return d_lines[0].terms
      else:
          return ""

   @api.one
   def get_freight_terms(self):
      vendor_list = self.get_vendor()[0]
      vendor = vendor_list[0]

      dropship_lines = self.partner_id.dropship_ids
      if (len(dropship_lines) == 0):
          dropship_lines = self.partner_id.parent_id.dropship_ids

      d_lines = dropship_lines.filtered(lambda d: d.vendor.id == vendor.id)
      if (len(d_lines) > 0):
          return d_lines[0].freight_terms
      else:
          return ""

   @api.one
   def get_account_num(self):
      vendor_list = self.get_vendor()[0]
      vendor = vendor_list[0]

      dropship_lines = self.partner_id.dropship_ids
      if (len(dropship_lines) == 0):
          dropship_lines = self.partner_id.parent_id.dropship_ids

      d_lines = dropship_lines.filtered(lambda d: d.vendor.id == vendor.id)
      if (len(d_lines) > 0):
          return d_lines[0].account_no
      else:
          return ""

   @api.multi
   def _get_tax_amount_by_group(self):
      self.ensure_one()
      res = {}
      currency = self.currency_id or self.company_id.currency_id
      for line in self.order_line:
         base_tax = 0
         for tax in line.tax_id:
            group = tax.tax_group_id
            res.setdefault(group, 0.0)
            amount = tax.compute_all(line.price_reduce + base_tax, quantity=line.product_uom_qty)['taxes'][0]['amount']
            res[group] += amount
            if tax.include_base_amount:
               base_tax += amount
      res = sorted(res.items(), key=lambda l: l[0].sequence)
      res = map(lambda l: (l[0].name, l[1]), res)
      return res

   @api.one
   def get_commission_amount(self):
      if (len(self.commission_ids) > 0):
         amount = 0
         for comm in self.commission_ids:
             amount += comm.amount_total
         return amount
      else:
         return 0

   @api.one
   def get_amount_post_commission(self):
      comm_amount = self.get_commission_amount()[0]
      return self.amount_total - comm_amount

   @api.one
   def get_deposit_req(self):
      return self.amount_total / 2

   @api.one
   def get_payment(self):
      payment = 0
      for invoice in self.invoice_ids:
         if invoice.state != 'draft':
            payment += invoice.amount_total - invoice.residual
      return payment

   @api.one
   def get_balance(self):
      payment = self.get_payment()[0]
      balance = self.amount_total - payment
      return balance

class customer_fields(models.Model):
   _inherit = 'res.partner'

   x_resale_num     = fields.Char(string="Resale Number", size=100, help="ReSale Number")
   x_resale_date    = fields.Date(string="Resale Number Date", help="Resale Number Date")

   #dropship_ids = fields.One2many('sale.order','dropship_id','Dropship')

class SaleOrderLineReport(models.Model):
   _inherit = 'sale.order.line'

   x_showroom       = fields.Many2one("res.partner", related='order_id.x_showroom', string="Showroom", readonly=True, required=False, store=True)
   x_order_shipdate = fields.Date(string="Ship Date", help="Sales Order Ship Date", related='order_id.x_order_shipdate', readonly=True, required=False, store=True)
   x_customer       = fields.Many2one("res.partner", related='order_id.partner_id', string="Customer", readonly=True, required=False, store=True)
   x_mfg_order      = fields.Char(string="Mfg Order#", related='order_id.x_mfg_order', help="Mfg Order#", readonly=True, required=False, store=True)
   x_vend_inv_num   = fields.Char(string="Mfg Inv#", related='order_id.x_vend_inv_num', help="Mfg Inv#", readonly=True, required=False, store=True)
   x_order_total    = fields.Monetary(string="Order Total", related='order_id.amount_total', help="Sales Order Total", readonly=True, required=False, store=True)
   x_salesperson    = fields.Many2one("res.users", related='order_id.user_id', string="Salesperson", readonly=True, required=False, store=True)
   x_sales_team     = fields.Many2one("crm.team", related='order_id.team_id', string="Sales Team", readonly=True, required=False, store=True)
   x_order_city     = fields.Char(string="City", related='order_partner_id.city', size=200, help="City", readonly=True, required=False, store=True)
   x_order_state_id = fields.Many2one("res.country.state", related='order_partner_id.state_id', string="State", readonly=True, required=False, store=True)
   x_order_date     = fields.Datetime(string="Order Date", related='order_id.date_order', help="Sales Order Date", readonly=True, required=False, store=True)
   x_internal_cat   = fields.Many2one("product.category", related='product_tmpl_id.categ_id', string="Category", readonly=True, required=False, store=True)
   product_tmpl_id = fields.Many2one(
        'product.template', 'Product Template',
        auto_join=True, index=True, ondelete="cascade")   
   x_status         = fields.Selection("sale.order.state", related='order_id.state', readonly=True, required=False, store=True)
   x_client_po      = fields.Char(string="Customer PO", related='order_id.x_client_po', readonly=True, required=False, store=True, help="Customer Purchase Order")
   layout_category_id = fields.Many2one('sale.layout_category', string='Section')




   # x_internal_cat = fields.Many2one('product.category', 'Category' ),

   # catg_id =  fields.related('x_internal_cat', 'catg_id', type='Many2one', relation='product.template', string='Event', readonly=True, store=True)

class ProductNPDDates(models.Model):
   _inherit = 'product.template'

   x_product_intro_date = fields.Date(string="Introduction Date", help="Product Introduction Date", required=False, index=True)
   x_product_arch_date  = fields.Date(string="Archive Date", help="Product Archive Date", required=False, index=True)


   @api.multi
   def write(self, vals):
      if vals.get('active') == False:
         todays_date = date.today().strftime("%m/%d/%y")
         vals.update({'x_product_arch_date': todays_date})
      else:
         vals.update({'x_product_arch_date': None})

      res = super(ProductNPDDates, self).write(vals)
      return res

class PurchaseOrder(models.Model):
   _inherit='purchase.order'

   @api.one
   def get_com_cot(self):
      sale_line = self.env['sale.order.line'].search([('purchase_line_id', '=', self.order_line[0].id)])
      if sale_line:
         return sale_line.order_id.x_com_cot
      return ''


# class BaseConfigSettings(models.TransientModel):

#    _name = 'base.config.settings'
#    _inherit = 'res.config.settings'

#    custom_footer = fields.Boolean(related="company_id.custom_footer", string="Custom footer *", help="Check this to define the report footer manually. Otherwise it will be filled in automatically.")