# -*- coding: utf-8 -*-
{
    'name': "ShowroomFields",

    'summary': """
	Module adds custom fields to Quotes and Sales Orders for the Showrooms. 
    """,

    'description': """
	The fields are 
        - Terms
        - Freight Terms
        - FOB
        - Attention —> Customer Contact
        - Client PO
        - Client Account
        - Sidemark
        - Mfg Order
        - Mfg Order Date
        - Vendor Invoice Number
        - Vendor Invoice Date
        - Special Instructions
        - CFA
        - Salesperson 1
        - Salesperson 2
        - Ship Date
        - Tracking Number
    """,

    'author': "BizAlytics Consulting LLC",
    'website': "http://www.bizalytics.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Sales',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','sale','crm','delivery','purchase','product','sales_team'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'data/data_view.xml',
        'showroom_fields_view.xml',
        'report/report.xml',
        'report/biz_report_layout.xml',
        'report/sale_report_showroom_bill_templates.xml',
        'report/sale_report_we_bill_templates.xml',
        'report/sale_report_quote_template.xml',
        'report/report_label.xml',
        'report/report_label_back.xml',
        'report/purchase_order_templates.xml',
        'report/purchase_order_without_price_templates.xml',
        'report/invoice_template.xml',
        'report/invoice_showroom_bill_template.xml',
        
    ],
    # only loaded in demonstration mode
    'demo': [
    ],
}
