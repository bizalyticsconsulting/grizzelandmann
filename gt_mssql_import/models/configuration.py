# -*- coding: utf-8 -*-

#python3.5 -m pip install pymssql for Python 3.5

from odoo import models, fields, api
import pymssql

class MssqlConfig(models.Model):

    _name = 'config.config'
    _rec_name='db_name'
    
    db_name = fields.Char('DB Name',required=True)
    server = fields.Char('Server',required=True)
    password = fields.Char('Password',required=True)
    user_name = fields.Char('User name',required=True)
    type = fields.Selection([('import','Import'),('export','Export')],string='Operation',required=True)


