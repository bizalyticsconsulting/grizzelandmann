# -*- coding: utf-8 -*-
from odoo import models, fields, api
from datetime import datetime
import logging

logger = logging.getLogger(__name__)


class ImportCustomersData(models.Model):
    _inherit = 'res.partner'

    def createCustomer(self, vender_data):
        ir_model_data_obj = self.env['ir.model.data']
        country_obj = self.env['res.country']
        res_state_obj = self.env['res.country.state']
        user_obj = self.env['res.users']
        dropship_obj = self.env['dropship.custom']
        terms_obj = self.env['account.payment.term']

        vender_ids = []
        for data in vender_data:
            ir_model_ids = ir_model_data_obj.search(
                [('name', '=', data[0]), ('model', '=', 'res.partner'), ('module', '=', 'ms_customer')])
            customer_val = {}
            customer_val.update({
                'partner_type': data[1].lower(),
                'active': data[2],
                'street': data[4],
                'street2': data[5],
                'city': data[6],
                'zip': data[8],
                'phone': data[17],
                'email': data[19],
                'website': data[20],
                'sidemark': data[21],
                'fob': data[26],
                'specifier': data[27],
                'federal_id': data[28],
                'social_security': data[29],
                'entered_date': data[30],
                'changed_by': data[31],
                'changed_date': data[32],
                'sales_vendor_tax': data[33],
                'message_follower_ids': False,
                'freightable_tax': data[34],
                'crating_tax': data[35],
                'other_tax': data[36],
                'company_type': 'company',
            })
            if len(data) >= 43:
                customer_val.update({
                    'x_resale_num': data[42],
                    'x_resale_date': data[43],
                })
            else:
                customer_val.update({
                    'x_resale_num': None,
                    'x_resale_date': None,
                })

            if data[3]:
                customer_val.update({'name': data[3]})
            elif not data[3] or data[0]:
                customer_val.update({'name': data[0]})

            if data[9] == "USA":
                country_id = country_obj.search([('code', '=', 'US')])
                if country_id:
                    customer_val.update({'country_id': country_id.id})
                    state_id = res_state_obj.search(
                        ['|', ('name', '=', data[7]), ('code', '=', data[7]), ('country_id', '=', country_id.id)])
                    if state_id:
                        customer_val.update({'state_id': state_id.id})
            elif data[9]:
                country_id = country_obj.search(['|', ('name', '=', data[9]), ('code', '=', data[9])])
                if country_id:
                    customer_val.update({'country_id': country_id.id})
                    state_id = res_state_obj.search(
                        ['|', ('name', '=', data[7]), ('code', '=', data[7]), ('country_id', '=', country_id.id)])
                    if state_id:
                        customer_val.update({'state_id': state_id.id})
            elif not data[9] and data[7]:
                country_id = country_obj.search([('code', '=', 'US')])
                if country_id:
                    customer_val.update({'country_id': country_id.id})
                    state_id = res_state_obj.search(
                        ['|', ('name', '=', data[7]), ('code', '=', data[7]), ('country_id', '=', country_id.id)])
                    if state_id:
                        customer_val.update({'state_id': state_id.id})

            if data[38]:
                terms_id = terms_obj.search([('name', '=', data[38])])
                if terms_id:
                    customer_val.update({'client_terms': terms_id[0].id})

            if data[39]:
                terms_id = terms_obj.search([('name', '=', data[39])])
                if terms_id:
                    customer_val.update({'client_freight_terms': terms_id[0].id})
            child_address_val ={}
            if data[10] or data[11] or data[12] or data[14] or data[16]:
                child_address_val = {
                    'type': 'delivery',
                    'street': data[10],
                    'street2': data[11],
                    'city': data[12],
                    'zip': data[14],
                    'name': data[16],
                    'message_follower_ids': []
                }
                if data[15] == "USA" or data[15]:
                    country_id = country_obj.search(
                        ['|', ('name', '=', data[15]), '|', ('code', '=', data[15]), ('code', '=', 'US')])
                    if country_id:
                        child_address_val.update({'country_id': country_id.id})
                        state_id = res_state_obj.search(['|', ('name', '=', data[13]), ('code', '=', data[13]),
                                                         ('country_id', '=', country_id.id)])
                        if state_id:
                            child_address_val.update({'state_id': state_id.id})
                if not data[15] and data[13]:
                    country_id = country_obj.search([('code', '=', 'US')])
                    if country_id:
                        child_address_val.update({'country_id': country_id.id})
                        state_id = res_state_obj.search(['|', ('name', '=', data[13]), ('code', '=', data[13]),
                                                         ('country_id', '=', country_id.id)])
                        if state_id:
                            child_address_val.update({'state_id': state_id[0].id})

            if data[22]:
                ir_model_user_id = ir_model_data_obj.search([('name', '=', data[22]), ('model', '=', 'res.users')])
                if ir_model_user_id:
                    user_id = user_obj.browse(ir_model_user_id[0].res_id)
                    customer_val.update({'user_id': user_id[0].id})

            if ir_model_ids:
                vender_ids.append(ir_model_ids[0].res_id)
                customer_id = self.browse(ir_model_ids[0].res_id)
                customer_id.write(customer_val)
                logger.info(
                    "Customer is available with name %s and Id %s " % (customer_val.get('name'), customer_id.id))
                ship_id = self.search([('parent_id', '=', customer_id.id)])
                if child_address_val:
                    for ship in ship_id:
                        if ship.type == 'delivery':
                            ship.write(child_address_val)

            else:
                customer_id = self.with_context(tracking_disable=False).create(customer_val)
                if child_address_val:
                    child_address_val.update({'parent_id': customer_id.id, 'name': customer_id.name})
                    self.with_context(tracking_disable=False).create(child_address_val)
                ir_model_data_obj.create(
                    {'name': data[0], 'res_id': customer_id.id, 'model': 'res.partner', 'module': 'ms_customer'})
                logger.info("Customer created with name %s and Id %s " % (customer_val.get('name'), customer_id.id))
                vender_ids.append(customer_id.id)
            # if data[23] or data[24] or data[25]:
            #     drophip_vals = {'freight_terms': data[25], 'terms': data[24],'note':data[45],
            #                     'account_no':data[44],'dropship_id': customer_id.id}
            #     if data[23]:
            #         ship_via_id = self.env['dropship.custom.shipvia'].search([('name','=',data[23])])
            #         if not ship_via_id:
            #             ship_via_id = self.env['dropship.custom.shipvia'].create({'name':data[23]})
            #             print(ship_via_id)
            #             drophip_vals.update({'ship_via': ship_via_id.id})
            #         else:
            #             # ship_via_id.ship_via[0].write(drophip_vals)
            #             drophip_vals.update({'ship_via': ship_via_id[0].id})
            #
            #
            #     if data[46]:
            #         ir_model_dropship_vendor_id = ir_model_data_obj.search([('name', '=', data[46]),('model', '=', 'res.partner')])
            #         if ir_model_dropship_vendor_id:
            #             vendor_id = self.browse(ir_model_dropship_vendor_id[0].res_id)
            #             drophip_vals.update({'vendor': vendor_id[0].id})
            #     dropship_ids = dropship_obj.search(
            #         [('freight_terms', '=', data[25]), ('terms', '=', data[24]),
            #          ('dropship_id', '=', customer_id.id),('note','=',data[45]),('account_no','=',data[44])])
            #     if dropship_ids:
            #         print(dropship_ids,drophip_vals)
            #         dropship_ids[0].write(drophip_vals)
            #     else:
            #         dropship_ids = dropship_obj.create(drophip_vals)
            self.env.cr.commit()
        return vender_ids

    @api.multi
    def import_customers_data(self, conn):
        log_obj = self.env['log.management']

        # Query to get all Customers
        partners_list = "SELECT Address.[Address ID]AS ID," \
                        "Address.[Type]AS TYPE," \
                        "Address.[Active]AS ACTIVE, " \
                        "Address.[Name]AS NAME, " \
                        "Address.[Address]AS ADDRESS_LINE1, " \
                        "Address.[Address 2]AS ADDRESS_LINE2, " \
                        "Address.[City]AS CITY, " \
                        "Address.[State]AS STATE," \
                        " Address.[Zip]AS ZIP, " \
                        "Address.[Country]AS COUNTRY," \
                        " Address.[Mailing Address]AS MAIL_ADDRESS_LINE1," \
                        " Address.[Mailing Address 2]AS MAIL_ADDRESS_LINE2, " \
                        "Address.[Mailing City]AS MAIL_CITY, " \
                        "Address.[Mailing State]AS MAIL_STATE," \
                        "Address.[Mailing Zip]AS MAIL_ZIP," \
                        "Address.[Mailing Country]AS MAIL_COUNTRY," \
                        "Address.[Attention]AS MAIL_ADDRESS_NAME," \
                        "Address.[Phone]AS PHONE," \
                        "Address.[Fax]AS FAX," \
                        "Address.[Email]AS EMAIL," \
                        "Address.[Web Site]AS WEBSITE," \
                        "Address.[Client Sidemark]AS SIDEMARK," \
                        "Address.[Salesperson]AS SALESPERSON," \
                        "Address.[Ship Via]AS SHIPVIA," \
                        "Address.[Terms]AS TERMS," \
                        "Address.[Freight Terms]AS FREIGHT_TERMS," \
                        "Address.[FOB]AS FOB," \
                        "Address.[Specifier]AS SPECIFIER," \
                        "Address.[Federal ID]AS FEDERALID," \
                        "Address.[Social Security #]AS SSN," \
                        "Address.[Entered Date]AS ENTERED_DATE," \
                        "Address.[Changed By]AS CHANGED_BY," \
                        "Address.[Changed Date]AS CHANGED_DATE," \
                        "Address.[Taxable Selling]AS SALES_TAX," \
                        "Address.[Taxable Freight]AS FREIGHT_TAXABLE," \
                        "Address.[Taxable Crating]AS CRATING_TAXABLE," \
                        "Address.[Taxable Other]AS OTHER_TAXABLE," \
                        "Address.[Commission Percent]AS COMMISSION_PERCENTAGE," \
                        "Address.[Client Ship Via]AS CLIENT_SHIP_VIA," \
                        "Address.[Client Terms]AS CLIENT_TERMS," \
                        "Address.[Client Freight Terms]AS CLIENT_FREIGHT_TERMS," \
                        "Address.[Sales Territory] AS SALES_TERRITORY, " \
                        "Address.[Resale #] AS RESALE_NUMBER, " \
                        "Address.[Resale Date] AS RESALE_NUMBER_DATE " \
                        "FROM dbo.Address Address " \
                        "WHERE Address.[Type] <> 'Vendor'"
        conn.execute(partners_list)
        conn_data = conn.fetchall()
        logger.info("Total Number of Customers getting ---- %s" % (len(conn_data)))
        # try:
        self.env['res.partner'].createCustomer(conn_data)
        # except Exception as e:
        #     log_vals = {'operation': 'customers', 'message': e, 'date': datetime.now()}
        #     log_obj.create(log_vals)
        return True
