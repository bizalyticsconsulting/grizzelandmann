# -*- coding: utf-8 -*-
from odoo import models, api
from datetime import datetime
import logging
logger = logging.getLogger(__name__)


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    @api.multi
    def update_product_catalog_data(self,conn,db,catalog_ids):
        log_obj = self.env['log.management']
        
        catalog_detail_list="SELECT [Line #] FROM [Catalog Detail] WHERE [Product #]=%s;"
        conn.execute(catalog_detail_list,self.product_default_code)
        catalog_result = conn.fetchall()
        deleted_list=[rec[0] for rec in catalog_result if rec[0] not in [int(seq.sequence) for seq in catalog_ids]]
#       Delete the record from MSSQL
        if deleted_list:
            delete_query="DELETE FROM [Catalog Detail] WHERE [Line #]=%s;"
            for seq in deleted_list:
                conn.execute(delete_query,seq)
                db.commit()
                
#       Prepare the query
        query='''UPDATE [Catalog Detail] SET 
                        Code=%s,
                        Description=%s
                        WHERE [Line #]=%s;'''
        try:
            for line in catalog_ids:
                data=(
                    line.code,
                    line.desc,
                    line.sequence)
                conn.execute(query,data)
    #           commit the changes made
                db.commit()
                logger.info("Updated the Product ---- %s" % line.product_tmpl_id.product_default_code)
        except Exception as e:
            log_vals = {'operation': 'p_purchased',
                        'message':'Update: '+ str(e), 'date':datetime.now() }
            log_obj.create(log_vals)
#       Will close all the connections
#        finally:
#            conn.close()
#            db.close()
        return True
    
    @api.multi
    def export_product_catalog_data(self,conn,db,catalog_ids):
#        ir_model_data_obj = self.env['ir.model.data']
        log_obj = self.env['log.management']
        
#       Prepare the query
        query='''INSERT INTO [Catalog Detail](
                        [Vendor],
                        [Product #],
                        [Product Option],
                        [Line #],
                        Code,
                        Description)
                        VALUES (%s,%s,%s,%s,%s,%s);'''
        try:
            for line in catalog_ids:
                data=(line.product_tmpl_id.seller_ids[0].name.partner_code if line.product_tmpl_id.seller_ids else '',
                line.product_tmpl_id.product_default_code,
                line.product_tmpl_id.default_code_option,
                line.sequence,
                line.code,
                line.desc)
                conn.execute(query,data)
    #           commit the changes made
                db.commit()
                logger.info("Product Catalog Deatil is created for the product %s " % (line.product_tmpl_id.product_default_code))
        except Exception as e:
            log_vals = {'operation': 'p_purchased',
                        'message':'Export: '+ str(e), 'date':datetime.now() }
            log_obj.create(log_vals)
        return True
    