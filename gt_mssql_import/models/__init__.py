# -*- coding: utf-8 -*-

from .import configuration
from .import sales_categories
from .import vendors
from .import log_file
from .import product_purchased
from .import product_catalog_details
from .import product_template
from . import address_contacts
from . import confirm_sale_orders
from . import deliver_carrier
from . import employees_data
from . import locations
from . import purchase_orders
from . import quotes
from . import rfq
from . import sales_purchase_taxlocation
from . import terms
from . import customers
from . import dropship_custom
