# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import datetime
import re
from bs4 import BeautifulSoup
import logging

logger = logging.getLogger(__name__)


class Quotes(models.Model):
    _inherit = 'sale.order'

    @api.multi
    def get_order_lines(self, conn):
        quote_order_line_list = "SELECT  [IDEAL Item #]as ID,\
                                            [Client]as client_id,\
                                            [Vendor]as vendor_id,\
                                            [Purchasing]as purchasing_id,\
                                            [Purchase Quantity]as purchasing_qty,\
                                            [Selling Quantity]as selling_qty,\
                                            [Unit]   	  as unit,\
                                            [Item Description]as item_description,\
                                            [Catalog ID]as catalog_id,\
                                            [Inventory ID]as inventory_id,\
                                            [User Code 1]as user_code_1,\
                                            [User Code 2]as user_code_2,\
                                            [Proposal #]as quote_number,\
                                            [Sales Code]as sales_code,\
                                            [PO #]as purchase_number,\
                                            [Invoice #]as invoice_number,\
                                            [Purchase Unit Cost]as purchase_unit_cost, \
                                            [Purchase Cost]as purchase_cost,\
                                            [Selling Unit Cost]as selling_unit_cost, \
                                            [Selling Cost]as selling_cost,\
                                            [Freight]as freight,\
                                            [Crating]as crating,\
                                            [Installation]as install,\
                                            [Other]as other_charges,\
                                            [Sales Tax]as sales_tax,\
                                            [Purchase Freight]as purchase_freight,\
                                            [Purchase Crating]as purchase_crating,\
                                            [Purchase Installation]as purchase_installation,\
                                            [Purchase Other]as purchase_other, \
                                            [Purchase Sales Tax]as purchase_sales_tax, \
                                            [Selling Discount Percent]as selling_discount_perc, \
                                            [Selling Discount Unit]as selling_discount_unit, \
                                            [Selling Discount]as selling_discount,  \
                                            [Purchase Discount Percent]as purchase_discount_perc,  \
                                            [Purchase Discount Unit]as purchase_discount_unit, \
                                            [Status]as status, \
                                            [Sidemark]as sidemark,    \
                                            [CFA]as cfa,  \
                                            [Reserve]as reserve, \
                                            [Date Requested]as date_requested,  \
                                            [Date Shipped]as date_shipped,  \
                                            [Confirmation Date]as confirmation_date, \
                                            [Tax Percent 1]as tax_percentage, \
                                            [Entered By]as entered_by,  \
                                            [Entered Date]as entered_date,  \
                                            [Commission Sales Percent]as comm_sales_perc,   \
                                            [Commission Sales]as comm_sales, \
                                            [Catalog Option]as catalog_option, \
                                            [Specifier Client]as client_specifier,   \
                                            [Salesperson 2]as salesperson_2, \
                                            [Item Ordering]as item_sequence,  \
                                            [Client Description]as client_description,  \
                                            [Vendor Description]as vendor_description,\
                                            CONCAT([Catalog ID], '-', [Catalog OPTION], '-',[Vendor]) as NewProductID \
                                            FROM Item WHERE  [PO #] IS NULL"
        conn.execute(quote_order_line_list)
        conn_data = conn.fetchall()
        order_lines = {}
        for data in conn_data:
            if data[12] not in order_lines.keys():
                order_lines.update({data[12]: [data]})
            else:
                order_lines[data[12]].append(data)
        return order_lines

    @api.multi
    def import_quotes(self, conn):
        ir_model_data_obj = self.env['ir.model.data']
        partner_obj = self.env['res.partner']
        partner_obj = self.env['res.partner']
        log_obj = self.env['log.management']
        res_state_obj = self.env['res.country.state']
        order_obj = self.env['sale.order']
        msg_obj = self.env['mail.message']
        terms_obj = self.env['account.payment.term']
        carrier_obj = self.env['delivery.carrier']
        seq_obj = self.env['ir.sequence']
        tax_obj = self.env['account.tax']

        # Get Lines
        order_lines = order_obj.get_order_lines(conn)

        quotes_list = "SELECT  i.[Proposal #],\
                                p.[Proposal Item] as quote_number_int, \
                                p.[Client] as client, \
                                p.[Proposal Date] as quote_date,  \
                                p.[Vendor] as supplier, \
                                p.[Purchasing] as purchasing_location, \
                                p.[Salesperson] as salesperson, \
                                p.[Ship Via] as ship_via, \
                                p.[Terms] as terms, \
                                p.[Freight Terms] as shipping_terms, \
                                p.[FOB] as FOB, \
                                p.[Attention] as attention_to, \
                                p.[Ship To] as ship_to, \
                                p.[Ship To Name] as ship_to_name, \
                                p.[Ship To Address] as ship_address1, \
                                p.[Ship To Address2] as ship_address2, \
                                p.[Ship To City] as ship_to_city, \
                                p.[Ship To State] as ship_to_state, \
                                p.[Ship To Zip] as ship_to_zip, \
                                p.[Ship To Attention] as ship_to_attention,\
                                p.[Ship To Phone] as ship_to_phone, \
                                p.[Ship To Fax] as ship_to_fax, \
                                p.[Client PO] as client_po, \
                                p.[Client Account] as client_account_number,\
                                p.[Date Requested] as date_requested,\
                                p.[Sidemark] as sidemark,\
                                p.[Confirmation Date] as confirm_date,\
                                p.[Special Instructions] as special_instructions,\
                                p.[Notes] as chatter,\
                                p.[Client Ship Via] as client_ship_via,\
                                p.[Client Terms] as client_terms,\
                                p.[Client Freight Terms] as client_freight_terms,\
                                p.[Specifier Client] as specifier, \
                                p.[Salesperson 2] as salesperson_2, \
                                i.[PO #] as pono,\
                                a.[Address ID] as ID,\
                                a.[Type] as TYPE,\
                                a.[Active] as ACTIVE, \
                                a.[Name] as NAME, \
                                a.[Address] as ADDRESS_LINE1, \
                                a.[Address 2] as ADDRESS_LINE2, \
                                a.[City] as CITY, \
                                a.[State] as STATE,\
                                a.[Zip] as ZIP, \
                                a.[Country] as COUNTRY,\
                                a.[Mailing Address] as MAIL_ADDRESS_LINE1,\
                                a.[Mailing Address 2] as MAIL_ADDRESS_LINE2, \
                                a.[Mailing City] as MAIL_CITY, \
                                a.[Mailing State] as MAIL_STATE,\
                                a.[Mailing Zip] as MAIL_ZIP,\
                                a.[Mailing Country] as MAIL_COUNTRY,\
                                a.[Attention] as MAIL_ADDRESS_NAME,\
                                a.[Phone] as PHONE,\
                                a.[Fax] as FAX,\
                                a.[Email] as EMAIL,\
                                a.[Web Site] as WEBSITE,\
                                a.[Client Sidemark] as SIDEMARK,\
                                a.[Salesperson] as SALESPERSON,\
                                a.[Ship Via] as SHIPVIA,\
                                a.[Terms] as TERMS,\
                                a.[Freight Terms] as FREIGHT_TERMS,\
                                a.[FOB] as FOB,\
                                a.[Specifier] as SPECIFIER,\
                                a.[Federal ID] as FEDERALID,\
                                a.[Social Security #] as SSN,\
                                a.[Entered Date] as ENTERED_DATE,\
                                a.[Changed By] as CHANGED_BY,\
                                a.[Changed Date] as CHANGED_DATE,\
                                a.[Taxable Selling] as SALES_TAX,\
                                a.[Taxable Freight] as FREIGHT_TAXABLE,\
                                a.[Taxable Crating] as CRATING_TAXABLE,\
                                a.[Taxable Other] as OTHER_TAXABLE,\
                                a.[Commission Percent] as COMMISSION_PERCENTAGE,\
                                a.[Client Ship Via] as CLIENT_SHIP_VIA,\
                                a.[Client Terms] as CLIENT_TERMS,\
                                a.[Client Freight Terms] as CLIENT_FREIGHT_TERMS,\
                                a.[Sales Territory] as SALES_TERRITORY,\
                                a.[Resale #] AS RESALE_NUMBER,\
                                a.[Resale Date] AS RESALE_NUMBER_DATE \
                                FROM Proposal p INNER JOIN  Item i on i.[Proposal #] = p.[Proposal #] and i.Client = p.Client \
                                INNER JOIN dbo.Address a on a.[Address ID] = p.[Client] where i.[PO #] IS NULL "
        conn.execute(quotes_list)
        conn_data = conn.fetchall()
        logger.info("Total Number of Quote Orders getting are ---- %s" % (len(conn_data)))
        # try:
        quote_records = []
        for data in conn_data:
            if ((str(data[0]).strip()) + "-" + (str(data[2]).strip()) in quote_records):
                continue
            quote_records.append((str(data[0]).strip()) + "-" + (str(data[2]).strip()))
            quote_vals = {}
            if not data[2]:
                log_vals = {'operation': 'quotes',
                            'message': "Client not found for Quote mssql id {}".format(data[2]), 'date': datetime.now()}
                log_obj.create(log_vals)
                continue
            else:
                ir_model_partner_id = ir_model_data_obj.search(
                    [('name', '=', data[2]), ('model', '=', 'res.partner')])
                if ir_model_partner_id:
                    partner_sale_client_id = partner_obj.browse(ir_model_partner_id.res_id)
                    quote_vals.update({'partner_id': partner_sale_client_id[0].id})
                else:
                    customer_id = partner_obj.with_context({
                        'default_active':False,
                    }).createCustomer([data[35:]])
                    logger.info("----->>Customer Created from Quote")
                    if customer_id:
                        quote_vals.update({'partner_id': customer_id[0]})
                quote_vals.update(
                    {
                        'date_order': data[3],
                        'x_ship_via': data[7],
                        'x_showroom_terms': data[8],
                        'x_freight_terms': data[9],
                        'specifier': data[6],
                        'x_fob': data[10],
                        'x_client_po': data[22],
                        'x_client_account': data[23],
                        'x_sidemark': data[25],
                        'confirmation_date': data[26] if data[26] else datetime.now(),
                        'x_special_inst': data[27],
                    })
                seq_ids = seq_obj.search([('code','=','sale.order')])
                if seq_ids:
                    quote_vals.update({'name': seq_ids[0].prefix + "-" + (str(data[0]).strip()) + "-" + (str(data[2]).strip())})

                if data[5]:
                    ir_model_partner_id = ir_model_data_obj.search(
                        [('name', '=', data[5]), ('model', '=', 'res.partner')])
                    if ir_model_partner_id:
                        partner_sale_id = partner_obj.browse(ir_model_partner_id.res_id)
                        quote_vals.update({'x_showroom': partner_sale_id[0].id})

                        if data[6]:
                            sales_partner_id = partner_obj.search(
                                [('parent_id', '=', partner_sale_id.id), ('type', '=', 'contact'), ('name','=', data[6])])
                            if sales_partner_id:
                                quote_vals.update({'x_salesperson1': sales_partner_id[0].id})
                            else:
                                s_p_id = partner_obj.create({'name': data[6], 'parent_id': partner_sale_id.id, 'type': 'contact'})
                                quote_vals.update({'x_salesperson1': s_p_id.id})

                if data[11]:
                    ir_model_partner_id = ir_model_data_obj.search(
                        [('name', '=', data[11]), ('model', '=', 'res.partner')])
                    if ir_model_partner_id:
                        partner_sale_id = partner_obj.browse(ir_model_partner_id.res_id)
                        quote_vals.update({'x_attention_to': partner_sale_id[0].id})

                if data[33]:
                    ir_model_partner_id = ir_model_data_obj.search(
                        [('name', '=', data[33]), ('model', '=', 'res.partner')])
                    if ir_model_partner_id:
                        partner_sale_id = partner_obj.browse(ir_model_partner_id.res_id)
                        quote_vals.update({'x_salesperson2': partner_sale_id[0].id})

                ir_user_model_ids = ir_model_data_obj.search(
                    [('name', '=', data[6]), ('model', '=', 'res.users')])
                # If their is existing ir_model_id means we have created it before then it will move in this condition else it will create new
                if ir_user_model_ids:
                    quote_vals.update({'user_id': ir_user_model_ids[0].res_id})
                else:
                    quote_vals.update({'user_id': False})

                if data[32]:
                    ir_model_partner_id = ir_model_data_obj.search(
                        [('name', '=', data[32]), ('model', '=', 'res.partner')])
                    if ir_model_partner_id:
                        partner_sale_id = partner_obj.browse(ir_model_partner_id.res_id)
                        quote_vals.update({'specifier_client': partner_sale_id[0].id})

                if data[8]:
                    terms_id = terms_obj.search([('name', '=', data[8])])
                    if terms_id:
                        quote_vals.update({'payment_term_id': terms_id[0].id})

                if data[30]:
                    terms_id = terms_obj.search([('name', '=', data[30])])
                    if terms_id:
                        quote_vals.update({'client_terms': terms_id[0].id})

                if data[31]:
                    terms_id = terms_obj.search([('name', '=', data[31])])
                    if terms_id:
                        quote_vals.update({'client_freight_terms': terms_id[0].id})

                if data[7]:
                    carrier_id = carrier_obj.search([('name', '=', data[7])])
                    if carrier_id:
                        quote_vals.update({'carrier_id': carrier_id[0].id})

                if data[29]:
                    carrier_id = carrier_obj.search([('name', '=', data[29])])
                    if carrier_id:
                        quote_vals.update({'client_ship_via': carrier_id[0].id})

                if data[12]:
                    if data[13]:
                        ir_model_partner_ship_id = ir_model_data_obj.search(
                            [('name', '=', data[12]), ('model', '=', 'res.partner')])
                        if ir_model_partner_ship_id:
                            partner_ship_id = partner_obj.browse(ir_model_partner_ship_id[0].res_id)
                            if partner_ship_id and partner_ship_id.id in partner_obj.ids:

                                partner_ship_vals = {'parent_id': partner_ship_id.id, 'type': 'delivery'}
                                if data[17]:
                                    state_id = res_state_obj.search(
                                        ['|', ('name', '=', data[17]), ('code', '=', data[7])])
                                    if state_id:
                                        partner_ship_vals.update({'state_id': state_id[0].id})

                                partner_ship_vals.update(
                                    {'name': data[13], 'street': data[14], 'street2': data[15], 'fax': data[21]})

                                if data[16]:
                                    partner_ship_vals.update({'city': data[16]})

                                if data[20]:
                                    partner_ship_vals.update({'phone': data[20]})

                                if data[18]:
                                    partner_ship_vals.update({'zip': data[18], })

                                shipping_address_ids = partner_obj.search(
                                    ['|', '|', '|', '|', ('street', '=', data[14]), ('street2', '=', data[15]),
                                     ('city', '=', data[16]), ('zip', '=', data[18]), ('name', '=', data[13]),
                                     ('parent_id', '=', partner_ship_id.id)])
                                if shipping_address_ids:
                                    partner_obj.write(partner_ship_vals)
                                    logger.info("The child ids are writed")
                                else:
                                    partner_obj.create(partner_ship_vals)
                                    logger.info("The child ids are created")

                ir_model_ids = ir_model_data_obj.search(
                    [('name', '=', data[0]), ('model', '=', 'sale.order'), ('module', '=', 'quotes')])
                if ir_model_ids:
                    quote_id = self.browse(ir_model_ids[0].res_id)
                    quote_id.write(quote_vals)
                    if data[28]:
                        msg_ids = msg_obj.search(
                            [('model', '=', 'sale.order'), ('res_id', '=', quote_id.id), ('body', '=', data[28])])
                        if not msg_ids:
                            quote_id.message_post(body=data[28])
                    logger.info("Quote Order is available with Id {} ".format(quote_id))
                else:
                    quote_id = self.with_context(tracking_disable=False).create(quote_vals)
                    if data[28]:
                        quote_id.message_post(body=data[28])
                    ir_model_data_obj.create(
                        {'name': data[0], 'res_id': quote_id.id, 'model': 'sale.order', 'module': 'quotes'})
                    logger.info("Quote Order is created with Id {} ".format(quote_id))

            if order_lines.get(data[0], False):
                order_obj.create_sale_order_line(quote_id, order_lines.get(data[0]))
            else:
                log_vals = {
                    'operation': 'quotes',
                    'message': "Lines not founds for Quote order having Odoo id {} and Mssql id {} ".format(
                        quote_id.id, data[0]),
                    'date': datetime.now()}
                log_obj.create(log_vals)
            self.env.cr.commit()
        # except Exception as e:
        #     log_vals = {'operation': 'quotes',
        #                 'message': e, 'date': datetime.now()}
        #     log_obj.create(log_vals)

        return True

    @api.multi
    def create_sale_order_line(self, order, lines):
        ir_model_data_obj = self.env['ir.model.data']
        partner_obj = self.env['res.partner']
        uom_obj = self.env['uom.uom']
        uom_categ_obj = self.env['uom.category']
        prod_templ_obj = self.env['product.template']
        prod_obj = self.env['product.product']
        log_obj = self.env['log.management']
        quote_line_obj = self.env['sale.order.line']
        purchase_obj = self.env['purchase.order']
        categ_obj = self.env['product.category']
        tax_obj = self.env['account.tax']
        tag_obj = self.env['product.tag']
        supplier_info_obj = self.env['product.supplierinfo']

        for data in lines:
            print("Line Data ---->",data)
            uom_id = False
            quote_line_vals = {}
            quote_line_vals.update(
                {
                    'order_id': order.id,
                    'product_uom_qty': data[5],
                    'product_qty': data[4],
                    'price_unit': data[18],
                    'freight': data[20],
                    'sequence': data[50],
                    'install': data[21],
                    'other_charges': data[22],
                    'sales_tax': data[23],
                    'discount': data[30],
                })
            if data[42] > 0:
                saletax_ir_model_ids = tax_obj.search(
                    [('name', '=', 'Sales Tax - ' + str(data[42]).strip()), ('type_tax_use', '=', 'sale')])
                if saletax_ir_model_ids:
                    quote_line_vals.update({'tax_id': [(6,0,saletax_ir_model_ids.ids)]})
                else:
                    tax_id = tax_obj.create({
                        'name': 'Sales Tax - ' + str(data[42]).strip(),
                        'type_tax_use': 'sale',
                        'amount': float(data[42]),
                        'amount_type': 'percent'
                    })
                    quote_line_vals.update({'tax_id': [(6,0,[tax_id.id])]})
            else:
                quote_line_vals.update({'tax_id': []})

            if data[51]:
                if isinstance(data[51], str):
                    client_desc = data[51]
                    # client_desc = data[51].encode('ascii', 'ignore').encode('utf-8')
                    # client_desc = re.sub(r'<.*?>', '', client_desc)
                    # soup = BeautifulSoup(client_desc, "lxml")
                    # texts = soup.find_all(text=True)
                    # for t in texts:
                    #     newtext = t.replace("&nbsp", " ")
                    #     client_desc = t.replace_with(newtext)
                # elif isinstance(data[51], unicode):
                #     client_desc = data[51].encode('ascii', 'ignore').encode('utf-8')
                    # client_desc = re.sub(r'<.*?>', '', client_desc)
                    # soup = BeautifulSoup(client_desc, "lxml")
                    # texts = soup.find_all(text=True)
                    # for t in texts:
                    #     newtext = t.replace("&nbsp", " ")
                    #     client_desc = t.replace_with(newtext)
                quote_line_vals.update({'name': client_desc})
            elif data[7]:
                quote_line_vals.update({'name': data[7]})
            else:
                quote_line_vals.update({'name': ' '})


            tag_list = []
            if data[10]:
                tag_id = tag_obj.search([('name', '=', data[10])])
                if not tag_id:
                    tag_id = tag_obj.create({
                        'name': data[10],
                        'active': True,
                    })
                    tag_list.append(tag_id[0].id)
                else:
                    tag_list.append(tag_id[0].id)
            if data[11]:
                tag_id = tag_obj.search([('name', '=', data[11])])
                if not tag_id:
                    tag_id = tag_obj.create({
                        'name': data[11],
                        'active': True,
                    })
                    tag_list.append(tag_id[0].id)
                else:
                    tag_list.append(tag_id[0].id)
            if tag_list:
                quote_line_vals.update({'tag_ids': [(6, 0, tag_list)]})

            if data[1]:
                ir_model_partner_id = ir_model_data_obj.search(
                    [('name', '=', data[1]), ('model', '=', 'res.partner')])
                if not ir_model_partner_id:
                    log_vals = {'operation': 'q_lineitem',
                                'message': "The Partner  of Quote lineitem {} id is not found".format(data[0]),
                                'date': datetime.now()}
                    log_obj.create(log_vals)
                else:
                    partner_order_id = partner_obj.browse(ir_model_partner_id[0].res_id)
                    quote_line_vals.update({'order_partner_id': partner_order_id[0].id})

            if data[6]:
                uom_ids = uom_obj.search([('name', 'ilike', data[6])])
                if uom_ids:
                    uom_id = uom_ids[0]
                    quote_line_vals.update({'product_uom': uom_id.id})
                else:
                    uom_categ_ids = uom_categ_obj.search([('name', '=', 'Units')])
                    if not uom_categ_ids:
                        uom_categ_id = uom_categ_obj.create({
                            'name': 'Units',
                            'uom_type':'smaller'
                        })
                    else:
                        uom_categ_id = uom_categ_ids[0]
                    uom_id = uom_obj.create({'name': data[6], 'category_id': uom_categ_id[0].id,'uom_type':'smaller'})
                    quote_line_vals.update({'product_uom': uom_id[0].id})
            else:
                uom_ids = uom_obj.search([('name', 'ilike', 'Each')])
                if uom_ids:
                    uom_id = uom_ids[0]
                    quote_line_vals.update({'product_uom': uom_id.id})
                else:
                    uom_categ_ids = uom_categ_obj.search([('name', '=', 'Units')])
                    if not uom_categ_ids:
                        uom_categ_id = uom_categ_obj.create({
                            'name': 'Units',
                            'uom_type': 'smaller'
                        })
                    else:
                        uom_categ_id = uom_categ_ids[0]
                    uom_id = uom_obj.create({'name': 'Each', 'category_id': uom_categ_id[0].id, 'uom_type': 'smaller'})
                    quote_line_vals.update({'product_uom': uom_id[0].id})


            if not data[53]:
                log_vals = {'operation': 'q_lineitem',
                            'message': "The Product id {} of Quote lineitem {} id is not found".format(data[53],
                                                                                                       data[0]),
                            'date': datetime.now()}
                log_obj.create(log_vals)
                continue
            else:
                nprd = {
                    'name': data[7] or data[53],
                    'default_code': data[53],
                    'active': True,
                    'uom_id': uom_id.id if uom_id else False,
                    'uom_po_id': uom_id.id if uom_id else False,
                    'sale_ok': True,
                    'purchase_ok': True,
                    'type': 'product',
                    'purchase_method': 'purchase',
                }
                ir_model_product_templ_id = ir_model_data_obj.search(
                    [('name', '=', data[53]), ('model', '=', 'product.template'),('module','=','product_template')])
                if not ir_model_product_templ_id:
                    # log_vals = {'operation': 'q_lineitem',
                    #             'message': "The Product id {}  is not found so created".format(data[53]),
                    #             'date': datetime.now()}
                    # log_obj.create(log_vals)

                    # to create product
                    product_ids = prod_obj.search([('product_tmpl_id.default_code', '=', data[53])])
                    product_tmpl_id = product_ids[0].product_tmpl_id if product_ids else False
                    if product_ids:
                        product_id = product_ids[0]
                        if not uom_id:
                            quote_line_vals.update({'product_uom': product_id.uom_id.id})
                    else:
                        product_id = False
                        if not uom_id:
                            uom_ids = uom_obj.search([('name', 'ilike', 'Each'), ('category_id.name','=','Units')])
                            if uom_ids:
                                uom_id = uom_ids[0]
                            else:
                                uom_categ_ids = uom_categ_obj.search([('name', '=', 'Units')])
                                if not uom_categ_ids:
                                    uom_categ_id = uom_categ_obj.create({
                                        'name': 'Units'
                                    })
                                else:
                                    uom_categ_id = uom_categ_ids[0]
                                uom_id = uom_obj.create({'name': 'Each', 'category_id': uom_categ_id[0].id,'uom_type':'smaller'})
                        quote_line_vals.update({'product_uom': uom_id.id})
                        # if data[4]:

                        ir_model_categ_id = ir_model_data_obj.search(
                            [('name', '=', data[13]), ('model', '=', 'product.category')])
                        if ir_model_categ_id:
                            categ_id = categ_obj.browse(ir_model_categ_id.res_id)
                            nprd.update({'categ_id': categ_id.id})
                        product_tmpl_id = prod_templ_obj.create(nprd)
                        product_ids = prod_obj.search([('product_tmpl_id', '=', product_tmpl_id.id)])
                        if product_ids:
                            product_id = product_ids[0]
                    quote_line_vals.update({
                        'product_id': product_id.id,
                        # 'product_tmpl_id':product_tmpl_id.id
                    })
                    ir_model_id = ir_model_data_obj.create(
                        {
                            'name': data[53],
                            'res_id': product_tmpl_id.id,
                            'model': 'product.template',
                            'module': 'product_template'
                        })
                    logger.info("Product is created with Ir model Id --> {} and Res ID --> {} ".format(ir_model_id,product_tmpl_id.id))
                    if data[2]:
                        ir_model_vender_ids = ir_model_data_obj.search(
                            [('name', '=', data[2]), ('model', '=', 'res.partner'), ('module', '=', 'ms_vendor')])
                        if not ir_model_vender_ids:
                            log_vals = {
                                'operation': 'q_lineitem',
                                'message': 'Vender is not geting in ir model data for product name',
                                'date': datetime.now()
                            }
                            log_obj.create(log_vals)
                        else:
                            svals = {
                                'name': ir_model_vender_ids[0].res_id,
                                'product_tmpl_id': product_tmpl_id[0].id,
                                'min_qty': 1,
                                'product_name': product_tmpl_id[0].name,
                                'product_id': product_id[0].id
                            }
                            if data[18]:
                                svals.update({'price': float(data[18])})
                            s_info_ids = supplier_info_obj.search(
                                [('name', '=', ir_model_vender_ids[0].res_id),
                                 ('product_tmpl_id', '=', product_tmpl_id[0].id)])
                            if not s_info_ids:
                                supplier_info_obj.create(svals)

                else:
                    product_tmpl_id = prod_templ_obj.browse(ir_model_product_templ_id[0].res_id)
                    product_tmpl_id.write(nprd)
                    product_id = prod_obj.search([('product_tmpl_id', '=', product_tmpl_id.id)])
                    quote_line_vals.update({
                        'product_id': product_id[0].id,
                        # 'product_tmpl_id':product_tmpl_id[0].id
                    })
            if not data[6]:
                quote_line_vals.update({'product_uom': product_id[0].uom_id.id})



            if data[14]:
                ir_model_purchase_order_id = ir_model_data_obj.search(
                    [('name', '=', data[14]), ('model', '=', 'purchase.order')])
                if ir_model_purchase_order_id:
                    purchase_order_id = purchase_obj.browse(ir_model_purchase_order_id[0].res_id)
                    quote_line_vals.update({'p_order_id': purchase_order_id[0].id})

            ir_model_ids = ir_model_data_obj.search(
                [('name', '=', data[0]), ('model', '=', 'sale.order.line'), ('module', '=', 'quote.line.item')])

            if ir_model_ids:
                if not quote_line_vals.get('product_uom', False):
                    if product_id:
                        quote_line_vals.update({'product_uom': product_id.uom_id.id})
                quote_line_itemid = quote_line_obj.browse(ir_model_ids[0].res_id)
                quote_line_itemid.write(quote_line_vals)
                logger.info("Quote Order Line is available with Id {} ".format(quote_line_itemid))
            else:
                if not quote_line_vals.get('product_uom', False):
                    if product_id:
                        quote_line_vals.update({'product_uom': product_id.uom_id.id})
                quote_line_itemid = quote_line_obj.create(quote_line_vals)
                # tax
                if data[42] > 0 and data[24] <= 0:
                    msg = "Tax percentage " + str(data[42]) + " found - value entered is $0 for quote Line number " + str(quote_line_itemid.id) + " and MSSQL Line number " + str(data[0])
                    log_vals = {'operation': 'q_lineitem',
                                'message': msg,
                                'date': datetime.now()}
                    log_obj.create(log_vals)

                ir_model_id = ir_model_data_obj.create(
                    {
                        'name': data[0],
                        'res_id': quote_line_itemid.id,
                        'model': 'sale.order.line',
                        'module': 'quote.line.item'
                    })
                logger.info("Quote Order Line is created with Id --> {} and Res Id --> {}".format(ir_model_id,quote_line_itemid.id))
                self.env.cr.commit()

