# -*- coding: utf-8 -*-
from odoo import models, fields, api
import logging
logger = logging.getLogger(__name__)


class ProductCatalogDetails(models.Model):
    _name = 'product.catalog.details'
    _rec_name='product_tmpl_id'
    _description='Product Catalog Details'
    
    sequence=fields.Char('Sequence')
    code=fields.Char('Code')
    desc=fields.Text('Description')
    product_tmpl_id=fields.Many2one('product.template','Template Id')
    
    

class ProductTemplate(models.Model):
    _inherit='product.template'
    
    pro_catal_detail_ids=fields.One2many('product.catalog.details','product_tmpl_id','Catalog Detail Id')
    