# -*- coding: utf-8 -*-

from odoo import models, fields, api
import datetime
from datetime import datetime
from dateutil.parser import parse
from bs4 import BeautifulSoup
import re
import logging

logger = logging.getLogger(__name__)


class RFQ(models.Model):
    _inherit = 'purchase.order'

    @api.multi
    def get_rfq_lines(self, conn):
        rfq_line_list = "SELECT TOP(500) [IDEAL Item #] as ID,\
                        [Client] as client_id,\
                        [Vendor] as vendor_id,\
                        [Purchasing] as purchasing_id,\
                        [Purchase Quantity] as purchasing_qty,\
                        [Selling Quantity] as selling_qty,\
                        [Unit] as unit,\
                        [Item Description] as item_description,\
                        [Catalog ID] as catalog_id,\
                        [Inventory ID] as inventory_id,\
                        [User Code 1] as user_code_1,\
                        [User Code 2] as user_code_2,\
                        [Proposal #] as quote_number,\
                        [Sales Code] as sales_code,\
                        [PO #] as purchase_number,\
                        [Invoice #] as invoice_number,\
                        [Purchase Unit Cost] as purchase_unit_cost,\
                        [Purchase Cost] as purchase_cost,\
                        [Selling Unit Cost] as selling_unit_cost,\
                        [Selling Cost] as selling_cost,\
                        [Freight] as freight,\
                        [Crating] as crating,\
                        [Installation] as install,\
                        [Other] as other_charges,\
                        [Sales Tax] as sales_tax,\
                        [Purchase Freight] as purchase_freight,\
                        [Purchase Crating] as purchase_crating,\
                        [Purchase Installation] as purchase_installation,\
                        [Purchase Other] as purchase_other,\
                        [Purchase Sales Tax] as purchase_sales_tax,\
                        [Selling Discount Percent] as selling_discount_perc,\
                        [Selling Discount Unit] as selling_discount_unit,\
                        [Selling Discount] as selling_discount,\
                        [Purchase Discount Percent] as purchase_discount_perc,\
                        [Purchase Discount Unit] as purchase_discount_unit,\
                        [Status] as status,\
                        [Sidemark] as sidemark,\
                        [CFA] as cfa,\
                        [Reserve] as reserve,\
                        [Date Requested] as date_requested,\
                        [Date Shipped] as date_shipped,\
                        [Confirmation Date] as confirmation_date,\
                        [Tax Percent 1] as tax_percentage,\
                        [Entered By] as entered_by,\
                        [Entered Date] as entered_date,\
                        [Commission Sales Percent] as comm_sales_perc,\
                        [Commission Sales] as comm_sales,\
                        [Catalog Option] as catalog_option,\
                        [Specifier Client] as client_specifier,\
                        [Salesperson 2] as salesperson_2, \
                        [Item Ordering] as item_sequence,\
                        [Client Description] as client_description,\
                        [Vendor Description] as vendor_description,\
                        CONCAT([Catalog ID], '-', [Catalog OPTION], '-',[Vendor]) as NewProductID \
                        FROM      Item \
                        WHERE [PO #] IS NULL \
                        ORDER BY [Entered Date] DESC"

        conn.execute(rfq_line_list)
        conn_data = conn.fetchall()
        logger.info("Total Number of RFQ lines getting are ---- %s" % (len(conn_data)))
        rfq_order_lines = {}
        for data in conn_data:
            if data[12] not in rfq_order_lines.keys():
                rfq_order_lines.update({str(data[12]): [data]})
            else:
                rfq_order_lines[str(data[12])].append(data)
        return rfq_order_lines

    @api.multi
    def import_rfq(self, conn):
        ir_model_data_obj = self.env['ir.model.data']
        partner_obj = self.env['res.partner']
        log_obj = self.env['log.management']
        res_state_obj = self.env['res.country.state']
        user_obj = self.env['res.users']
        purchase_obj = self.env['purchase.order']
        terms_obj = self.env['account.payment.term']
        carrier_obj = self.env['delivery.carrier']
        msg_obj = self.env['mail.message']
        seq_obj = self.env['ir.sequence']

        rfq_lines = purchase_obj.get_rfq_lines(conn)
        rfq_list = "SELECT TOP(500) \
                   p.[Client]as client,\
                   p.[Proposal Item] as quote_number_int,\
                   p.[Proposal #] as quote_number_txt,\
                   p.[Proposal Date] as quote_date,\
                   p.[Vendor] as supplier,\
                   p.[Purchasing]  as purchasing_location,\
                   p.[Salesperson] as salesperson,\
                   p.[Ship Via] as ship_via,\
                   p.[Terms] as terms,\
                   p.[Freight Terms] as shipping_terms,\
                   p.[FOB] as FOB,\
                   p.[Attention] as attention_to,\
                   p.[Ship To] as ship_to,\
                   p.[Ship To Name] as ship_to_name,\
                   p.[Ship To Address] as ship_address1,\
                   p.[Ship To Address2] as ship_address2,\
                   p.[Ship To City] as ship_to_city,\
                   p.[Ship To State] as ship_to_state,\
                   p.[Ship To Zip]  as ship_to_zip,\
                   p.[Ship To Attention] as ship_to_attention,\
                   p.[Ship To Phone] as ship_to_phone,\
                   p.[Ship To Fax] as ship_to_fax,\
                   p.[Client PO] as client_po,\
                   p.[Client Account] as client_account_number,\
                   p.[Date Requested] as date_requested,\
                   p.[Sidemark] as sidemark,\
                   p.[Confirmation Date] as confirm_date,\
                   p.[Special Instructions] as special_instructions,\
                   p.[Notes] as chatter,\
                   p.[Client Ship Via] as client_ship_via,\
                   p.[Client Terms] as client_terms,\
                   p.[Client Freight Terms] as client_freight_terms, \
                   p.[Specifier Client] as specifier,\
                   p.[Salesperson 2] as salesperson_2,\
                  a.[Address ID]as ID,\
                  a.[Type]as TYPE,\
                  a.[Active]as ACTIVE,\
                  a.[Name]as NAME,\
                  a.[Address]as ADDRESS_LINE1,\
                  a.[Address 2]as ADDRESS_LINE2,\
                  a.[City]as CITY,\
                  a.[State]as STATE,\
                  a.[Zip]as ZIP,\
                  a.[Country]as COUNTRY,\
                  a.[Mailing Address]as MAIL_ADDRESS_LINE1,\
                  a.[Mailing Address 2]as MAIL_ADDRESS_LINE2,\
                  a.[Mailing City]as MAIL_CITY,\
                  a.[Mailing State]as MAIL_STATE,\
                  a.[Mailing Zip]as MAIL_ZIP,\
                  a.[Mailing Country]as MAIL_COUNTRY,\
                  a.[Attention]as MAIL_ADDRESS_NAME,\
                  a.[Phone]as PHONE,\
                  a.[Fax]as FAX,\
                  a.[Email]AS EMAIL,\
                  a.[Web Site]as WEBSITE,\
                  a.[Client Sidemark]as SIDEMARK,\
                  a.[Salesperson]as SALESPERSON,\
                  a.[Ship Via]as SHIPVIA,\
                  a.[Terms]as TERMS,\
                  a.[Freight Terms]as FREIGHT_TERMS,\
                  a.[FOB]as FOB,\
                  a.[Specifier]as SPECIFIER,\
                  a.[Federal ID]as FEDERALID,\
                  a.[Social Security #]as SSN,\
                  a.[Entered Date]as ENTERED_DATE,\
                  a.[Changed By]as CHANGED_BY,\
                  a.[Changed Date]as CHANGED_DATE,\
                  a.[Taxable Selling]as SALES_TAX,\
                  a.[Taxable Freight]as FREIGHT_TAXABLE,\
                  a.[Taxable Crating]as CRATING_TAXABLE,\
                  a.[Taxable Other]as OTHER_TAXABLE,\
                  a.[Client Ship Via]AS CLIENT_SHIP_VIA,\
                  a.[Client Terms]AS CLIENT_TERMS,\
                  a.[Client Freight Terms]AS CLIENT_FREIGHT_TERMS,\
                  a.[Sales Territory]as SALES_TERRITORY \
                   FROM   Proposal p INNER JOIN  Item i on  p.[Proposal #] = i.[Proposal #] INNER JOIN dbo.Address a on a.[Address ID] = p.[Vendor]\
                   WHERE  i.[PO #] IS NULL "\
                   "ORDER BY   [Proposal Date] desc"
        conn.execute(rfq_list)
        conn_data = conn.fetchall()
        logger.info("Total Number of RFQ getting are ---- %s" % (len(conn_data)))
        # try:
        rfq_recs = []
        for data in conn_data:
            if data[2] in rfq_recs:
                continue
            rfq_recs.append(data[2])
            rfq_vals = {}
            if not data[4]:
                log_vals = {
                    'operation': 'rfq',
                    'message': "The vendor of {} id is not found".format(data[0]),
                    'date': datetime.now()
                }
                log_obj.create(log_vals)
            else:
                ir_model_partner_id = ir_model_data_obj.search([('name', '=', data[4]), ('model', '=', 'res.partner')])
                if ir_model_partner_id:
                    partner_purchase_client_id = partner_obj.browse(ir_model_partner_id[0].res_id)
                    rfq_vals.update({'partner_id': partner_purchase_client_id[0].id})
                else:
                    vendor_id = partner_obj.createVendor([data[34:]])
                    if vendor_id:
                        vendor = partner_obj.browse(vendor_id[0])
                        vendor.write({'active': False})
                        rfq_vals.update({'partner_id': vendor.id})
                rfq_vals.update({
                    'date_order': data[3],
                    'client_po': data[22],
                    'client_account': data[23],
                    'sidemark': data[25],
                })

                seq_ids = seq_obj.search([('code', '=', 'purchase.order')])
                if seq_ids:
                    rfq_vals.update(
                        {'name': seq_ids[0].prefix + (str(data[2]).strip()).rjust(seq_ids[0].padding, "0")})
                #for Purchase showroom
                if data[5]:
                    ir_model_partner_id = ir_model_data_obj.search(
                        [('name', '=', data[4]), ('model', '=', 'res.partner')])
                    if ir_model_partner_id:
                        partner_id = partner_obj.browse(ir_model_partner_id.res_id)
                        rfq_vals.update({'purchaser_showroom': partner_id[0].id})

                if data[33]:
                    ir_model_user_id = ir_model_data_obj.search([('name', '=', data[33]), ('model', '=', 'res.users')])
                    if ir_model_user_id:
                        user_id = user_obj.browse(ir_model_user_id.res_id)
                        rfq_vals.update({'salesperson_1': user_id[0].id})

                if data[32]:
                    ir_model_partner_id = ir_model_data_obj.search(
                        [('name', '=', data[32]), ('model', '=', 'res.partner')])
                    if ir_model_partner_id:
                        partner_sale_id = partner_obj.browse(ir_model_partner_id.res_id)
                        rfq_vals.update({'specifier_client': partner_sale_id[0].id})

                if data[8]:
                    terms_id = terms_obj.search([('name', '=', data[8])])
                    if terms_id:
                        rfq_vals.update({'payment_term_id': terms_id[0].id})

                if data[9]:
                    terms_id = terms_obj.search([('name', '=', data[9])])
                    if terms_id:
                        rfq_vals.update({'freight_terms': terms_id[0].id})

                if data[30]:
                    terms_id = terms_obj.search([('name', '=', data[30])])
                    if terms_id:
                        rfq_vals.update({'client_terms': terms_id[0].id})

                if data[31]:
                    terms_id = terms_obj.search([('name', '=', data[31])])
                    if terms_id:
                        rfq_vals.update({'client_freight_terms': terms_id[0].id})

                if data[7]:
                    carrier_id = carrier_obj.search([('name', '=', data[7])])
                    if carrier_id:
                        rfq_vals.update({'ship_via': carrier_id.id})

                if data[29]:
                    carrier_id = carrier_obj.search([('name', '=', data[29])])
                    if carrier_id:
                        rfq_vals.update({'client_ship_via': carrier_id[0].id})

                if data[12]:
                    if data[13]:
                        ir_model_partner_id = ir_model_data_obj.search(
                            [('name', '=', data[12]), ('model', '=', 'res.partner')])
                        if ir_model_partner_id:
                            partner_sale_id = partner_obj.browse(ir_model_partner_id[0].res_id)
                            if partner_sale_id and partner_sale_id.id in partner_obj.ids:

                                partner_ship_vals = {'parent_id': partner_sale_id.id, 'type': 'delivery'}
                                if data[17]:
                                    state_id = res_state_obj.search(
                                        ['|', ('name', '=', data[17]), ('code', '=', data[7])])
                                    if state_id:
                                        partner_ship_vals.update({'state_id': state_id[0].id})

                                partner_ship_vals.update(
                                    {'name': data[13], 'street': data[14], 'street2': data[15], 'fax': data[21]})

                                if data[16]:
                                    partner_ship_vals.update({'city': data[16]})

                                if data[20]:
                                    partner_ship_vals.update({'phone': data[20]})

                                if data[18]:
                                    partner_ship_vals.update({'zip': data[18], })

                                shipping_address_ids = partner_obj.search(
                                    ['|', '|', '|', '|', ('street', '=', data[14]), ('street2', '=', data[15]),
                                     ('city', '=', data[16]), ('zip', '=', data[18]), ('name', '=', data[13]),
                                     ('parent_id', '=', partner_sale_id.id)])
                                if shipping_address_ids:
                                    partner_obj.write(partner_ship_vals)
                                    logger.info("The child ids are writed")
                                else:
                                    partner_obj.create(partner_ship_vals)
                                    logger.info("The child ids are created")

                ir_model_ids = ir_model_data_obj.search([('name', '=', data[2]), ('model', '=', 'purchase.order'),('module','=','rfq.order')])
                if ir_model_ids:
                    rfq_id = self.browse(ir_model_ids[0].res_id)
                    rfq_id.write(rfq_vals)
                    if data[28]:
                        msg_ids = msg_obj.search([('model','=','sale.order'), ('res_id','=',rfq_id.id),('body','=',data[28])])
                        if not msg_ids:
                            rfq_id.message_post(body=data[28])
                    logger.info("RFQ is available with Id {} ".format(rfq_id.id))
                else:
                    rfq_id = self.with_context(tracking_disable=False).create(rfq_vals)
                    if data[28]:
                        rfq_id.message_post(body=data[28])
                    logger.info("RFQ is created with Id {} ".format(rfq_id.id))
                    ir_model_data_obj.create(
                        {
                            'name': data[2],
                            'res_id': rfq_id.id,
                            'model': 'purchase.order',
                            'module': 'rfq.order'
                        })



            # creating rfq order lines
            if rfq_lines.get(data[2], False):
                purchase_obj.create_rfq_order_lines(rfq_id, rfq_lines.get(data[2]))
            else:
                log_vals = {'operation': 'rfq',
                            'message': "Lines not found for RFQ Order having Odoo id {} and MSSQL id {}".format(
                                rfq_id.id, data[2]),
                            'date': datetime.now()}
                log_obj.create(log_vals)
            self.env.cr.commit()
        # except Exception as e:
        #     log_vals = {'operation': 'rfq',
        #                 'message': e, 'date':datetime.now() }
        #     log_obj.create(log_vals)

        return True

    def create_rfq_order_lines(self, order, lines):
        ir_model_data_obj = self.env['ir.model.data']
        partner_obj = self.env['res.partner']
        log_obj = self.env['log.management']
        uom_obj = self.env['uom.uom']
        uom_categ_obj = self.env['uom.category']
        prod_obj = self.env['product.product']
        prod_templ_obj = self.env['product.template']
        user_obj = self.env['res.users']
        purchase_line_obj = self.env['purchase.order.line']
        tax_obj = self.env['account.tax']
        categ_obj = self.env['product.category']

        for data in lines:
            print("Data ----------->",data)
            rfq_line_vals = {}
            uom_id = False
            rfq_line_vals.update({
                'order_id': order.id,
                'product_qty': data[4],
                'price_unit': data[16],
                'sequence': data[50],
                'date_planned': order.date_order,
            })
            if data[42] > 0:
                tax_ir_model_ids = tax_obj.search(
                    [('name', '=', 'Purchase Tax - ' + str(data[42]).strip()), ('type_tax_use', '=', 'purchase')])
                if tax_ir_model_ids:
                    rfq_line_vals.update({'taxes_id': [(6, 0, tax_ir_model_ids.ids)]})
                else:
                    tax_id = tax_obj.create({
                        'name': 'Purchase Tax - ' + str(data[42]).strip(),
                        'type_tax_use': 'purchase',
                        'amount': float(data[42]),
                        'amount_type': 'percent'
                    })
                    rfq_line_vals.update({'taxes_id': [(6, 0, [tax_id.id])]})
            else:
                rfq_line_vals.update({'taxes_id': []})

            if data[52]:
                if isinstance(data[52], str):
                    vendor_desc = data[52]
                elif data[52] != None:
                    vendor_desc = data[52].decode('ascii', 'ignore').encode('ascii').encode('utf-8')
                    # vendor_desc = re.sub(r'<.*?>', '', vendor_desc)
                    # soup = BeautifulSoup(vendor_desc)
                    # texts = soup.find_all(text=True)
                    # for t in texts:
                    #     newtext = t.replace("&nbsp", " ")
                    #     vendor_desc = t.replace_with(newtext)
                # elif isinstance(data[52], unicode):
                #     vendor_desc = data[52].encode('ascii', 'ignore')
                    # vendor_desc = re.sub(r'<.*?>', '', vendor_desc.encode('utf-8'))
                    # soup = BeautifulSoup(vendor_desc, "lxml")
                    # texts = soup.find_all(text=True)
                    # for t in texts:
                    #     newtext = t.replace("&nbsp", " ")
                    #     vendor_desc = t.replace_with(newtext)
                rfq_line_vals.update({'name': vendor_desc})
            elif data[7]:
                rfq_line_vals.update({'name': data[7]})
            else:
                rfq_line_vals.update({'name': ' '})

            if data[6]:
                uom_ids = uom_obj.search([('name', 'ilike', data[6].strip()), ('category_id.name','=','Unit')])
                print(uom_ids)
                if uom_ids:
                    uom_id = uom_ids[0]
                else:
                    uom_categ_ids = uom_categ_obj.search([('name', '=', 'Unit')])
                    if not uom_categ_ids:
                        uom_categ_id = uom_categ_obj.create({
                            'name': 'Unit'
                        })
                    else:
                        uom_categ_id = uom_categ_ids[0]
                    uom_id = uom_obj.create({
                        'name': data[6].strip(),
                        'category_id': uom_categ_id[0].id,
                        'uom_type':'smaller'
                    })
                    print("uom_idd",uom_id)
                rfq_line_vals.update({'product_uom': uom_id.id})

            if data[2]:
                ir_model_partner_id = ir_model_data_obj.search(
                    [('name', '=', data[2]), ('model', '=', 'res.partner')])
                if ir_model_partner_id:
                    partner_order_id = partner_obj.browse(ir_model_partner_id[0].res_id)
                    rfq_line_vals.update({'partner_id': partner_order_id[0].id})

            if not data[53]:
                log_vals = {
                    'operation': 'rfq_lineitem',
                    'message': "The Product name of RFQ lineitem {} id is not found".format(data[53]),
                    'date': datetime.now()
                }
                log_obj.create(log_vals)
                continue
            else:
                ir_model_product_templ_id = ir_model_data_obj.search(
                    [('name', '=', data[53]), ('model', '=', 'product.template')])
                if not ir_model_product_templ_id:
                    # to create product
                    product_ids = prod_obj.search([('product_tmpl_id.default_code', '=', data[53])])
                    if product_ids:
                        product_id = product_ids[0]
                        if not uom_id:
                            rfq_line_vals.update({'product_uom': product_id.uom_id.id})
                    else:
                        if not uom_id:
                            uom_ids = uom_obj.search([('name', 'ilike', 'Each'), ('category_id.name','=', 'Unit')])
                            if uom_ids:
                                uom_id = uom_ids[0]
                            else:
                                uom_categ_ids = uom_categ_obj.search([('name', '=', 'Unit')])
                                if not uom_categ_ids:
                                    uom_categ_id = uom_categ_obj.create({
                                        'name': 'Unit'
                                    })
                                else:
                                    uom_categ_id = uom_categ_ids[0]
                                uom_id = uom_obj.create({'name': 'Each', 'category_id': uom_categ_id[0].id})
                            rfq_line_vals.update({'product_uom': uom_id.id})
                        p_prd = {
                            'name': data[7] or data[53],
                            'default_code': data[53],
                            'active': False,
                            'uom_id': uom_id.id,
                            'uom_po_id': uom_id.id,
                            'sale_ok': False,
                            'purchase_ok': True,
                            'type': 'product',
                            'purchase_method': 'purchase',
                        }
                        ir_model_categ_id = ir_model_data_obj.search(
                            [('name', '=', data[13]), ('model', '=', 'product.category')])
                        if ir_model_categ_id:
                            categ_id = categ_obj.browse(ir_model_categ_id[0].res_id)
                            p_prd.update({'categ_id': categ_id.id})
                        product_id = prod_templ_obj.create(p_prd)
                        product_ids = prod_obj.search([('product_tmpl_id', '=', product_id.id)])
                        if product_ids:
                            product_id = product_ids[0]
                    rfq_line_vals.update({'product_id': product_id.id})
                else:
                    product_res_id = prod_templ_obj.browse(ir_model_product_templ_id[0].res_id)
                    product_id = prod_obj.search([('product_tmpl_id', '=', product_res_id.id)])
                    rfq_line_vals.update({'product_id': product_id[0].id})

            if data[43]:
                ir_model_user_id = ir_model_data_obj.search(
                    [('name', '=', data[43]), ('model', '=', 'res.users')])
                if ir_model_user_id:
                    user_id = user_obj.browse(ir_model_user_id[0].res_id)
                    rfq_line_vals.update({'entered_by': user_id[0].id})

            ir_model_ids = ir_model_data_obj.search(
                [('name', '=', data[0]), ('model', '=', 'purchase.order.line')])

            if ir_model_ids:
                rfq_order_lineitem_id = purchase_line_obj.browse(ir_model_ids[0].res_id)
                if not rfq_line_vals.get('product_uom', False):
                    if product_id:
                        rfq_line_vals.update({'product_uom': product_id.uom_id.id})
                rfq_order_lineitem_id.write(rfq_line_vals)
                logger.info("RFQ Line is available with Id {} ".format(rfq_order_lineitem_id.id))
            else:
                if not rfq_line_vals.get('product_uom', False):
                    if product_id:
                        rfq_line_vals.update({'product_uom': product_id.uom_id.id})
                rfq_order_lineitem_id = purchase_line_obj.create(rfq_line_vals)

                # tax
                if data[42] > 0 and data[29] <= 0:
                    msg = "Tax percentage " + str(data[42]) + " found - value entered is $0 for SaleOrder Line number " + str(rfq_order_lineitem_id.id) + " and MSSQL Line number " + str(
                        data[0])
                    log_vals = {'operation': 'sale_order_lineitem',
                                'message': msg,
                                'date': datetime.now()}
                    log_obj.create(log_vals)

                ir_model_data_obj.create({
                    'name': data[0],
                    'res_id': rfq_order_lineitem_id.id,
                    'model': 'purchase.order.line'
                })
                logger.info("RFQ Line is created with Id {} ".format(rfq_order_lineitem_id.id))
