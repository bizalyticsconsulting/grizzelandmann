# -*- coding: utf-8 -*-
from odoo import models, fields, api
from datetime import datetime
from dateutil.parser import parse
from bs4 import BeautifulSoup
import re
import logging
logger = logging.getLogger(__name__)


class PurchaseOrders(models.Model):
    _inherit = 'purchase.order'

    po_seq_int = fields.Integer('PO Sequence Integer')
    
    @api.multi
    def get_purchase_lines(self, conn):
        purchase_order_line_list = "SELECT \
                                    [IDEAL Item #] as ID,\
                                    [Client] as client_id,\
                                    [Vendor] as vendor_id,\
                                    [Purchasing] as purchasing_id,\
                                    [Purchase Quantity] as purchasing_qty,\
                                    [Selling Quantity] as selling_qty,\
                                    [Unit] as unit,\
                                    [Item Description] as item_description,\
                                    [Catalog ID] as catalog_id,\
                                    [Inventory ID] as inventory_id,\
                                    [User Code 1] as user_code_1,\
                                    [User Code 2] as user_code_2,\
                                    [Proposal #] as quote_number,\
                                    [Sales Code] as sales_code,\
                                    [PO #] as purchase_number,\
                                    [Invoice #] as invoice_number,\
                                    [Purchase Unit Cost] as purchase_unit_cost,\
                                    [Purchase Cost] as purchase_cost,\
                                    [Selling Unit Cost] as selling_unit_cost,\
                                    [Selling Cost] as selling_cost,\
                                    [Freight] as freight,\
                                    [Crating] as crating,\
                                    [Installation] as install,\
                                    [Other] as other_charges,\
                                    [Sales Tax] as sales_tax,\
                                    [Purchase Freight] as purchase_freight,\
                                    [Purchase Crating] as purchase_crating,\
                                    [Purchase Installation] as purchase_installation,\
                                    [Purchase Other] as purchase_other,\
                                    [Purchase Sales Tax] as purchase_sales_tax,\
                                    [Selling Discount Percent] as selling_discount_perc,\
                                    [Selling Discount Unit] as selling_discount_unit,\
                                    [Selling Discount] as selling_discount,\
                                    [Purchase Discount Percent] as purchase_discount_perc,\
                                    [Purchase Discount Unit] as purchase_discount_unit,\
                                    [Status] as status,\
                                    [Sidemark] as sidemark,\
                                    [CFA] as cfa,\
                                    [Reserve] as reserve,\
                                    [Date Requested] as date_requested,\
                                    [Date Shipped] as date_shipped,\
                                    [Confirmation Date] as confirmation_date,\
                                    [Tax Percent 1] as tax_percentage,\
                                    [Entered By] as entered_by,\
                                    [Entered Date] as entered_date,\
                                    [Commission Sales Percent] as comm_sales_perc,\
                                    [Commission Sales] as comm_sales,\
                                    [Catalog Option] as catalog_option,\
                                    [Specifier Client] as client_specifier,\
                                    [Salesperson 2] as salesperson_2,\
                                    [Item Ordering] as item_sequence,\
                                    [Client Description] as client_description,\
                                    [Vendor Description] as vendor_description,\
                                    CONCAT([Catalog ID], '-', [Catalog OPTION], '-',[Vendor]) as NewProductID \
                                    FROM  Item \
                                    WHERE   [PO #] IS NOT NULL \
                                    ORDER BY [Entered Date],[Vendor] DESC"
                                    
                                    # AND [PO #] = '14002'\
        conn.execute(purchase_order_line_list)
        conn_data = conn.fetchall()
        logger.info("Total Number of Purchase Orders Lines getting are ---- %s" % (len(conn_data)))
        purchase_order_lines = {}
        for data in conn_data:
            if data[14] not in purchase_order_lines.keys():
                purchase_order_lines.update({data[14]: [data]})
            else:
                purchase_order_lines[data[14]].append(data)
        return purchase_order_lines

    @api.multi
    def import_purchase_orders(self, conn):
        log_obj = self.env['log.management']
        purchase_obj = self.env['purchase.order']
        # Get Purchase Order Lines
        porder_lines = purchase_obj.get_purchase_lines(conn)
        purchase_order_list = "SELECT  \
                              p.[PO #] as ID,\
                              p.[PO Code] as code,\
                              p.[PO Date] as order_date,\
                              p.Client as client,\
                              p.Purchasing as purchaser_showroom,\
                              p.Salesperson as salesperson,\
                              p.Specifier as specifier,\
                              p.[Ship Via] as ship_via, \
                              p.Terms as terms,\
                              p.[Freight Terms] as shipping_terms,\
                              p.FOB as fob,\
                              p.Attention as order_attention,\
                              p.[Ship To] as ship_to,\
                              p.[Ship To Name] as ship_name, \
                              p.[Ship To Address] as shipping_address_1,\
                              p.[Ship To Address2] as shipping_address_2,\
                              p.[Ship To City] as shipping_city,\
                              p.[Ship To State] as shipping_state,\
                              p.[Ship To Zip] as shipping_zip,\
                              p.[Ship To Attention] as shipping_attn_to,\
                              p.[Ship To Phone] as shipping_phone,\
                              p.[Ship To Fax] as shipping_fax,\
                              p.[Client PO] as client_po,\
                              p.[Client Account] as client_account,\
                              p.[Date Requested] as date_requested_crd,\
                              p.Sidemark as sidemark,\
                              p.[Confirmation Date] as confirmation_date,\
                              p.[Date Shipped] as date_shipped,\
                              p.[Special Instructions] as special_instructions,\
                              p.[Notes] as order_notes_chatter,\
                              p.[Client Ship Via] as client_ship_via,\
                              p.[Client Terms] as client_terms,\
                              p.[Client Freight Terms] as client_freight_terms,\
                              p.[Specifier Client] as specifier_client,\
                              p.[Salesperson 2] as salesperson_2, \
                              p.[Vendor] as vendor,\
                              a.[Address ID]as ID,\
                              a.[Type]as TYPE,\
                              a.[Active]as ACTIVE,\
                              a.[Name]as NAME,\
                              a.[Address]as ADDRESS_LINE1,\
                              a.[Address 2]as ADDRESS_LINE2,\
                              a.[City]as CITY,\
                              a.[State]as STATE,\
                              a.[Zip]as ZIP,\
                              a.[Country]as COUNTRY,\
                              a.[Mailing Address]as MAIL_ADDRESS_LINE1,\
                              a.[Mailing Address 2]as MAIL_ADDRESS_LINE2,\
                              a.[Mailing City]as MAIL_CITY,\
                              a.[Mailing State]as MAIL_STATE,\
                              a.[Mailing Zip]as MAIL_ZIP,\
                              a.[Mailing Country]as MAIL_COUNTRY,\
                              a.[Attention]as MAIL_ADDRESS_NAME,\
                              a.[Phone]as PHONE,\
                              a.[Fax]as FAX,\
                              a.[Email]AS EMAIL,\
                              a.[Web Site]as WEBSITE,\
                              a.[Client Sidemark]as SIDEMARK,\
                              a.[Salesperson]as SALESPERSON,\
                              a.[Ship Via]as SHIPVIA,\
                              a.[Terms]as TERMS,\
                              a.[Freight Terms]as FREIGHT_TERMS,\
                              a.[FOB]as FOB,\
                              a.[Specifier]as SPECIFIER,\
                              a.[Federal ID]as FEDERALID,\
                              a.[Social Security #]as SSN,\
                              a.[Entered Date]as ENTERED_DATE,\
                              a.[Changed By]as CHANGED_BY,\
                              a.[Changed Date]as CHANGED_DATE,\
                              a.[Taxable Selling]as SALES_TAX,\
                              a.[Taxable Freight]as FREIGHT_TAXABLE,\
                              a.[Taxable Crating]as CRATING_TAXABLE,\
                              a.[Taxable Other]as OTHER_TAXABLE,\
                              a.[Client Ship Via]AS CLIENT_SHIP_VIA,\
                              a.[Client Terms]AS CLIENT_TERMS,\
                              a.[Client Freight Terms]AS CLIENT_FREIGHT_TERMS,\
                              a.[Sales Territory]as SALES_TERRITORY \
                              FROM dbo.PO p INNER JOIN  Item i on  p.[PO #] = i.[PO #] INNER JOIN dbo.Address a on a.[Address ID] = p.[Vendor] \
                              WHERE  i.[PO #] IS NOT NULL  \
                              ORDER BY  [PO Date] desc"
                              # AND p.[PO #] = '14002' \
                              
        conn.execute(purchase_order_list)
        conn_data = conn.fetchall()
        logger.info("Total Number of Purchase Orders getting are ---- %s" % (len(porder_lines.keys())))
        po_recs = []
        k=1
        for data in conn_data:
            k=k+1
            # try:
            if data[0] in po_recs:
                continue
            po_recs.append(data[0])
            purchase_order_id = self.create_purchase_order(data)
            if not purchase_order_id:
                continue
            if porder_lines.get(data[0], False):
                purchase_order_res = purchase_obj.create_purchase_order_line(purchase_order_id, data[35], data, porder_lines.get(data[0]))
                for po_id in purchase_order_res:
                    po_id.button_confirm()
            else:
                log_vals = {'operation': 'purchase_order',
                            'message': "Lines not found for Purchase Order having Odoo id {} and MSSQL id {}".format(
                                purchase_order_id.id, data[0]),
                            'date': datetime.now()}
                log_obj.create(log_vals)
            self.env.cr.commit()
            # except Exception as e:
            #     msg = "The MSSQL PO # " + str(data[0]) +" is not processed. "+str(e)
            #     log_vals = {'operation': 'purchase_order',
            #         'message': msg, 'date':datetime.now() }
            #     log_obj.create(log_vals)
        # except Exception as e:
        #     log_vals = {'operation': 'purchase_order',
        #                 'message': e, 'date':datetime.now() }
        #     log_obj.create(log_vals)
        return True
    
    def create_purchase_order(self, data):
        ''' Create Purchase Order and return PO '''
        ir_model_data_obj = self.env['ir.model.data']
        partner_obj = self.env['res.partner']
        log_obj = self.env['log.management']
        user_obj = self.env['res.users']
        purchase_obj = self.env['purchase.order']
        res_state_obj = self.env['res.country.state']
        terms_obj = self.env['account.payment.term']
        carrier_obj = self.env['delivery.carrier']
        msg_obj = self.env['mail.message']
        seq_obj = self.env['ir.sequence']
        purchase_order_val = {}
        vendor = False
        purchase_order_id = False
        if data[35]:
            vendor = data[35]
            
        #If vendor passed by the PO line context
        if self.env.context.get('po_line_vendor'):
            vendor = self.env.context.get('po_line_vendor')
            
        if self.env.context.get('poid_other_name'):
            purchase_number_id = self.env.context.get('poid_other_name')
            purchase_order_val.update({'po_seq_int': self.env.context.get('po_seq')})
            if 'PO' in purchase_number_id:
                purchase_number_id = purchase_number_id.split('PO')[1]
        else:
            purchase_number_id = data[0]
            
        if not vendor:
            log_vals = {
                'operation': 'purchase_order',
                'message': "The Vendor name {} of Purchase Order id = {} is not found".format(vendor,purchase_number_id),
                'date': datetime.now()
            }
            log_obj.create(log_vals)
            # continue
            return False
        else:
            ir_model_partner_id = ir_model_data_obj.search(
                [('name', '=', vendor), ('model', '=', 'res.partner'), ('module', '=', 'ms_vendor')])
            if not ir_model_partner_id:
                vendor_id = partner_obj.createVendor([data[36:]])
                if vendor_id:
                    vendor = partner_obj.browse(vendor_id[0])
                    vendor.write({'active': False})
                    purchase_order_val.update({'partner_id': vendor.id})
            else:
                partner_purchase_client_id = partner_obj.browse(ir_model_partner_id[0].res_id)
                purchase_order_val.update({'partner_id': partner_purchase_client_id[0].id})

            purchase_order_val.update({
                'date_order': data[2],
                'client_po': data[22],
                'client_account': data[23],
                'sidemark': data[25],
                'date_planned': data[2],
            })

            seq_ids = seq_obj.search([('code', '=', 'purchase.order')])
            if seq_ids:
                if self.env.context.get('poid_other_name'):
                    po_name = self.env.context.get('poid_other_name')
                else:
                    po_name = seq_ids[0].prefix + (str(purchase_number_id).strip()).rjust(seq_ids[0].padding, "0")
                purchase_order_val.update({'name': po_name})

            if data[4]:
                ir_model_partner_id = ir_model_data_obj.search(
                    [('name', '=', data[4]), ('model', '=', 'res.partner')])
                if ir_model_partner_id:
                    partner_id = partner_obj.browse(ir_model_partner_id.res_id)
                    purchase_order_val.update({'purchaser_showroom': partner_id[0].id})

            if data[34]:
                ir_model_user_id = ir_model_data_obj.search([('name', '=', data[34]), ('model', '=', 'res.users')])
                if ir_model_user_id:
                    user_id = user_obj.browse(ir_model_user_id.res_id)
                    purchase_order_val.update({'salesperson_1': user_id[0].id})

            if data[33]:
                ir_model_partner_id = ir_model_data_obj.search(
                    [('name', '=', data[33]), ('model', '=', 'res.partner')])
                if ir_model_partner_id:
                    partner_sale_id = partner_obj.browse(ir_model_partner_id[0].res_id)
                    purchase_order_val.update({'specifier_client': partner_sale_id[0].id})

            if data[8]:
                terms_id = terms_obj.search([('name', '=', data[8])])
                if terms_id:
                    purchase_order_val.update({'payment_term_id': terms_id[0].id})

            if data[9]:
                terms_id = terms_obj.search([('name', '=', data[9])])
                if terms_id:
                    purchase_order_val.update({'freight_terms': terms_id[0].id})

            if data[31]:
                terms_id = terms_obj.search([('name', '=', data[31])])
                if terms_id:
                    purchase_order_val.update({'client_terms': terms_id[0].id})

            if data[32]:
                terms_id = terms_obj.search([('name', '=', data[32])])
                if terms_id:
                    purchase_order_val.update({'client_freight_terms': terms_id[0].id})

            if data[7]:
                carrier_id = carrier_obj.search([('name', '=', data[7])])
                if carrier_id:
                    purchase_order_val.update({'ship_via': carrier_id[0].id})

            if data[30]:
                carrier_id = carrier_obj.search([('name', '=', data[30])])
                if carrier_id:
                    purchase_order_val.update({'client_ship_via': carrier_id[0].id})

            if data[12]:
                if data[13]:
                    ir_model_partner_id = ir_model_data_obj.search(
                        [('name', '=', data[12]), ('model', '=', 'res.partner')])
                    if ir_model_partner_id:
                        partner_sale_id = partner_obj.browse(ir_model_partner_id[0].res_id)
                        if partner_sale_id and partner_sale_id.id in partner_obj.ids:

                            partner_ship_vals = {'parent_id': partner_sale_id.id, 'type': 'delivery'}
                            if data[17]:
                                state_id = res_state_obj.search(
                                    ['|', ('name', '=', data[17]), ('code', '=', data[17])])
                                if state_id:
                                    partner_ship_vals.update({'state_id': state_id[0].id})

                            partner_ship_vals.update({
                                'name': data[13],
                                'street': data[14],
                                'street2': data[15],
                                'fax': data[21]
                            })

                            if data[16]:
                                partner_ship_vals.update({'city': data[16]})

                            if data[20]:
                                partner_ship_vals.update({'phone': data[20]})

                            if data[18]:
                                partner_ship_vals.update({'zip': data[18], })

                            shipping_address_ids = partner_obj.search(
                                ['|', '|', '|', '|', ('street', '=', data[14]), ('street2', '=', data[15]),
                                 ('city', '=', data[16]), ('zip', '=', data[18]), ('name', '=', data[13]),
                                 ('parent_id', '=', partner_sale_id.id)])
                            if shipping_address_ids:
                                partner_obj.write(partner_ship_vals)
                                # logger.info("The child ids are writed")
                            else:
                                partner_obj.create(partner_ship_vals)
                                # logger.info("The child ids are created")

            ir_model_ids = ir_model_data_obj.search(
                [('name', '=', purchase_number_id), ('model', '=', 'purchase.order'), ('module', '=', 'purchase_order')])
            if ir_model_ids:
                purchase_order_id = self.browse(ir_model_ids[0].res_id)
                purchase_order_id.write(purchase_order_val)
                if data[29]:
                    msg_ids = msg_obj.search(
                        [('model', '=', 'purchase.order'), ('res_id', '=', purchase_order_id.id),
                         ('body', '=', data[29])])
                    if not msg_ids:
                        purchase_order_id.message_post(body=data[29])
                # logger.info("Purchase Order is available with Id {} ".format(purchase_order_id))
            else:
                purchase_order_id = self.with_context(tracking_disable=False).create(purchase_order_val)
                if data[29]:
                    purchase_order_id.message_post(body=data[29])
                ir_d = ir_model_data_obj.create({
                    'name': purchase_number_id,
                    'res_id': purchase_order_id.id,
                    'model': 'purchase.order',
                    'module': 'purchase_order'
                })
                # logger.info("Purchase Order is created with Id {} ".format(purchase_order_id))
            return purchase_order_id

    def create_purchase_order_line(self, order, po_vendor, po_data, lines):
        ''' Create purchase order line with respect to order and data. But if any po line vendor not matched with the PO vendor then create new PO '''
        purchase_order_res = self.env['purchase.order']
        ir_model_data_obj = self.env['ir.model.data']
        partner_obj = self.env['res.partner']
        log_obj = self.env['log.management']
        uom_obj = self.env['uom.uom']
        uom_categ_obj = self.env['uom.category']
        prod_obj = self.env['product.product']
        user_obj = self.env['res.users']
        prod_templ_obj = self.env['product.template']
        purchase_line_obj = self.env['purchase.order.line']
        tax_obj = self.env['account.tax']
        categ_obj = self.env['product.category']
        ctx = self.env.context.copy()
        purchase_order_res |= order
        org_purchase_order = order
        for data in lines:
            #Check for the Vendors.if PO vendor is not matched with the product vendor then create new PO with PO postfix-1
            if data[2] != po_vendor:
                max_seq_no = max([i.po_seq_int for i in purchase_order_res])
                already_exist_po = False
                #Fetch vendor partner ID
                ir_model_partner_id = ir_model_data_obj.search([('name', '=', data[2]), ('model', '=', 'res.partner'), ('module', '=', 'ms_vendor')])
                if ir_model_partner_id:
                    already_exist_po = purchase_order_res.filtered(lambda x: x.partner_id.id == ir_model_partner_id[0].res_id)
                if already_exist_po:
                    order = already_exist_po
                else:
                    poid_other_name = str(order.name).split('-')[0] +'-'+str(max_seq_no + 1)
                    ctx.update({'po_line_vendor': data[2], 'poid_other_name': poid_other_name, 'po_seq': max_seq_no + 1})
                    order = self.with_context(ctx).create_purchase_order(po_data)
                    logger.info("Purchase Order Line Vendor not matched, create new Purchase Order :%s : %s.", order,poid_other_name)
                purchase_order_res |= order
            else:
                order = org_purchase_order
                
            purchase_order_line_val = {}
            uom_id = False
            purchase_order_line_val.update({
                'order_id': order.id,
                'product_qty': data[4],
                'price_unit': data[16],
                'sequence': data[50],
                'date_planned': order.date_order
            })
            if data[42] > 0:
                tax_ir_model_ids = tax_obj.search(
                    [('name', '=', 'PT-' + str(data[42]).strip()), ('type_tax_use', '=', 'purchase')])
                if tax_ir_model_ids:
                    purchase_order_line_val.update({'taxes_id': [(6, 0, tax_ir_model_ids.ids)]})
                else:
                    tax_id = tax_obj.create({
                        'name': 'PT-' + str(data[42]).strip(),
                        'type_tax_use': 'purchase',
                        'amount': float(data[42]),
                        'amount_type': 'percent'
                    })
                    purchase_order_line_val.update({'taxes_id': [(6, 0, [tax_id.id])]})
            else:
                purchase_order_line_val.update({'taxes_id': []})
            if data[52]:
                if isinstance(data[52], str):
                    vendor_desc = data[52]
                elif data[52] != None:
                    vendor_desc = data[52].encode('ascii', 'ignore')
                    # vendor_desc = re.sub(r'<.*?>', '', vendor_desc.encode('utf-8'))
                    # soup = BeautifulSoup(vendor_desc, "lxml")
                    # texts = soup.find_all(text=True)
                    # for t in texts:
                    #     newtext = t.replace("&nbsp", " ")
                    #     vendor_desc = t.replace_with(newtext)
                # elif isinstance(data[52], unicode):
                    # vendor_desc = data[52].encode('ascii', 'ignore')
                    # vendor_desc = re.sub(r'<.*?>', '', vendor_desc.encode('utf-8'))
                    # soup = BeautifulSoup(vendor_desc, "lxml")
                    # texts = soup.find_all(text=True)
                    # for t in texts:
                    #     newtext = t.replace("&nbsp", " ")
                    #     vendor_desc = t.replace_with(newtext)
                purchase_order_line_val.update({'name': vendor_desc})
            elif data[7]:
                purchase_order_line_val.update({'name': data[7]})
            else:
                purchase_order_line_val.update({'name': ' '})

            if data[6] and data[6].isalpha():
                uom_ids = uom_obj.search([('name', 'ilike', data[6].strip()), ('category_id.name','=', 'Unit')])
                if uom_ids:
                    uom_id = uom_ids[0]
                else:
                    uom_categ_ids = uom_categ_obj.search([('name', '=', 'Unit')])
                    if not uom_categ_ids:
                        uom_categ_id = uom_categ_obj.create({
                            'name': 'Unit'
                        })
                    else:
                        uom_categ_id = uom_categ_ids[0]
                    uom_id = uom_obj.create({
                        'name': data[6].strip(),
                        'category_id': uom_categ_id[0].id,
                        'uom_type': 'smaller'
                    })
                purchase_order_line_val.update({'product_uom': uom_id.id})

            if data[2]:
                ir_model_partner_id = ir_model_data_obj.search(
                    [('name', '=', data[2]), ('model', '=', 'res.partner'),('module','=', 'ms_vendor')])
                if not ir_model_partner_id:
                    log_vals = {'operation': 'pur_lineitem',
                                'message': "The Partner name {} of Purchase lineitem id = {} is not found".format(data[2], data[0]),
                                'date': datetime.now()}
                    log_obj.create(log_vals)
                else:
                    partner_order_id = partner_obj.browse(ir_model_partner_id[0].res_id)
                    purchase_order_line_val.update({'partner_id': partner_order_id[0].id})

            if not data[53]:
                log_vals = {'operation': 'pur_lineitem',
                            'message': "The Product name {} of Purchase lineitem id = {} is not found".format(data[53], data[0]),
                            'date': datetime.now()}
                log_obj.create(log_vals)
                continue
            else:
                if isinstance(data[53], str):
                    product_data = data[53]
                    # product_data = re.sub(r'<.*?>', '', product_data.encode('utf-8'))
                elif data[53] != None:
                    product_data = data[53].encode('ascii', 'ignore')
                    # product_data = re.sub(r'<.*?>', '', product_data.encode('utf-8'))
                ir_model_product_templ_id = ir_model_data_obj.search(
                    [('name', '=', product_data), ('model', '=', 'product.template')])
                if not ir_model_product_templ_id:
                    # to create product
                    product_ids = prod_obj.search([('product_tmpl_id.default_code', '=', data[53])])
                    if product_ids:
                        product_id = product_ids[0]
                        if not uom_id:
                            uom_id = product_id.uom_id
                            purchase_order_line_val.update({'product_uom': uom_id.id})

                    else:
                        product_v = {
                            'name': data[7] or data[53],
                            'default_code': data[53],
                            'active': False,
                            'sale_ok': False,
                            'purchase_ok': True,
                            'type': 'product',
                            'purchase_method': 'purchase',
                        }
                        ir_model_categ_id = ir_model_data_obj.search(
                            [('name', '=', data[13]), ('model', '=', 'product.category')])
                        if ir_model_categ_id:
                            categ_id = categ_obj.browse(ir_model_categ_id[0].res_id)
                            product_v.update({'categ_id': categ_id.id})
                        if not uom_id:
                            uom_ids = uom_obj.search([('name', 'ilike', 'Each'),('category_id.name','=', 'Unit')])
                            if uom_ids:
                                uom_id = uom_ids[0]
                            else:
                                uom_categ_ids = uom_categ_obj.search([('name', '=', 'Unit')])
                                if not uom_categ_ids:
                                    uom_categ_id = uom_categ_obj.create({
                                        'name': 'Unit'
                                    })
                                else:
                                    uom_categ_id = uom_categ_ids[0]
                                uom_id = uom_obj.create({'name': 'Each', 'category_id': uom_categ_id[0].id})
                            purchase_order_line_val.update({'product_uom': uom_id.id})

                        product_v.update({
                            'uom_id': uom_id.id,
                            'uom_po_id': uom_id.id
                        })
                        product_id = prod_templ_obj.create(product_v)
                        product_ids = prod_obj.search([('product_tmpl_id', '=', product_id.id)])
                        if product_ids:
                            product_id = product_ids[0]
                    purchase_order_line_val.update({'product_id': product_id.id})
                else:
                    product_res_id = prod_templ_obj.browse(ir_model_product_templ_id[0].res_id)
                    product_id = prod_obj.search([('product_tmpl_id', '=', product_res_id.id)])
                    purchase_order_line_val.update({'product_id': product_id[0].id})

            if data[43]:
                ir_model_user_id = ir_model_data_obj.search([('name', '=', data[43]), ('model', '=', 'res.users')])
                if ir_model_user_id:
                    user_id = user_obj.browse(ir_model_user_id[0].res_id)
                    purchase_order_line_val.update({'entered_by': user_id[0].id})

            ir_model_ids = ir_model_data_obj.search([('name', '=', data[0]), ('model', '=', 'purchase.order.line'),
                                                     ('module', '=', 'purchase.order.line')])
            if ir_model_ids:
                purchase_order_lineitem_id = purchase_line_obj.browse(ir_model_ids[0].res_id)
                if not purchase_order_line_val.get('product_uom', False):
                    if product_id:
                        purchase_order_line_val.update({'product_uom': product_id.uom_id.id})
                #If PO Line Product Qty not changed then remove the 'product_qty' from vals dict. So that PO logs for Product Qty will not create every time.04.02.2019
                if 'product_qty' in purchase_order_line_val and purchase_order_line_val.get('product_qty') == purchase_order_lineitem_id.product_qty:
                    purchase_order_line_val.pop('product_qty')
                purchase_order_lineitem_id.write(purchase_order_line_val)
                # logger.info("Purchase Order Line is available with Id {} ".format(purchase_order_lineitem_id))
            else:
                if not purchase_order_line_val.get('product_uom', False):
                    if product_id:
                        purchase_order_line_val.update({'product_uom': product_id.uom_id.id})
                purchase_order_lineitem_id = purchase_line_obj.create(purchase_order_line_val)
                # tax
                if data[42] > 0 and data[29] <= 0:
                    msg = "Tax percentage " + str(data[42]) + " found - value entered is $0 for PurchaseOrder Line number " + str(purchase_order_lineitem_id.id) + " and MSSQL Line number " + str(data[0])
                    log_vals = {'operation': 'pur_lineitem',
                                'message': msg,
                                'date': datetime.now()}
                    log_obj.create(log_vals)
                ir_model_id = ir_model_data_obj.create({
                    'name': data[0],
                    'res_id': purchase_order_lineitem_id.id,
                    'model': 'purchase.order.line',
                    'module': 'purchase.order.line'})
                # logger.info("Purchase Order Line is created with Id {} ".format(ir_model_id))
        return purchase_order_res

