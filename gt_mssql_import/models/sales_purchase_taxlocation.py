# -*- coding: utf-8 -*-
from odoo import models, fields, api
from datetime import datetime
import logging
logger = logging.getLogger(__name__)


class SalesPurchaseTaxLocation(models.Model):
    _inherit = 'account.tax'

    @api.multi
    def import_tax_location(self, conn):
        ir_model_data_obj = self.env['ir.model.data']
        log_obj = self.env['log.management']

        # Query to get Sales Purchase Taxlocation
        conn.execute("SELECT [Tax Location]as ID,\
                             [Tax Description]as Description,\
                             [Tax Percent 1]as sales_tax,\
                             [Tax Percent 1]as purchase_tax\
                              FROM [Tax Location]")
        conn_data = conn.fetchall()
        logger.info("Total Number of Sales and Purchase Location getting ---- %s" % (len(conn_data)))
        try:
            for data in conn_data:
                if data[2]:
                    sale_amount = ("%.2f" % round(data[2], 2))
                    purchase_amount = ("%.2f" % round(data[3], 2))
                    sale_tax_val = {'tax_description': data[1], 'amount': float(sale_amount), 'type_tax_use': 'sale'}
                    purchase_tax_val = {'tax_description': data[1], 'amount': float(purchase_amount),
                                        'type_tax_use': 'purchase'}

                    # checking for Sales Tax
                    sale_ir_model_ids = ir_model_data_obj.search(
                        [('name', '=', 'ST-' + data[0]), ('model', '=', 'account.tax')])
                    if sale_ir_model_ids:
                        sale_res_id = self.browse(sale_ir_model_ids[0].res_id)
                        sale_res_id.write(sale_tax_val)
                        logger.info("Sales Location is available with name %s and Id %s " % (data[1], sale_res_id.id))
                    else:
                        sale_tax_val.update({'name': 'Sales Tax - ' + data[1]})
                        sale_tax_id = self.create(sale_tax_val)
                        ir_model_data_obj.create(
                            {'name': 'ST-' + data[0], 'res_id': sale_tax_id.id, 'model': 'account.tax'})
                        logger.info("Sale Location created with name %s and Id %s " % (data[1], sale_tax_id.id))

                    # checking for Purchase Tax
                    purchase_ir_model_ids = ir_model_data_obj.search(
                        [('name', '=', 'PT-' + data[0]), ('model', '=', 'account.tax')])
                    if purchase_ir_model_ids:
                        purchase_res_id = self.browse(purchase_ir_model_ids[0].res_id)
                        purchase_res_id.write(purchase_tax_val)
                        logger.info("Purchase Location is available with name %s and Id %s " % (data[1], purchase_res_id.id))
                    else:

                        purchase_tax_val.update({'name': 'Purchase Tax - ' + data[1]})
                        purchase_tax_id = self.create(purchase_tax_val)
                        ir_model_data_obj.create(
                            {'name': 'PT-' + data[0], 'res_id': purchase_tax_id.id, 'model': 'account.tax'})
                        logger.info("Purchase Location created with name %s and Id %s " % (data[1], purchase_tax_id.id))
                else:
                    log_vals = {
                        'operation': 'tax_location',
                        'message': 'Name is not available for the tax location ID %s' % (data[0]),
                        'date': datetime.now()
                    }
                    log_obj.create(log_vals)
                self.env.cr.commit()
        except Exception as e:
            log_vals = {'operation': 'tax_location',
                        'message': e, 'date': datetime.now()}
            log_obj.create(log_vals)
        return True
