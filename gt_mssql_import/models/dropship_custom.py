# -*- coding: utf-8 -*-
from odoo import models, fields, api
from datetime import datetime
import logging
from odoo.tools.safe_eval import safe_eval
logger = logging.getLogger(__name__)


class ShipViaDelivery(models.Model):
    _inherit = 'dropship.custom'

    @api.multi
    def import_dropship_custom_data(self, conn):
        ir_model_data_obj = self.env['ir.model.data']
        log_obj = self.env['log.management']
        partner_obj = self.env['res.partner']

        # Query to get all Carriers from mssql
        conn.execute("SELECT * from [Address Accounts]")
        conn_data = conn.fetchall()
        logger.info("Total Number of Dropship Custom Ids getting ---- %s" % (len(conn_data)))
        try:
            for data in conn_data:
                # Log if Description is not available
                ir_model_dropship_customer_id = ir_model_data_obj.search(
                    [('name', '=', data[0]), ('model', '=', 'res.partner'), ('module', '=', 'ms_customer')])
                if ir_model_dropship_customer_id:
                    customer_id = partner_obj.browse(ir_model_dropship_customer_id[0].res_id)
                    if not customer_id:
                        log_vals = {'operation': 'dropship_custom',
                                    'message': "Customer Id not found for DropShip Custom  {}".format(data[0]),
                                    'date': datetime.now()}
                        log_obj.create(log_vals)
                        continue
                    else:
                        dropship_vals = {
                            'terms': data[3],
                            'freight_terms': data[4],
                            'account_no': data[5],
                            'note': data[6],
                        }


                        dropship_vals.update({'dropship_id': customer_id[0].id})
                        if data[2]:
                            ship_via_id = self.env['dropship.custom.shipvia'].search([('name', '=', data[2])])
                            if not ship_via_id:
                                ship_via_id = self.env['dropship.custom.shipvia'].create({'name': data[2]})
                                dropship_vals.update({'ship_via': ship_via_id.id})
                            else:
                                dropship_vals.update({'ship_via': ship_via_id[0].id})

                        if data[1]:
                            ir_model_dropship_vendor_id = ir_model_data_obj.search(
                                [('name', '=', data[1]), ('model', '=', 'res.partner'),('module','=','ms_vendor')])
                            if ir_model_dropship_vendor_id:
                                vendor_id = partner_obj.browse(ir_model_dropship_vendor_id[0].res_id)
                                dropship_vals.update({'vendor': vendor_id[0].id})

                        dropship_ids = self.search(
                            [('freight_terms', '=', data[4]), ('terms', '=', data[3]),
                             ('dropship_id', '=', customer_id.id), ('note', '=', data[6]), ('account_no', '=', data[5])])
                        if dropship_ids:
                            logger.info("Dropship Ids Updated ----> {}".format(dropship_ids))
                            dropship_ids[0].write(dropship_vals)
                        else:
                            dropship_ids = self.create(dropship_vals)
                            logger.info("Dropship Ids Created ----> {}".format(dropship_ids))
                self.env.cr.commit()
        except Exception as e:
            log_vals = {'operation': 'dropship_custom', 'message': e, 'date': datetime.now()}
            log_obj.create(log_vals)
        return True