# -*- coding: utf-8 -*-

from odoo import models, fields, api
import datetime
from datetime import datetime
import logging
import re
from bs4 import BeautifulSoup

logger = logging.getLogger(__name__)


class SalesOrderConfirmed(models.Model):
    _inherit = 'sale.order'

    @api.multi
    def get_myorder_lines(self, conn):
        sale_order_line_list = "SELECT i.[IDEAL Item #]as ID,\
                                        i.[Client] as client_id,\
                                        i.[Vendor] as vendor_id,\
                                        i.[Purchasing]as purchasing_id,\
                                        i.[Purchase Quantity]as purchasing_qty,\
                                        i.[Selling Quantity]as selling_qty,\
                                        i.[Unit]   as unit,\
                                        i.[Item Description]as item_description,\
                                        i.[Catalog ID]as catalog_id,\
                                        i.[Inventory ID]as inventory_id,\
                                        i.[User Code 1]as user_code_1,\
                                        i.[User Code 2]as user_code_2,\
                                        i.[Proposal #]as quote_number,\
                                        i.[Sales Code]as sales_code,\
                                        i.[PO #]as purchase_number,\
                                        i.[Invoice #]as invoice_number,\
                                        i.[Purchase Unit Cost]as purchase_unit_cost,\
                                        i.[Purchase Cost]as purchase_cost,\
                                        i.[Selling Unit Cost]as selling_unit_cost,\
                                        i.[Selling Cost]as selling_cost,\
                                        i.[Freight]as freight,\
                                        i.[Crating]as crating,\
                                        i.[Installation]as install,\
                                        i.[Other]  as other_charges,\
                                        i.[Sales Tax]as sales_tax,\
                                        i.[Purchase Freight]as purchase_freight,\
                                        i.[Purchase Crating]as purchase_crating,\
                                        i.[Purchase Installation]as purchase_installation,\
                                        i.[Purchase Other]as purchase_other,\
                                        i.[Purchase Sales Tax]as purchase_sales_tax,\
                                        i.[Selling Discount Percent]as selling_discount_perc,\
                                        i.[Selling Discount Unit]as selling_discount_unit,\
                                        i.[Selling Discount]as selling_discount,\
                                        i.[Purchase Discount Percent]as purchase_discount_perc,\
                                        i.[Purchase Discount Unit]as purchase_discount_unit,\
                                        i.[Status] as status,[Sidemark]as sidemark,\
                                        i.[CFA]as cfa,\
                                        i.[Reserve]as reserve,\
                                        i.[Date Requested]as date_requested,\
                                        i.[Date Shipped]as date_shipped,\
                                        i.[Confirmation Date]as confirmation_date,\
                                        i.[Tax Percent 1]as tax_percentage,\
                                        i.[Entered By]as entered_by,\
                                        i.[Entered Date]as entered_date,\
                                        i.[Commission Sales Percent] as comm_sales_perc,\
                                        i.[Commission Sales] as comm_sales,\
                                        i.[Catalog Option] as catalog_option,\
                                        i.[Specifier Client] as client_specifier,\
                                        i.[Salesperson 2] as salesperson_2,\
                                        i.[Item Ordering]as item_sequence,\
                                        i.[Client Description]as client_description,\
                                        i.[Vendor Description]as client_description,\
                                        CONCAT(i.[Catalog ID], '-', i.[Catalog OPTION], '-',i.[Vendor]) as NewProductID, \
                                        i.[Taxable Selling]AS SALES_TAX, \
                                        a.[Account #], \
                                        a.[Ship Via], \
                                        a.[Terms], \
                                        a.[Freight Terms], \
                                        a.[Note] \
                                        FROM Item i LEFT OUTER JOIN [Address Accounts] a on a.[Address ID] = i.[Client] and a.[Vendor] = i.[Vendor] \
                                        WHERE i.[PO #] IS NOT NULL \
                                        ORDER BY i.[Entered Date] DESC"
        conn.execute(sale_order_line_list)
        conn_data = conn.fetchall()
        order_lines = {}
        for data in conn_data:
            if data[14] not in order_lines.keys():
                order_lines.update({data[14]: [data]})
            else:
                order_lines[data[14]].append(data)
        return order_lines

    @api.multi
    def import_confirm_sale_orders(self, conn):
        ir_model_data_obj = self.env['ir.model.data']
        partner_obj = self.env['res.partner']
        log_obj = self.env['log.management']
        res_state_obj = self.env['res.country.state']
        order_obj = self.env['sale.order']
        msg_obj = self.env['mail.message']
        terms_obj = self.env['account.payment.term']
        carrier_obj = self.env['delivery.carrier']
        seq_obj = self.env['ir.sequence']

        # Get Lines
        order_lines = order_obj.get_myorder_lines(conn)
        confirm_sale_order_list = "SELECT  \
                                p.[PO #] as ID,\
                                p.[PO Code]as code,\
                                p.[PO Date]as order_date,\
                                p.Client as client,\
                                p.Purchasing as purchaser_showroom,\
                                p.Salesperson as salesperson, \
                                p.Specifier as specifier,\
                                p.[Ship Via]as ship_via, \
                                p.Terms as terms, \
                                p.[Freight Terms]as shipping_terms, \
                                p.FOB as fob,\
                                p.Attention as order_attention,  \
                                p.[Ship To]as ship_to, \
                                p.[Ship To Name]as ship_name, \
                                p.[Ship To Address]as shipping_address_1, \
                                p.[Ship To Address2]as shipping_address_2, \
                                p.[Ship To City]as shipping_city,  \
                                p.[Ship To State]as shipping_state, \
                                p.[Ship To Zip]as shipping_zip, \
                                p.[Ship To Attention]as shipping_attn_to, \
                                p.[Ship To Phone]as shipping_phone, \
                                p.[Ship To Fax]as shipping_fax,  \
                                p.[Client PO]as client_po,  \
                                p.[Client Account]as client_account, \
                                p.[Date Requested]as date_requested_crd,  \
                                p.Sidemark as sidemark,  \
                                p.[Confirmation Date]as confirmation_date,  \
                                p.[Date Shipped]as date_shipped,   \
                                p.[Payment Client 1]as payment_amount_1, \
                                p.[Payment Client 2]as payment_amount_2, \
                                p.[Payment Client 3]as payment_amount_3, \
                                p.[Payment Date 1]as payment_date_1, \
                                p.[Payment Date 2]as payment_date_2,  \
                                p.[Payment Date 3]as payment_date_3,  \
                                p.[Payment Check 1]as payment_check_1,  \
                                p.[Payment Check 2]as payment_check_2,   \
                                p.[Payment Check 3]as payment_check_3,  \
                                p.[Special Instructions]as special_instructions, \
                                p.[Notes]as order_notes_chatter, \
                                p.[Client Ship Via]as client_ship_via,  \
                                p.[Client Terms]as client_terms,   \
                                p.[Client Freight Terms]as client_freight_terms, \
                                p.[Specifier Client]as specifier_client, \
                                p.[Salesperson 2]as salesperson_2, \
                                a.[Address ID]AS ID,\
                                a.[Type]AS TYPE,\
                                a.[Active]AS ACTIVE, \
                                a.[Name]AS NAME, \
                                a.[Address]AS ADDRESS_LINE1, \
                                a.[Address 2]AS ADDRESS_LINE2, \
                                a.[City]AS CITY, \
                                a.[State]AS STATE,\
                                a.[Zip]AS ZIP, \
                                a.[Country]AS COUNTRY,\
                                a.[Mailing Address]AS MAIL_ADDRESS_LINE1,\
                                a.[Mailing Address 2]AS MAIL_ADDRESS_LINE2, \
                                a.[Mailing City]AS MAIL_CITY, \
                                a.[Mailing State]AS MAIL_STATE,\
                                a.[Mailing Zip]AS MAIL_ZIP,\
                                a.[Mailing Country]AS MAIL_COUNTRY,\
                                a.[Attention]AS MAIL_ADDRESS_NAME,\
                                a.[Phone]AS PHONE,\
                                a.[Fax]AS FAX,\
                                a.[Email]AS EMAIL,\
                                a.[Web Site]AS WEBSITE,\
                                a.[Client Sidemark]AS SIDEMARK,\
                                a.[Salesperson]AS SALESPERSON,\
                                a.[Ship Via]AS SHIPVIA,\
                                a.[Terms]AS TERMS,\
                                a.[Freight Terms]AS FREIGHT_TERMS,\
                                a.[FOB]AS FOB,\
                                a.[Specifier]AS SPECIFIER,\
                                a.[Federal ID]AS FEDERALID,\
                                a.[Social Security #]AS SSN,\
                                a.[Entered Date]AS ENTERED_DATE,\
                                a.[Changed By]AS CHANGED_BY,\
                                a.[Changed Date]AS CHANGED_DATE,\
                                a.[Taxable Selling]AS SALES_TAX,\
                                a.[Taxable Freight]AS FREIGHT_TAXABLE,\
                                a.[Taxable Crating]AS CRATING_TAXABLE,\
                                a.[Taxable Other]AS OTHER_TAXABLE,\
                                a.[Commission Percent]AS COMMISSION_PERCENTAGE,\
                                a.[Client Ship Via]AS CLIENT_SHIP_VIA,\
                                a.[Client Terms]AS CLIENT_TERMS,\
                                a.[Client Freight Terms]AS CLIENT_FREIGHT_TERMS,\
                                a.[Sales Territory]AS SALES_TERRITORY, \
                                a.[Resale #] AS RESALE_NUMBER,\
                                a.[Resale Date] AS RESALE_NUMBER_DATE \
                                FROM    dbo.PO p INNER JOIN  Item i on  p.[PO #] = i.[PO #] INNER JOIN dbo.Address a on a.[Address ID] = p.Client\
                                WHERE i.[PO #] IS NOT NULL  \
                                ORDER BY[PO Date] desc"
        conn.execute(confirm_sale_order_list)
        conn_data = conn.fetchall()
        logger.info("Total Number of Confirm Sale Orders getting are ---- %s  :  %s" % (
        len(conn_data), len(order_lines.keys())))
        # try:
        sale_records = []
        for data in conn_data:
            # try:
            sale_order_val = {}
            if data[0] in sale_records:
                continue
            sale_records.append(data[0])
            if not data[3]:
                log_vals = {'operation': 'con_sale',
                            'message': "Client not getting from mssql of Sale Order mssql ID {} of Sale Order {}".format(
                                data[3], data[14]),
                            'date': datetime.now()}
                log_obj.create(log_vals)
            else:
                ir_model_partner_id = ir_model_data_obj.search(
                    [('name', '=', data[3]), ('model', '=', 'res.partner')])
                if ir_model_partner_id:
                    partner_sale_client_id = partner_obj.browse(ir_model_partner_id.res_id)
                    sale_order_val.update({'partner_id': partner_sale_client_id[0].id})
                else:
                    customer_id = partner_obj.with_context({
                        'default_active': False,
                    }).createCustomer([data[44:]])
                    if customer_id:
                        sale_order_val.update({'partner_id': customer_id[0]})

                sale_order_val.update({
                    'date_order': data[2],
                    'specifier': data[6],
                    'x_ship_via': data[7],
                    'x_showroom_terms': data[8],
                    'x_freight_terms': data[9],
                    'x_fob': data[10],
                    'x_client_po': data[22],
                    'x_client_account': data[23],
                    'confirmation_date': data[26] if data[26] else data[74] if data[74] else datetime.now(),
                    'x_order_shipdate': data[27],
                    'x_special_inst': data[37],
                })

                seq_ids = seq_obj.search([('code', '=', 'sale.order')])
                if seq_ids:
                    sale_order_val.update(
                        {'name': seq_ids[0].prefix + (str(data[0]).strip()).rjust(seq_ids[0].padding, "0")})

                if isinstance(data[25], str):
                    x_sidemark = data[25]
                    # x_sidemark = data[25].decode('ascii', 'ignore').encode('ascii')  # note: this removes the character and encodes back to string.
                # elif isinstance(data[25], unicode):
                #     x_sidemark = data[25].encode('ascii', 'ignore')
                else:
                    x_sidemark = ''
                sale_order_val.update({'x_sidemark': x_sidemark})

                # for show room and sales pe
                if data[4]:
                    ir_model_partner_id = ir_model_data_obj.search(
                        [('name', '=', data[4]), ('model', '=', 'res.partner')])
                    if ir_model_partner_id:
                        partner_sale_id = partner_obj.browse(ir_model_partner_id.res_id)
                        sale_order_val.update({'x_showroom': partner_sale_id[0].id})

                        if data[5]:
                            sales_partner_id = partner_obj.search(
                                [('parent_id', '=', partner_sale_id.id), ('type', '=', 'contact'),
                                 ('name', '=', data[5])])
                            if sales_partner_id:
                                sale_order_val.update({'x_salesperson1': sales_partner_id[0].id})
                            else:
                                s_p_id = partner_obj.create(
                                    {'name': data[5], 'parent_id': partner_sale_id.id, 'type': 'contact'})
                                sale_order_val.update({'x_salesperson1': s_p_id.id})

                # for Sales Person and sales person 2
                if data[43]:
                    ir_model_partner_id = ir_model_data_obj.search(
                        [('name', '=', data[43]), ('model', '=', 'res.partner')])
                    if ir_model_partner_id:
                        partner_sale_id = partner_obj.browse(ir_model_partner_id.res_id)
                        sale_order_val.update({'x_salesperson2': partner_sale_id[0].id})

                ir_user_model_ids = ir_model_data_obj.search(
                    [('name', '=', data[5]), ('model', '=', 'res.users')])
                # If their is existing ir_model_id means we have created it before then it will move in this condition else it will create new
                if ir_user_model_ids:
                    sale_order_val.update({'user_id': ir_user_model_ids[0].res_id})
                else:
                    sale_order_val.update({'user_id': False})

                if data[11]:
                    ir_model_partner_id = ir_model_data_obj.search(
                        [('name', '=', data[11]), ('model', '=', 'res.partner')])
                    if ir_model_partner_id:
                        partner_sale_id = partner_obj.browse(ir_model_partner_id.res_id)
                        sale_order_val.update({'x_attention_to': partner_sale_id[0].id})

                if data[42]:
                    ir_model_partner_id = ir_model_data_obj.search(
                        [('name', '=', data[42]), ('model', '=', 'res.partner')])
                    if ir_model_partner_id:
                        partner_sale_id = partner_obj.browse(ir_model_partner_id.res_id)
                        sale_order_val.update({'specifier_client': partner_sale_id[0].id})

                if data[8]:
                    terms_id = terms_obj.search([('name', '=', data[8])])
                    if terms_id:
                        sale_order_val.update({'payment_term_id': terms_id[0].id})

                if data[40]:
                    terms_id = terms_obj.search([('name', '=', data[40])])
                    if terms_id:
                        sale_order_val.update({'client_terms': terms_id[0].id})

                if data[41]:
                    terms_id = terms_obj.search([('name', '=', data[41])])
                    if terms_id:
                        sale_order_val.update({'client_freight_terms': terms_id[0].id})

                if data[7]:
                    carrier_id = carrier_obj.search([('name', '=', data[7])])
                    if carrier_id:
                        sale_order_val.update({'carrier_id': carrier_id[0].id})

                if data[39]:
                    carrier_id = carrier_obj.search([('name', '=', data[39])])
                    if carrier_id:
                        sale_order_val.update({'client_ship_via': carrier_id[0].id})

                if data[12]:
                    if data[13]:
                        ir_model_partner_ship_id = ir_model_data_obj.search(
                            [('name', '=', data[12]), ('model', '=', 'res.partner')])
                        if ir_model_partner_ship_id:
                            partner_ship_id = partner_obj.browse(ir_model_partner_ship_id[0].res_id)
                            if partner_ship_id and partner_ship_id.id in partner_obj.ids:

                                partner_ship_vals = {'parent_id': partner_ship_id.id, 'type': 'delivery'}
                                if data[17]:
                                    state_id = res_state_obj.search(
                                        ['|', ('name', '=', data[17]), ('code', '=', data[17])])
                                    if state_id:
                                        partner_ship_vals.update({'state_id': state_id[0].id})

                                partner_ship_vals.update(
                                    {
                                        'name': data[13],
                                        'street': data[14],
                                        'street2': data[15],
                                        'fax': data[21]
                                    })

                                if data[16]:
                                    partner_ship_vals.update({'city': data[16]})

                                if data[20]:
                                    partner_ship_vals.update({'phone': data[20]})

                                if data[18]:
                                    partner_ship_vals.update({'zip': data[18], })

                                shipping_address_ids = partner_obj.search(
                                    ['|', '|', '|', '|', ('street', '=', data[14]), ('street2', '=', data[15]),
                                     ('city', '=', data[16]), ('zip', '=', data[18]), ('name', '=', data[13]),
                                     ('parent_id', '=', partner_ship_id.id)])
                                if shipping_address_ids:
                                    partner_obj.write(partner_ship_vals)
                                    logger.info("The child ids are writed")
                                else:
                                    partner_obj.create(partner_ship_vals)
                                    logger.info("The child ids are created")

                ir_model_ids = ir_model_data_obj.search(
                    [('name', '=', data[0]), ('model', '=', 'sale.order'), ('module', '=', 'confirm_sale_order')])

                if ir_model_ids:
                    confirm_sale_id = self.browse(ir_model_ids[0].res_id)
                    confirm_sale_id.write(sale_order_val)
                    if data[38]:
                        msg_ids = msg_obj.search([('model', '=', 'sale.order'), ('res_id', '=', confirm_sale_id.id),
                                                  ('body', '=', data[38])])
                        if not msg_ids:
                            confirm_sale_id.message_post(body=data[38])
                    logger.info("Sale Order is available with Id {} ".format(confirm_sale_id))
                else:
                    confirm_sale_id = self.with_context(tracking_disable=False).create(sale_order_val)
                    if data[38]:
                        confirm_sale_id.message_post(body=data[38])
                    ir_model_id = ir_model_data_obj.create({
                        'name': data[0],
                        'res_id': confirm_sale_id.id,
                        'model': 'sale.order',
                        'module': 'confirm_sale_order'
                    })
                    logger.info("Sale Order is created with Id {} and Order Id {}".format(ir_model_id,
                                                                                          ir_model_id[0].res_id))

            # create sale Order line
            print("order_lines.get(data[0])",order_lines.get(data[0]))
            if order_lines.get(data[0], False):
                order_obj.with_context({
                    'price_unit': data[18]
                }).create_saleorder_line(confirm_sale_id, order_lines.get(data[0]))
                confirm_sale_id.action_confirm()
                confirm_sale_id.write({
                    'confirmation_date': data[26] if data[26] else data[2] if data[2] else datetime.now(),
                })
            else:
                log_vals = {
                    'operation': 'sale_order_lineitem',
                    'message': "Lines not found for Sale order having Odoo id {} and MSsql Database id {}".format(
                        confirm_sale_id.id, data[0]),
                    'date': datetime.now()}
                log_obj.create(log_vals)
            self.env.cr.commit()

            # except Exception as e:
            #     msg = "The MSSQL Sale Order # " + str(data[0]) + " is not processed. " + str(e)
            #     log_vals = {'operation': 'con_sale', 'message': e, 'date': datetime.now()}
            #     log_obj.create(log_vals)

        return True

    def create_saleorder_line(self, order, lines):
        ir_model_data_obj = self.env['ir.model.data']
        partner_obj = self.env['res.partner']
        log_obj = self.env['log.management']
        uom_obj = self.env['uom.uom']
        uom_categ_obj = self.env['uom.category']
        prod_obj = self.env['product.product']
        prod_templ_obj = self.env['product.template']
        sale_line_obj = self.env['sale.order.line']
        user_obj = self.env['res.users']
        purchase_obj = self.env['purchase.order']
        categ_obj = self.env['product.category']
        tax_obj = self.env['account.tax']
        tag_obj = self.env['product.tag']
        supplier_info_obj = self.env['product.supplierinfo']

        for data in lines:
            sale_order_line_val = {}
            uom_id = False
            # So Line Discount calculations...14.02.2019
            try:
                line_discount_per = float(abs(data[31])) * 100 / float(data[18])
            except Exception as e:
                line_discount_per = 0

            sale_order_line_val.update({
                'order_id': order.id,
                'product_uom_qty': data[5],
                'product_qty': data[4],
                'price_unit': data[18],
                'freight': data[20],
                'account_no':data[55],
                'ship_via':data[56],
                'terms':data[57],
                'freight_terms':data[58],
                'note':data[59],
                'crating': data[21],
                'sequence': data[50],
                'install': data[22],
                'other_charges': data[23],
                'sales_tax': data[24],
                'discount': line_discount_per,  # data[30] change 14.02.2019,
                'entered_date': data[44],
            })
            if data[42] > 0 and data[54] == 'Yes':
                saletax_ir_model_ids = tax_obj.search(
                    [('name', '=', 'Sales Tax - ' + str(data[42]).strip()), ('type_tax_use', '=', 'sale')])
                if saletax_ir_model_ids:
                    sale_order_line_val.update({'tax_id': [(6, 0, saletax_ir_model_ids.ids)]})
                else:
                    tax_id = tax_obj.create({
                        'name': 'Sales Tax - ' + str(data[42]).strip(),
                        'type_tax_use': 'sale',
                        'amount': float(data[42]),
                        'amount_type': 'percent'
                    })
                    sale_order_line_val.update({'tax_id': [(6, 0, [tax_id.id])]})
            else:
                sale_order_line_val.update({'tax_id': []})

            if data[51]:
                if isinstance(data[51], str):
                    client_desc = data[51]
                    # client_desc = data[51].encode('ascii', 'ignore').encode('utf-8')
                # elif isinstance(data[51], unicode):
                #     client_desc = data[51].encode('ascii', 'ignore').encode('utf-8')
                sale_order_line_val.update({'name': client_desc})
            elif data[7]:
                sale_order_line_val.update({'name': data[7]})
            else:
                sale_order_line_val.update({'name': ' '})

            if data[6]:
                uom_ids = uom_obj.search([('name', '=ilike', data[6].strip()), ('category_id.name', '=', 'Units')])
                print("uom_ids",uom_ids.name)
                if uom_ids:
                    uom_id = uom_ids[0]
                else:
                    uom_categ_ids = uom_categ_obj.search([('name', '=', 'Units')])
                    if not uom_categ_ids:
                        uom_categ_id = uom_categ_obj.create({
                            'name': 'Units'
                        })
                    else:
                        uom_categ_id = uom_categ_ids[0]
                    uom_id = uom_obj.create({'name': data[6].strip(), 'category_id': uom_categ_id[0].id,'uom_type':'smaller'})
                sale_order_line_val.update({'product_uom': uom_id.id})

            if data[1]:
                ir_model_partner_id = ir_model_data_obj.search([('name', '=', data[1]), ('model', '=', 'res.partner')])
                if not ir_model_partner_id:
                    log_vals = {'operation': 'sale_order_lineitem',
                                'message': "The Customer name {} of Line Item {} id is not found".format(data[1],
                                                                                                         data[0]),
                                'date': datetime.now()}
                    log_obj.create(log_vals)
                else:
                    partner_order_id = partner_obj.browse(ir_model_partner_id.res_id)
                    sale_order_line_val.update({'order_partner_id': partner_order_id[0].id})

            tag_list = []
            if data[10]:
                tag_id = tag_obj.search([('name', '=', data[10])])
                if not tag_id:
                    tag_id = tag_obj.create({
                        'name': data[10],
                        'active': True,
                    })
                    tag_list.append(tag_id[0].id)
                else:
                    tag_list.append(tag_id[0].id)
            if data[11]:
                tag_id = tag_obj.search([('name', '=', data[11])])
                if not tag_id:
                    tag_id = tag_obj.create({
                        'name': data[11],
                        'active': True,
                    })
                    tag_list.append(tag_id[0].id)
                else:
                    tag_list.append(tag_id[0].id)
            if tag_list:
                sale_order_line_val.update({'tag_ids': [(6, 0, tag_list)]})

            if not data[53]:
                log_vals = {'operation': 'sale_order_lineitem',
                            'message': "The Product name {} of Sale Order MSSQL lineitem {} id is not found".format(
                                data[53], data[0]),
                            'date': datetime.now()}
                log_obj.create(log_vals)
                continue
            else:
                new_prd = {
                    'name': data[7] or data[53],
                    'product_default_code': data[53],
                    'default_code': data[53],
                    'active': True,
                    'uom_id': uom_id[0].id if uom_id else False,
                    'uom_po_id': uom_id[0].id if uom_id else False,
                    'sale_ok': True,
                    'purchase_ok': True,
                    'type': 'product',
                    'purchase_method': 'purchase',
                }
                ir_model_product_templ_id = ir_model_data_obj.search(
                    [('name', '=', data[53]), ('model', '=', 'product.template'),('module','=','product_template')])
                if not ir_model_product_templ_id:
                    # to create product
                    product_ids = prod_obj.search([('product_tmpl_id.default_code', '=', data[53])])
                    product_tmpl_id = product_ids[0].product_tmpl_id if product_ids else False
                    if product_ids:
                        product_id = product_ids[0]
                        if not uom_id:
                            uom_id = product_id.uom_id
                            sale_order_line_val.update({'product_uom': uom_id.id})
                    else:
                        product_id = False
                        if not uom_id:
                            uom_ids = uom_obj.search([('name', 'ilike', 'Each' if not data[6] else data[6]), ('category_id.name', '=', 'Units')])
                            if uom_ids:
                                uom_id = uom_ids[0]
                            else:
                                uom_categ_ids = uom_categ_obj.search([('name', '=', 'Units')])
                                if not uom_categ_ids:
                                    uom_categ_id = uom_categ_obj.create({
                                        'name': 'Units'
                                    })
                                else:
                                    uom_categ_id = uom_categ_ids[0]
                                uom_id = uom_obj.create({'name': 'Each', 'category_id': uom_categ_id[0].id,'uom_type':'smaller'})
                            sale_order_line_val.update({'product_uom': uom_id.id})

                        ir_model_categ_id = ir_model_data_obj.search(
                            [('name', '=', data[13]), ('model', '=', 'product.category')])
                        if ir_model_categ_id:
                            categ_id = categ_obj.browse(ir_model_categ_id.res_id)
                            new_prd.update({'categ_id': categ_id.id})
                        if not data[6]:
                            new_prd.update({
                                'uom_id':uom_id[0].id,
                                'uom_po_id':uom_id[0].id
                            })
                        product_tmpl_id = prod_templ_obj.create(new_prd)
                        product_ids = prod_obj.search([('product_tmpl_id', '=', product_tmpl_id.id)])
                        if product_ids:
                            product_id = product_ids[0]
                        if data[2]:
                            ir_model_vender_ids = ir_model_data_obj.search(
                                [('name', '=', data[2]), ('model', '=', 'res.partner'), ('module', '=', 'ms_vendor')])
                            if not ir_model_vender_ids:
                                log_vals = {
                                    'operation': 'sale_order_lineitem',
                                    'message': 'Vender is not geting in ir model data for product name',
                                    'date': datetime.now()
                                }
                                log_obj.create(log_vals)
                            else:
                                svals = {
                                    'name': ir_model_vender_ids[0].res_id,
                                    'product_tmpl_id': product_tmpl_id[0].id,
                                    'min_qty': 1,
                                    'product_name': product_tmpl_id[0].name,
                                    'product_id': product_id[0].id
                                }
                                if data[18]:
                                    svals.update({'price': float(data[18])})
                                s_info_ids = supplier_info_obj.search(
                                    [('name', '=', ir_model_vender_ids[0].res_id),
                                     ('product_tmpl_id', '=', product_tmpl_id[0].id)])
                                if not s_info_ids:
                                    supplier_info_obj.create(svals)
                        ir_model_id = ir_model_data_obj.create(
                            {
                                'name': data[53],
                                'res_id': product_tmpl_id.id,
                                'model': 'product.template',
                                'module': 'product_template'
                            })
                        logger.info("Product is created with Ir model Id --> {} and Res ID --> {} ".format(ir_model_id,
                                                                                                           product_tmpl_id.id))
                    sale_order_line_val.update({
                        'product_id': product_id.id,
                        'product_uom': uom_id.id,
                        'product_tmpl_id': product_tmpl_id.id})

                else:
                    product_tmpl_id = prod_templ_obj.browse(ir_model_product_templ_id[0].res_id)
                    print("product_tmpl_id",product_tmpl_id,product_tmpl_id.uom_id.name)
                    new_prd.update({
                        'uom_id':product_tmpl_id.uom_id.id,
                        'uom_po_id':product_tmpl_id.uom_id.id,
                    })
                    product_tmpl_id.write(new_prd)
                    print("product_tmpl_id222222222", product_tmpl_id,)
                    product_id = prod_obj.search([('product_tmpl_id', '=', product_tmpl_id.id)])
                    sale_order_line_val.update({
                        'product_id': product_id[0].id,
                        'product_tmpl_id':product_tmpl_id[0].id,
                        'product_uom': product_tmpl_id[0].uom_id.id,
                    })
                    print("sale_order_line_val",sale_order_line_val)
            if data[43]:
                ir_model_user_id = ir_model_data_obj.search([('name', '=', data[43]), ('model', '=', 'res.users')])
                if ir_model_user_id:
                    user_id = user_obj.browse(ir_model_user_id.res_id)
                    sale_order_line_val.update({'entered_by': user_id[0].id})

            ir_model_ids = ir_model_data_obj.search(
                [('name', '=', data[0]), ('model', '=', 'sale.order.line'), ('module', '=', 'sale_order_line')])
            if ir_model_ids:
                if not sale_order_line_val.get('product_uom', False):
                    if product_id:
                        sale_order_line_val.update({'product_uom': product_id.uom_id.id})
                sale_order_lineitem_id = sale_line_obj.browse(ir_model_ids[0].res_id)
                print("sale_order_lineitem_id",sale_order_lineitem_id)
                sale_order_lineitem_id.write(sale_order_line_val)
                logger.info("======sale_order_line_val=========>>> {}".format(sale_order_line_val))
                logger.info("Sale Order Line is available with Id {} ".format(sale_order_lineitem_id))
            else:
                if not sale_order_line_val.get('product_uom', False):
                    if product_id:
                        sale_order_line_val.update({'product_uom': product_id.uom_id.id})
                sale_order_lineitem_id = sale_line_obj.create(sale_order_line_val)

                # tax
                if data[42] > 0 and data[25] <= 0:
                    msg = "Tax percentage " + str(
                        data[42]) + " found - value entered is $0 for SaleOrder Line number " + str(
                        sale_order_lineitem_id.id) + " and MSSQL Line number " + str(
                        data[0])
                    log_vals = {'operation': 'sale_order_lineitem',
                                'message': msg,
                                'date': datetime.now()}
                    log_obj.create(log_vals)

                ir_model_data_obj.create({
                    'name': data[0],
                    'res_id': sale_order_lineitem_id.id,
                    'model': 'sale.order.line',
                    'module': 'sale_order_line'
                })
                logger.info("Sale Order Line is created with Id {} ".format(sale_order_lineitem_id))

