# -*- coding: utf-8 -*-
from odoo import models, fields, api
from datetime import datetime
import re
import logging
logger = logging.getLogger(__name__)


class ImportAddressContacts(models.Model):
    _inherit = 'res.partner'

    @api.multi
    def import_address_contacts(self,conn):
        ir_model_data_obj = self.env['ir.model.data']
        log_obj = self.env['log.management']

        # Query to get all address
        address_contact_list = "SELECT TOP(500) * FROM [Address Contact]"
        conn.execute(address_contact_list)
        conn_data = conn.fetchall()
        logger.info("Total Number of Address getting ---- %s" % (len(conn_data)))
        try:
            for data in conn_data:
                if not (data[4] or data[6] or data[2]):
                    log_vals = {
                        'operation': 'add_contacts',
                        'message': "Contact Name or Email or phone not found for MSSQL ID {}".format(data[0]),
                        'date': datetime.now()
                    }
                    log_obj.create(log_vals)
                else:
                    customer_address_val = {'type':'contact'}

                    if isinstance(data[0], str):
                        address_name = data[0]
                        # address_name = re.sub(r'<.*?>', '', address_name.encode('utf-8'))
                    elif data[0] != None:
                        address_name = data[0].decode('ascii', 'ignore').encode('ascii')
                        # address_name = re.sub(r'<.*?>', '', address_name.encode('utf-8'))


                    ir_model_data_id = ir_model_data_obj.search([('name','=',address_name),('model','=','res.partner')])
                    if ir_model_data_id:
                        address_id = self.browse(ir_model_data_id[0].res_id)
                        customer_address_val.update({'parent_id':address_id.id})

                        if data[4] and data[5]:
                            if isinstance(data[4], str) and isinstance(data[5], str):
                                phone = data[4] + "[" +  data[5] + "]"
                                # phone = data[4].encode('ascii', 'ignore') + "[" +  data[5].encode('ascii','ignore')+ "]"
                                # phone = re.sub(r'<.*?>', '', phone.encode('utf-8'))
                            # elif isinstance(data[4], unicode) and isinstance(data[5], unicode):
                            #     phone = data[4].encode('ascii', 'ignore') + "[" + data[5].encode('ascii','ignore') + "]"
                            #     phone = re.sub(r'<.*?>', '', phone.encode('utf-8'))
                            customer_address_val.update({'phone': phone})
                        elif data[4] and not data[5]:
                            if isinstance(data[4], str):
                                phone = data[4]
                                # phone = data[4].encode('ascii', 'ignore').encode('ascii').encode('utf-8')
                                # phone = re.sub(r'<.*?>', '', phone)
                            # elif isinstance(data[4], unicode):
                            #     phone = data[4].encode('ascii', 'ignore')
                            #     phone = re.sub(r'<.*?>', '', phone.encode('utf-8'))
                            customer_address_val.update({'phone': phone})

                        if data[2]:
                            if isinstance(data[2], str):
                                name = data[2]
                                # name = data[2].encode('ascii', 'ignore').encode('ascii')
                                # name = re.sub(r'<.*?>', '', name.encode('utf-8'))
                            # elif isinstance(data[2], unicode):
                            #     name = data[2].encode('ascii', 'ignore')
                            #     name = re.sub(r'<.*?>', '', name.encode('utf-8'))
                            customer_address_val.update({'name': name})

                        if data[3]:
                            if isinstance(data[3], str):
                                function = data[3]
                                # function = data[3].encode('ascii', 'ignore').encode('ascii')
                                # function = re.sub(r'<.*?>', '', function.encode('utf-8'))
                            # elif isinstance(data[3], unicode):
                            #     function = data[3].encode('ascii', 'ignore')
                            #     function = re.sub(r'<.*?>', '', function.encode('utf-8'))
                            customer_address_val.update({'function': function})

                        if data[6]:
                            if isinstance(data[6], str):
                                email = data[6]
                                # email = data[6].encode('ascii', 'ignore').encode('ascii')
                                # email = re.sub(r'<.*?>', '', email.encode('utf-8'))
                            # elif isinstance(data[6], unicode):
                            #     email = data[6].encode('ascii', 'ignore')
                            #     email = re.sub(r'<.*?>', '', email.encode('utf-8'))
                            customer_address_val.update({'email': email})

                        if data[7]:
                            if isinstance(data[7], str):
                                comment = data[7]
                                # comment = data[7].encode('ascii', 'ignore').encode('ascii')
                                # comment = re.sub(r'<.*?>', '', comment.encode('utf-8'))
                            # elif isinstance(data[7], unicode):
                            #     comment = data[7].encode('ascii', 'ignore')
                            #     comment = re.sub(r'<.*?>', '', comment.encode('utf-8'))
                            customer_address_val.update({'comment': comment})
                        contact_item_no = False
                        if data[1]:
                            if isinstance(data[1], str):
                                contact_item_no = data[1]
                                # contact_item_no = data[1].encode('ascii', 'ignore').encode('ascii')
                                # contact_item_no = re.sub(r'<.*?>', '', contact_item_no.encode('utf-8'))
                            # elif isinstance(data[1], unicode):
                            #     contact_item_no = data[1].encode('ascii', 'ignore')
                            #     contact_item_no = re.sub(r'<.*?>', '', contact_item_no.encode('utf-8'))
                            customer_address_val.update({'contact_item_no': contact_item_no})
                        if data[6] and data[4] and data[2]:
                            partner_ids = self.search(
                                [('parent_id', '=', address_id.id), ('email', '=', email),
                                 ('phone', '=', phone), ('name','=', data[2]), ('type', '=', 'contact')])
                        elif data[6] and data[4]:
                            partner_ids = self.search(
                                [('parent_id', '=', address_id.id), ('email', '=', email),('phone', '=', phone),('type', '=', 'contact')])
                        elif data[6] and data[2]:
                            partner_ids = self.search(
                                [('parent_id', '=', address_id.id), ('name', '=', name), ('email', '=', email),('type', '=', 'contact')])
                        elif data[4] and data[2]:
                            partner_ids = self.search(
                                [('parent_id', '=', address_id.id), ('name', '=', name), ('phone', '=', phone),('type', '=', 'contact')])
                        elif data[2]:
                            partner_ids = self.search(
                                [('parent_id', '=', address_id.id), ('name', '=', data[2]),
                                 ('type', '=', 'contact')])
                        elif data[6]:
                            partner_ids = self.search(
                                [('parent_id', '=', address_id.id), ('email', '=', email),
                                 ('type', '=', 'contact')])
                        elif data[4]:
                            partner_ids = self.search(
                                [('parent_id', '=', address_id.id), ('phone', '=', phone),
                                 ('type', '=', 'contact')])

                        if not customer_address_val.get('name', False):
                            customer_address_val.update({'name': customer_address_val.get('email', False) or customer_address_val.get('phone', False)})
                        if not partner_ids:
                            address_id = self.create(customer_address_val)
                            logger.info("Address Created ---- %s" % (address_id))
                        else:
                            self.write(customer_address_val)
                            logger.info("Address available ---- %s" % (partner_ids[0].id))
                    else:
                        log_vals = {
                            'operation': 'add_contacts',
                            'message': "Parent not found for Contact ID {}".format(address_name),
                            'date': datetime.now()
                        }
                        log_obj.create(log_vals)
                    self.env.cr.commit()
        except Exception as e:
            log_vals = {'operation': 'add_contacts',
                        'message': e, 'date': datetime.now()}
            log_obj.create(log_vals)

        return True
