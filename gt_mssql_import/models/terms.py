# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import datetime
import logging
logger = logging.getLogger(__name__)


class PaymentTerms(models.Model):
    _inherit= 'account.payment.term'

    @api.multi
    def import_payment_terms(self,conn):
        ir_model_data_obj = self.env['ir.model.data']
        log_obj = self.env['log.management']
        payment_line_obj = self.env['account.payment.term.line']

        # Query to get all Terms from mssql
        conn.execute("SELECT [PO Terms]as ID,[Description]as description,[PO Deposit Percent]as deposit_percentage,[Net Days]as net_days FROM [PO Terms]")
        conn_data = conn.fetchall()
        logger.info("Total Number of Terms getting ---- %s" % (len(conn_data)))
        try:
            for data in conn_data:
                ir_model_ids = ir_model_data_obj.search([('name', '=', data[0]),('model', '=', 'account.payment.term'),('module','=','terms')])
                if not data[1]:
                    log_vals = {'message': "The Name of this payment term MSSQL ID {} is not available".format(data[0]),
                                'operation': 'terms', 'date': datetime.now()}
                    log_obj.create(log_vals)
                    continue

                terms_val = {'name': data[1], 'note': "Payment Terms : " + data[1]}

                if ir_model_ids:
                    terms_id = self.browse(ir_model_ids[0].res_id)
                    terms_id.write(terms_val)
                    logger.error("Term is available with name %s and Id %s " % (data[1], terms_id))
                else:
                    terms_id = self.create(terms_val)
                    logger.error("Term created with name %s and Id %s " % (data[1], terms_id.id))
                    ir_model_data_obj.create(
                        {'name': data[0], 'res_id': terms_id.id, 'model': 'account.payment.term','module':'terms'})

                terms_line_val_1 = {'payment_id':terms_id.id}
                if data[2]:
                    terms_line_val_1.update({'value':'percent','value_amount':data[2]})

                if data[3] and int(data[3])>0:
                    terms_line_val_1.update({'option':'day_after_invoice_date','days':data[3]})

                payment_term_line_ids = payment_line_obj.search([('value_amount','=',data[2]),('days','=',data[3])])
                if payment_term_line_ids:
                    payment_term_line_ids.write(terms_line_val_1)
                else:
                    payment_line_obj.create(terms_line_val_1)

                self.env.cr.commit()
        except Exception as e:
            log_vals = {'operation': 'terms',
                        'message': e, 'date':datetime.now() }
            log_obj.create(log_vals)

        return True
