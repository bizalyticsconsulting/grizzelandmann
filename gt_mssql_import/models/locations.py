from odoo import api, fields, models
from datetime import datetime
import logging
logger = logging.getLogger(__name__)


class StockLocation(models.Model):
    _inherit = "stock.location"

    @api.multi
    def import_stock_location_data(self, conn):
        ir_model_data_obj = self.env['ir.model.data']
        log_obj = self.env['log.management']

        # Query to get all Locations from mssql
        conn.execute("SELECT [User Code] as id,[Code Type] as code_type,[Code Description]as name FROM [user code] WHERE [User Code] IN (SELECT DISTINCT(location) as location_id FROM INVENTORY)")
        conn_data = conn.fetchall()
        logger.info("Total Number of Location getting ---- %s" % (len(conn_data)))
        try:
            for data in conn_data:
                # Log for required field name
                if not data[2]:
                    log_vals = {'operation': 'locations',
                                'message': "The location id {} isn't having a name which is invalid".format(data[0]),
                                'date': datetime.now()}
                    log_obj.create(log_vals)
                    continue
    
                location_id = self.search([('name','=','Stock'),('location_id.name','=','WH')])
                if location_id:
                    location_vals = {
                        'usage': 'internal',
                        'name': str(data[2]),
                        'location_id': location_id[0].id
                    }
    
                ir_model_ids = ir_model_data_obj.search([('name', '=', data[0]), ('model', '=', 'stock.location'),('module','=','stock')])
                if ir_model_ids:
                    res_id = self.browse(ir_model_ids.res_id)
                    res_id.write(location_vals)
                    logger.info("Location is available with name %s and Id %s " % (data[2], res_id.id))
                else:
                    location_id = self.create(location_vals)
                    ir_model_data_obj.create({'name': data[0], 'res_id': location_id.id, 'model': 'stock.location','module':'stock'})
                    logger.info("Location created with name %s and Id %s " % (data[2], location_id.id))
                self.env.cr.commit()
        except Exception as e:
            log_vals = {
                'operation': 'locations',
                'message': e,
                'date': datetime.now()
            }
            log_obj.create(log_vals)
        return True

