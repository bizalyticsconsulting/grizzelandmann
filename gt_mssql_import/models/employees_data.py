from odoo import api, fields, models
from datetime import datetime
import logging
logger = logging.getLogger(__name__)


class ResUsers(models.Model):
    _inherit = "res.users"

    @api.multi
    def import_users_data(self, conn):
        logger.info("Importing Employee form Mssql---------------------")
        ir_model_data_obj = self.env['ir.model.data']
        log_obj = self.env['log.management']

        # Query to get List of Employee from Mssql
        conn.execute("SELECT\
                        [Employee ID] as id,\
                        [Employee Name] as name,\
                        Email as email,\
                        Active as active \
                        FROM EMPLOYEE")
        conn_data = conn.fetchall()
        logger.info("Total Number of Employee getting ---- %s" % (len(conn_data)))
        try:
            for data in conn_data:
                # Check for employee name and Email if not available log error and continue with other records
                if not data[2] and not data[1]:
                    log_vals = {'operation': 'user',
                                'message': "name and Email is not getting for Mssql ID {}".format( data[0]), 'date': datetime.now()}
                    log_obj.create(log_vals)

                name = ''
                email = ''
                if isinstance(data[1], str):
                    # note: this removes the character and encodes back to string.
                    name = data[1]
                elif data[1] != None:
                    name = data[1].decode('ascii', 'ignore').encode('ascii')

                if isinstance(data[2], str):
                    # note: this removes the character and encodes back to string.
                    email = data[2]
                elif data[2] != None:
                    email = data[2].decode('ascii', 'ignore').encode('ascii')

                user_val = {'name': name,
                            'active': data[3],
                            'email': email,
                            'customer': False
                            }

                ir_model_ids = ir_model_data_obj.search([('name', '=', data[0]), ('model', '=', 'res.users'), ('module','=','employee_msssql')])
                # If their is existing ir_model_id means we have created it before then it will move in this condition else it will create new
                if ir_model_ids:
                    res_id = self.browse(ir_model_ids[0].res_id)
                    logger.info("Employee is available with login %s and Id %s " % (name, res_id.id))
                    res_id.partner_id.write(user_val)
                else:
                    user_id = self.search(['|', ('login', '=', data[1].replace(" ", "")), ('login', '=', email)])
                    if user_id:
                        user_id = self.create({'login': data[1], 'name': data[1], 'active': data[3]})
                        log_vals = {'operation': 'user',
                                    'message': "[Actionable message]duplicate is found. created record with MSSQL ID odoo Login {} and ID :{}".format(
                                        data[0], user_id.login),
                                    'date': datetime.now()}
                        log_obj.create(log_vals)
                        user_id.partner_id.write(user_val)
                        ir_model_data_obj.create({'name': data[0], 'res_id': user_id.id, 'model': 'res.users', 'module':'employee_msssql'})
                    else:
                        if email:
                            user_id = self.create({'login': email, 'name': data[1], 'active': data[3]})
                        else:
                            user_id = self.create(
                                {'login': data[1].replace(" ", ""), 'name': data[1], 'active': data[3]})
                        user_id.partner_id.write(user_val)
                        ir_model_data_obj.create({
                            'name': data[0],
                            'res_id': user_id.id,
                            'model': 'res.users',
                            'module': 'employee_msssql'
                        })
                    logger.info("Employee created with login %s and Id %s " % (name, user_id.id))
                    self.env.cr.commit()
        except Exception as e:
            log_vals = {'operation': 'user',
                        'message': e, 'date': datetime.now()}
            log_obj.create(log_vals)
        return True







