# -*- coding: utf-8 -*-
{
    'name': "gt_mssql_import",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Globalteckz",
    'website': "http://www.globalteckz.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Generic',
    'version': '0.1',
    'sequence':1,

    # any module necessary for this one to work correctly
    'depends': ['base','sale','gt_studio_it_import_fields','dropship_custom','product_tags'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'data/ir_cron.xml',
        'views/category_view.xml',
        'views/configuration_views.xml',
        'views/html_view.xml',
        'views/partner_view.xml',
        'views/product_template.xml',
        'views/log_file_view.xml',
        'wizard/import_datas_wizard_view.xml',
    ],
}
