# -*- coding: utf-8 -*-

from odoo import fields, api, models, _
from itertools import groupby
from datetime import datetime, timedelta
from odoo.exceptions import UserError
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from odoo.tools.misc import formatLang
import odoo.addons.decimal_precision as dp
from dateutil.relativedelta import relativedelta
from odoo.osv import expression


class SaleOrder(models.Model):
    _inherit = "sale.order"
    
    purchase_order_line = fields.One2many('purchase.order', 'sale_order_id', 'Purchase Order')
    purchase_order_bool = fields.Boolean(string='Purchase Order Created', compute = '_compute_purchase_order_created')
    make_po_bool = fields.Boolean('Make PO', copy=False)
    update_so_po = fields.Boolean('Update SO PO', copy=False)
    
    def _compute_purchase_order_created(self):
        for rec in self:
            if rec.purchase_order_line:
                rec.purchase_order_bool = True
                
    @api.multi
    def action_generate_rfq(self):
        purchase_order_obj = self.env['purchase.order']
        for order in self:
            for order_line in order.order_line:
                #Check whether this sale order line's RFQ is created or not.
                if order_line.rfq_created:
                    #if created then check for the any new product added in Its Product's BOM, if yes then create RFQ for that
                    if order_line.product_id.type in ['consu', 'product']:
                       
                        action = self._find_suitable_rule(order_line.product_id)
                        if action:
                          
                            if 'Purchase' in action:
                            	for product_line in self.order_line:
                                    order_line.action_make_po_from_sale(product_line.product_id, product_line.product_uom_qty)
                                            
                else:
                    if order_line.product_id.type in ['consu', 'product']:
                     
                        action = self._find_suitable_rule(order_line.product_id)
                        if action:
                            # if Product is Purchase Product Then
                            if 'Buy' in action and 'Make To Order' in action:
                                for product_line in self.order_line:
                                    order_line.action_make_po_from_sale(order_line.product_id, order_line.product_uom_qty)
                                
        return True


    @api.multi
    def _find_suitable_rule(self, product_id):
        route_lst = []
        action = ''
        Pull = self.env['stock.rule']
        res = self.env['stock.rule']
        #Prepare Domain For find rule
        pick_type_srch = self.env['stock.picking.type'].search([('warehouse_id', '=', self.warehouse_id.id), ('code', '=', 'incoming'), ('active', '=', True)])
        domain = [('location_id', 'in', [pick_type_srch.default_location_dest_id.id])]
        if self.warehouse_id:
            domain = expression.AND([['|', ('warehouse_id', '=', self.warehouse_id.id), ('warehouse_id', '=', False)], domain])
        if product_id.product_tmpl_id.route_ids:
            res = Pull.search(expression.AND([[('route_id', 'in', product_id.product_tmpl_id.route_ids.ids)], domain]), order='route_sequence, sequence', limit=1)
            action = res.action
            
        #change 25.06.2018(compare with route)
        for route in product_id.product_tmpl_id.route_ids:
            route_lst.append(route.name)
        # return action
        return route_lst
    

    
class SaleOrderLine(models.Model):
    _inherit = "sale.order.line"
    
    rfq_created = fields.Boolean('RFQ Created', copy=False)
    
    @api.multi
    def _get_purchase_order_date_line(self, schedule_date, product_id, product_qty):
        self.ensure_one()
        seller_delay = int(product_id._select_seller(quantity=product_qty, uom_id=product_id.uom_id).delay)
        return schedule_date - relativedelta(days=seller_delay)
    
    @api.multi
    def _get_purchase_schedule_date_line(self):
        date_planned = self.order_id.date_order + timedelta(days=self.customer_lead)
        date_planned1 = (date_planned - relativedelta(days=self.company_id.po_lead))
        # format_date_planned = fields.Datetime.from_string(self.date_planned)
        date_planned2 = date_planned1
        schedule_date = date_planned2 
        return schedule_date
    
        return schedule_date
    
    @api.multi
    def _prepare_purchase_order_from_line(self, partner, product_id, product_qty):
        '''Prepare purchase order using the Partne,BOM lines and sale order line '''
        self.ensure_one()
        schedule_date = self._get_purchase_schedule_date_line()
        purchase_date = self._get_purchase_order_date_line(schedule_date, product_id, product_qty)
        fpos = self.env['account.fiscal.position'].with_context(company_id=self.company_id.id).get_fiscal_position(partner.id)
        
        # Fetch location and destination location
        pick_type_srch = self.env['stock.picking.type'].search([('warehouse_id', '=', self.order_id.warehouse_id.id), ('code', '=', 'incoming'), ('active', '=', True)])
        
        return {
            'partner_id': partner.id,
            'company_id': self.company_id.id,
            'currency_id': partner.property_purchase_currency_id.id or self.env.user.company_id.currency_id.id,
            'origin': self.order_id.name,
            'payment_term_id': partner.property_supplier_payment_term_id.id,
            'date_order': purchase_date.strftime(DEFAULT_SERVER_DATETIME_FORMAT),
            'fiscal_position_id': fpos,
            'sale_order_id': self.order_id.id,
            'picking_type_id': pick_type_srch[0].id if pick_type_srch else False,
            'partner': self.order_id.partner_id.id,      #update decause in sale_report module file sale_data.py(while creation-updated)
            'sale_order': self.order_id.id,
            'sales_team': self.order_id.team_id.id or False,
            'sale_line': self.id,
        }

    @api.multi
    def _prepare_purchase_order_line(self, product_id, product_qty, po, supplier):
        ''' Prepare Purchase Order line using the sale order lina and BOM lines '''
        self.ensure_one()
        # product_qty = bom_line.product_qty * self.product_uom_qty
        procurement_uom_po_qty = product_id.uom_id._compute_quantity(product_qty, product_id.uom_po_id)
        seller = product_id._select_seller(
            partner_id=supplier.name,
            quantity=procurement_uom_po_qty,
            date=po.date_order,
            uom_id=product_id.uom_po_id)

        taxes = product_id.supplier_taxes_id
        fpos = po.fiscal_position_id
        taxes_id = fpos.map_tax(taxes) if fpos else taxes
        if taxes_id:
            taxes_id = taxes_id.filtered(lambda x: x.company_id.id == self.company_id.id)

        price_unit = self.env['account.tax']._fix_tax_included_price_company(seller.price, product_id.supplier_taxes_id, taxes_id, self.company_id) if seller else 0.0
        if price_unit and seller and po.currency_id and seller.currency_id != po.currency_id:
            price_unit = seller.currency_id.compute(price_unit, po.currency_id)
        product_lang = product_id.with_context({
            'lang': supplier.name.lang,
            'partner_id': supplier.name.id,
        })
        name = product_lang.display_name
        # dimension, choice_lamping, option_c = product_id.dimensions_c, product_id.choice_lamping_c, product_id.options_c
        if product_lang.description_purchase:
            name += '\n' + product_lang.description_purchase
        # elif product_id.description_sale or dimension or choice_lamping or option_c:
        #     if product_id.description_sale:
        #         name = product_id.description_sale
        
        date_planned = self.env['purchase.order.line']._get_date_planned(seller, po=po).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
        # name = self.name
        # name += '\n\n' + self.name

        return {
            'name': name,
            'product_qty': procurement_uom_po_qty,
            'product_id': product_id.id,
            'product_uom': product_id.uom_po_id.id,
            'price_unit': price_unit,
            'date_planned': date_planned,
            'taxes_id': [(6, 0, taxes_id.ids)],
            'order_id': po.id,
            'line_description': self.name,
            'website_description': self.name,
        }

    @api.multi
    def _select_po_supplier(self, product_id):
        # suppliers = [s for s in product_id.seller_ids if s.product_id.id == product_id.id]
        suppliers = [s for s in product_id.seller_ids if not s.product_id or s.product_id.id == product_id.id]
        if not suppliers:
            pass
            # raise UserError(_('No vendor associated to product %s. Please set one to fix this problems.') % (product_id.name))
        return suppliers

    def _existing_po_domain(self, partner):
        domain = (
                    ('partner_id', '=', partner.id),
                    ('state', '=', 'draft'),
                    ('company_id', '=', self.company_id.id),
                    ('sale_order_id', '=', self.order_id.id),
                )
        return domain
    
    @api.multi
    def action_make_po_from_sale(self, product_id, product_qty):
        ''' Create Purchase Order and PO lines using the Sale order line and BOM line data '''
        cache = {}
        res = []
        po = False
        #Fetch Supplier data
        supplier = self._select_po_supplier(product_id)

        if supplier:
            supplier = supplier[0]
            partner = supplier.name
        else:
            return True

        #Check for existing created PO
        domain = self._existing_po_domain(partner)
        if domain:
            po = self.env['purchase.order'].search([dom for dom in domain])
            po = po[0] if po else False
        if not po:
            # Prepare PO values and create Purchase Order
            vals = self._prepare_purchase_order_from_line(partner, product_id, product_qty)
            po = self.env['purchase.order'].create(vals)
        po_line = False
        # Fetch the PO Poline for the same product and update the quantity
        # If same product then merge quantity
        if po:
            for po_line2 in po.order_line:
                if po_line2.product_id.id == product_id.id and po_line2.product_uom.id == product_id.uom_po_id.id:
                    # po_line2.product_qty = po_line2.product_qty + product_qty
                    po_line = True
            
        # Prepare PO Lines and Create Line
        #If PO line already created check the po qty with update qty of so if equal than ok otherwise ipdate po_qty with new so_qty
        if po_line:
            po_li= self.env['purchase.order.line'].search([('product_id','=',self.product_id.id)])
            for lines in po_li:
                if not self.product_uom_qty == lines.product_qty:
                    lines.update({'product_qty':self.product_uom_qty})


        if not po_line:
            vals = self._prepare_purchase_order_line(product_id, product_qty, po, supplier)
            po_lines_create = self.env['purchase.order.line'].create(vals)

        return True
