# -*- coding: utf-8 -*-

from odoo import fields, api, models, _
from itertools import groupby
from datetime import datetime, timedelta
from odoo.exceptions import UserError
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from odoo.tools.misc import formatLang

import odoo.addons.decimal_precision as dp


class PurchaseOrder(models.Model):
    _inherit = "purchase.order"
    sale_order_id = fields.Many2one('sale.order', 'Sale Order')
    update_from_procurement = fields.Boolean('Updated from Procurement')


class ProcurementOrder(models.Model):
    _inherit = "purchase.order"
    
    @api.multi
    def make_po(self):
        cache = {}
        res = []
        SaleOrder = self.env['sale.order']
        
        for procurement in self:
            # If for any products No vendor is defined...Then Exception Procurement with reasons.
            suppliers = procurement.product_id.seller_ids.filtered(lambda r: not r.product_id or r.product_id == procurement.product_id)
            if not suppliers:
                procurement.message_post(body=_('No vendor associated to product %s. Please set one to fix this procurement.') % (procurement.product_id.name))
                continue
            
            origin_sale_order = procurement.origin.split(':')
            sale_order_srch = SaleOrder.search([('name', '=', origin_sale_order[0])], limit=1)
            if sale_order_srch and not sale_order_srch.make_po_bool and sale_order_srch.purchase_order_line:
                
                #call method to check the any changes in sale order line or bom product after the generated button clicked.
                if not sale_order_srch.update_so_po:
                    sale_order_srch.action_generate_rfq()
                    sale_order_srch.update_so_po = True
                
                for po in sale_order_srch.purchase_order_line:
                    for po_line in po.order_line:
                        if procurement.product_id.id == po_line.product_id.id:
                            #Procurements Data
                            gpo = procurement.rule_id.group_propagation_option
                            group = (gpo == 'fixed' and procurement.rule_id.group_id.id) or \
                                    (gpo == 'propagate' and procurement.group_id.id) or False
                            picking_type_id = procurement.rule_id.picking_type_id.id
                            dest_address_id = procurement.partner_dest_id.id
                            origin = procurement.origin
                            group_id = group
            
                            name = (procurement.group_id and (procurement.group_id.name + ":") or "") + (procurement.name != "/" and procurement.name or procurement.move_dest_id.raw_material_production_id and procurement.move_dest_id.raw_material_production_id.name or "")
                            message = _("This purchase order has been created from: <a href=# data-oe-model=procurement.order data-oe-id=%d>%s</a>") % (procurement.id, name)
                            
                            # Update the Purchase Order By using Procurements.Like,picking_type_id, dest_address_id,etc
                            if not po.update_from_procurement:
                                #update PO First
                                po.picking_type_id = picking_type_id
                                po.dest_address_id = dest_address_id
                                po.group_id = group_id
                                po.update_from_procurement = True
                                
                                #update date_planned(schedule date)
                                schedule_date = procurement._get_purchase_schedule_date()
                                purchase_date = procurement._get_purchase_order_date(schedule_date)
                                po.date_order = purchase_date
                                
                                #Update the Origin
                                if not po.origin or procurement.origin not in po.origin.split(', '):
                                    # Keep track of all procurements
                                    if po.origin:
                                        if procurement.origin:
                                            update_origin = po.origin
                                        else:
                                            update_origin = po.origin
                                    else:
                                        update_origin = procurement.origin
                                    po.origin =  update_origin
                    
                                #update messgae post for PO
                                po.message_post(body=message)
                
                            #Update PO Lines
                            po_line.procurement_ids = [(4, procurement.id)]
                            #update product quantity
                            po_line.product_qty = procurement.product_qty
                        
                            procurement.purchase_line_id = po_line.id
                                
                res += [procurement.id]
            else:
                res = super(ProcurementOrder, self).make_po()
        return res
