# -*- coding: utf-8 -*-

from odoo import models, fields, api

class DropshipCustom(models.Model):
    _name = 'dropship.custom'
    _description='Dropship Custom'

    dropship_id = fields.Many2one('res.partner')
    vendor=fields.Many2one("res.partner","Vendor",domain=[("supplier","=",True)])
    #ship_via = fields.Char(string="Ship Via")
    #ship_via = fields.Selection([('',''),('IDL','IDL'),('FedEx','FedEx'),('FedEx Priority', 'FedEx Priority'),('Marina Packing','Marina Packing'),('Regal Delivery','Regal Delivery'),('UPS Overnight','UPS Overnight'),('UPS 2nd Day','UPS 2nd Day'),('UPS 3rd Day','UPS 3rd Day'),('UPS Ground','UPS Ground'),('Will Call Pick Up','Will Call Pick Up')], default='', string="Ship Via")
    # ship_via = fields.Selection([('',''),('DHL','DHL'),('DHL Ground','DHL Ground'),('IDL','IDL'),('Best Way','Best Way'),('FedEx','FedEx'),('FedEx 3rd Day','FedEx 3rd Day'),('FedEx Priority', 'FedEx Priority'),('Marina Packing','Marina Packing'),('Regal Delivery','Regal Delivery'),('UPS Express','UPS Express'),('UPS Overnight','UPS Overnight'),('UPS 2nd Day','UPS 2nd Day'),('UPS 3rd Day','UPS 3rd Day'),('UPS Ground','UPS Ground'),('Will Call Pick Up','Will Call Pick Up'),('FedEx IPD','FedEx IPD'),
    #                              ('UPS Third Day-ORANGE','UPS Third Day-ORANGE'),('Fed-Ex','Fed-Ex'),('Fed-Ex IPD','Fed-Ex IPD'),('FED-EX','FED-EX'),
    #                              ('Fed Ex - IPD','Fed Ex - IPD'),('Best Way/Prepaid','Best Way/Prepaid'),('Fed Ex 3 Day','Fed Ex 3 Day'),
    #                              ('Fed Ex - Standard','Fed Ex - Standard'),('UPS STANDARD','UPS STANDARD'),('FedEx 3 Day','FedEx 3 Day'),('UPS Ground ','UPS Ground '),('UPS','UPS')
    #                              ,('Ground','Ground'),('FedEx Standard','FedEx Standard'),('Fed Ex -Internatl','Fed Ex -Internatl'),('Fed Ex -IPD','Fed Ex -IPD'),
    #                              ('UPS Groud','UPS Groud'),('Fed Ex Ground','Fed Ex Ground')], default='', string="Ship Via")

    # ship_via = fields.One2many('dropship.custom.shipvia','ship_via_ids',string="Ship Via")
    ship_via = fields.Many2one('dropship.custom.shipvia',string="Ship Via")
    terms = fields.Char(string="Terms")
    freight_terms = fields.Char(string="Freight Terms")
    account_no = fields.Char(string="Account #")
    note = fields.Char(string="Note")

class DropshipCustomShipVia(models.Model):

    _name = 'dropship.custom.shipvia'

    name = fields.Char("Name")

# class DropshipCustomShipViaNEw(models.Model):
#
#     _name = 'dropship.custom.ship'
#
#     name = fields.Char("Name")
#     ship_via_ids = fields.Many2one('dropship.custom')

class ResPartner(models.Model):
    _inherit = "res.partner"

    dropship_ids = fields.One2many('dropship.custom','dropship_id','Dropship')

class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    ship_via = fields.Char(string="Ship Via", size=100)
    account_no = fields.Char("Account No",compute="get_detail")
    terms = fields.Char(string="Terms", size=100)
    freight_terms = fields.Char(string="Freight Terms", size=100)
    note = fields.Char(string="Note", size=200)

    @api.multi
    def get_detail(self):
        cus_dropship = self.env['dropship.custom']
        dropship_line_detail = ""
        for line in self:
            vendor = line.product_id.seller_ids and line.product_id.seller_ids[0].name
            if vendor:
                dropship_line = cus_dropship.search([('dropship_id', '=', line.order_id.partner_id.id),('vendor', '=', vendor.id)])
                if len(dropship_line) > 0:
                    dropship_line_detail = dropship_line[0]

                if dropship_line_detail:
                    line.account_no     = dropship_line_detail.account_no
                    line.ship_via       = dropship_line_detail.ship_via
                    line.terms          = dropship_line_detail.terms
                    line.freight_terms  = dropship_line_detail.freight_terms
                    line.note           = dropship_line_detail.note

                    line.order_id.x_ship_via       = dropship_line_detail.ship_via
                    line.order_id.x_freight_terms  = dropship_line_detail.freight_terms
                    line.order_id.x_client_account = dropship_line_detail.account_no
                    line.order_id.x_showroom_terms = dropship_line_detail.terms


class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'

    ship_via = fields.Char(string="Ship Via", compute="get_detail")
    account_no = fields.Char("Account No", compute="get_detail")
    terms = fields.Char(string="Terms", compute="get_detail")
    freight_terms = fields.Char(string="Freight Terms", compute="get_detail")
    note = fields.Char(string="Note", compute="get_detail")

    @api.multi
    def get_detail(self):
        # pro_obj = self.env['procurement.order']
        for line in self:
            # p_order = pro_obj.sudo().search([('purchase_line_id', '=', line.id)])
            print(line.sale_line_id,line.sale_order_id)
            if line.sale_line_id:
                    line.account_no = line.sale_line_id.account_no
                    line.ship_via = line.sale_line_id.ship_via
                    line.terms = line.sale_line_id.terms
                    line.freight_terms = line.sale_line_id.freight_terms
                    line.note = line.sale_line_id.note
