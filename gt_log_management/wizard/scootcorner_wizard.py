import time
from openerp import api, fields, models
import MySQLdb

class ScottcoornerWizard(models.Model):
    _name = "scootcorner.wizard"
    
    
#     import_product_stock = fields.Boolean('Import Product Stock')
    shop_ids = fields.Many2one('magento.store', string="Select Shops", readonly=True)
    instance_ids = fields.Many2one('magento.instance',string='Select Instance')
    
    currencies = fields.Boolean('Currencies')
    sales_purchase_taxLocations = fields.Boolean('Sales_Purchase TaxLocations')
    shipvia_delivery_carrier = fields.Boolean('ShipVia_delivery carrier')
    terms=fields.Boolean("Terms")
    sales_categories=fields.Boolean("SalesCategories")
    locations=fields.Boolean("Locations")
    vendors=fields.Boolean("Vendors")
    employee = fields.Boolean(string='Users')
    
    product_manufactured =fields.Boolean("Products Manufactured ")
    products_purchased=fields.Boolean('Products Purchased')
    customers=fields.Boolean('Customers')
    
    address_contacts  = fields.Boolean('Address Contacts')
    confirmed_sales_orders = fields.Boolean('Confirmed Sales Orders')
#     salesorder_lineitems = fields.Boolean('SalesOrder LineItems')
    quotes = fields.Boolean('Quotes')
#     quotes_lineitems = fields.Boolean('Quotes LineItems')
    rfq = fields.Boolean('RFQ')
#     rfq_lineitems = fields.Boolean('RFQ LineItems')
    purchase_orders = fields.Boolean(string='Purchase Orders')
    bom = fields.Boolean(string='BOM')
    
    
    @api.one
    def import_datas(self):
        config_obj = self.env['scottcoorner.config']
        currency_obj = self.env['res.currency']
        accounttax_obj = self.env['account.tax']
        user_obj = self.env['res.users']
        config_id = config_obj.search([])
        if config_id:
            db = MySQLdb.connect(host=config_id[0].url,
                     user=config_id[0].user_name,
                     passwd=config_id[0].password,
                     db=config_id[0].db_name)
            cursor = db.cursor()
            if self.currencies == True:
                currency_obj.import_currency_data(cursor)
            if self.employee:
                user_obj.import_users_data(cursor)
            if self.locations:
                accounttax_obj.import_taxes_data(cursor)
            
        return True
    
    
    





